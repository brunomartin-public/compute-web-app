Compute Web App
===

<p align="center">
  <img src="banner.png" alt="CWA banner" width="300"/>
</p>

#### 1. Introduction

Compute Web App (or CWA) is a web application designed to schedule and run intensive and distributed large data processes.

<p align="center">
  <img src="concept-cloud.png" alt="CWA Cloud concept" width="500"/>
  <em>CWA Cloud concept</em>
</p>

Its dreamed main features:

- Fast and easy to deploy,
- Secured process parameters, data and results transfer,
- Simple RESTfull API designed to:
  - upload data,
  - control and monitor processes and
  - retrieve results,
- Simple and efficient web interface provided,
- Designed to run processes in parallel,
- Adapted to dedicated servers.

Its dependencies:
- [Eigen](https://eigen.tuxfamily.org) for data arrangement,
- [POCO C++ Libraries](https://pocoproject.org) for process launch and handle,
- [Restbed](https://github.com/Corvusoft/restbed) or [Pistache](https://github.com/pistacheio/pistache) for the RESTful API (not decided yet),
- [Vue.js](https://vuejs.org) for web UI.

#### 2. Project organization

This section details organization of this project giving details about each subdirectory. The main project is in C/C++ and a Python version is developped in parallel. Improvements has been made lately on the Python version of CWA.

- `benchmark` contains benchmarks with targeted dependency candidates,
- `doc` contains necessary to build documentation on the source code and on architecture of the project,
- `frontend` contains source for the CWA web frontend sources and documenatation to generate it,
- `openapi` contains input file to generate wab page for RESTful API documentation,
- `python` contains Python version of CWA,
- `src` contains C/C++ source code and tests of this project and 
- `tests` shall contain high level tests.

#### 3. API Documentation

API documentation can be found [here](http://brunomartin-public.frama.io/compute-web-app)
