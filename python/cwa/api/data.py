import os, json
from pathlib import Path

import flask
import connexion

from cwa.base.data import Data, Dataset, Attribute, DatasetValue, GroupDatasetValue
from cwa.base import image_utils
from cwa.base.data_manager import DataManager

def get_data_manager() -> DataManager:
    """ Get data manager from current flask context """
    with flask.current_app.app_context():
        data_manager: DataManager = flask.current_app.data_manager
        return data_manager

def get_data_ids():
    """ Get all data ids. """
    return get_data_manager().load_ids()

# list data ids, names of .h5 files found in data_path directory
def search():
    """ List data ids and filenames found in data path. """
    
    data_ids = get_data_manager().load_ids()
    
    if 'details' in connexion.request.query_params:
        detailed_datas = [get(data_id) for data_id in data_ids]
        return detailed_datas
    
    return data_ids

# Create a handler for get data
def get(data_id):
    """ This function responds to a request for /api/data/{data_id}
    by recording data uploaded
    """
    
    data = get_data_manager().load(data_id=data_id)
    
    # Build dictionary from object
    def json_default(obj):
        # obj may be a numpy.dtype that has no dict
        # Try to return obj as a string in that case
        if not hasattr(obj, '__dict__'):
            return str(obj)        
        return obj.__dict__
    
    data_json = json.dumps(data, default=json_default)
    data = json.loads(data_json)
    
    # Adapt return data to current frontend
    # TODO: update it when frontend will be fixed
    data['post_time'] = data['queue_start_time']
    data['upload_end_time'] = data['queue_end_time']
    data['status']['uploaded'] = data['status']['queued']
    data['status']['upload_progress'] = data['status']['queue_progress']
    data['groups'] = data['group_count']
    data['filename'] = data['id'] + '.h5'
    
    # This masks root datasets for current frontend
    # TODO: don't mask it
    data['datasets'] = data['group_datasets']
    
    return data
    
# Create a handler for get data file
def get_file(data_id):
    """ This function responds to a request for /api/data/{data_id}
    by recording data uploaded
    """
    try:
        f = get_data_manager().open_file(data_id=data_id)
        filename = Path(f.name).name
        headers = {
            'Content-Disposition': 'attachment; filename="' + filename + '"',
            'Content-Length': os.fstat(f.fileno()).st_size
        }
        return f.read(), 200, headers
    except Exception as e:
        return f'Exception: {str(e)}', 404

# handler for post new data, value are sent after
def post(data: dict):
    """@
    This function responds to a request for /api/data
    by recording data uploaded
    """

    # data shall already be a dict containing at least required
    # parameters
    # TODO: Find why data is not a dictionary here
    if not isinstance(data, dict):
        data = json.loads(data)
        
    base_data = Data(
        name=data['name'],
        version=data['version'],
        datasets=[Dataset(x['name'], x['dtype'], x['shape']) \
            for x in data.get('datasets', [])],
        group_count=data['groups']['count'],
        group_datasets=[Dataset(x['name'], x['dtype'], x['shape']) \
            for x in data['groups']['datasets']],
        attributes=[Attribute(x['name'], x['dtype'], x['value']) \
            for x in data.get('attributes', [])]
    )
    
    try:
        data_id = get_data_manager().add(base_data)
    except FileExistsError as e:
        return f'Exception: {str(e)}', 500

    response = {
        "status": "ok",
        "data_id": data_id
    }

    return response

def put_dataset_value(data_id, dataset_id, body):
    """
    Put value of dataset at root level of a file
    """
    
    data_value = DatasetValue(dataset_id, body)
    get_data_manager().fill_root_dataset(data_id, data_value)

    response = {
        "status": "ok"
    }

    return response

def put_group_dataset_value(data_id, group_id, dataset_id, body):
    """
    Put value of dataset in a group of a file
    """
    
    dataset_value = GroupDatasetValue(group_id, dataset_id, body)
    get_data_manager().fill_group_dataset(data_id, dataset_value)
    
    response = {
        "status": "ok"
    }
    
    return response

def uploading_search():
    return get_data_manager().get_uploading_ids()

def get_group_dataset_image(data_id, group_id, dataset_id, start=0, length=1):
    """ Get dataset value create an image from it. """

    try:
        array = get_data_manager().load_dataset_value(
            data_id=data_id, group_id=group_id,
            dataset_id=dataset_id, start=start, length=length)
    except FileNotFoundError as e:
        return f'FileNotFoundError: {str(e)}', 404
    except TypeError as e:
        return f'Type not supported: {str(e)}', 400
    except Exception as e:
        return f'Exception: {str(e)}', 500

    http_accept = str()
    if 'ACCEPT' in connexion.request.headers:
        http_accept = connexion.request.headers['ACCEPT']

    http_accept = http_accept.split(',')

    image_type = "PNG"

    if 'image/png' in http_accept:
        pass
    elif 'image/jpeg' in http_accept:
        image_type = "JPEG"
    else:
        image_type = "PNG"
        # return 'Format not supported', 404

    try:
        data_path = flask.current_app.data_manager.data_path
        body = image_utils.array_to_image(array, image_type, data_path)
    except Exception as e:
        return f'Exception: {str(e)}', 500

    return body
