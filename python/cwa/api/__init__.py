from cwa.api.data import get_data_manager
from cwa.api.process_definition import get_process_def_manager
from cwa.api.result import get_result_manager

# Create a handler for our api root url
def search():
    data_path_str = str(get_data_manager().data_path.absolute())
    process_def_path_str = str(get_process_def_manager().process_def_path.absolute())
    result_path_str = str(get_result_manager().data_path.absolute())

    return {
        "status": "ok",
        "message": "Welcome to the Compute Web App",
        "version": "1.0",
        "data_path": data_path_str,
        "process_definition_path": process_def_path_str,
        "result_path": result_path_str
    }
