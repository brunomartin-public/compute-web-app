from pathlib import Path
import json
import pprint

import flask

from cwa.base.process_def import ProcessDefParameter
from cwa.base.process_def_manager import ProcessDefManager


def get_process_def_manager() -> ProcessDefManager:
    """ Get process definition manager from current flask context """
    with flask.current_app.app_context():
        process_def_manager: ProcessDefManager = flask.current_app.process_def_manager
        return process_def_manager

# local function to get a process definition from filename
def get(process_def_id):
    process_def = get_process_def_manager().load(process_def_id)
    
    # Build dictionary list from object
    process_def_json = json.dumps(
        process_def,default=lambda obj: obj.__dict__)
    process_def_dict: dict = json.loads(process_def_json)
    
    # Adapt return data to current frontend
    # TODO: update it when frontend will be fixed
    parameters = process_def_dict.get('parameters', [])
    parameters_old = {}
    for parameter in parameters:
        parameters_old[parameter['name']] = {
            'type': parameter['dtype'],
            'default': parameter['default'],
        }
    process_def_dict['parameters'] = parameters_old
    
    process_def_dict['results'] = process_def_dict['result_datasets']
    del process_def_dict['result_datasets']
    
    if len(process_def_dict['errors']) == 0:
        del process_def_dict['errors']
    else:
        process_def_dict['error'] = '\n'.join(
            process_def_dict['errors'])
        del process_def_dict['errors']
        
    del process_def_dict['args']
    
    return process_def_dict

# search handler to list all process datas
def search():
    """ List all present process definitions. """
    
    # Load process def ids and load dict content for each
    process_defs_dict = []
    for process_def_id in get_process_def_manager().load_ids():
        process_defs_dict.append(get(process_def_id))
        
    return process_defs_dict
