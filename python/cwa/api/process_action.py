import json

import flask

from cwa.base.process_manager import ProcessManager, ProcessNotFound, \
    ProcessAction, ProcessActionTimeout


def get_process_manager() -> ProcessManager:
    """ Get process manager from current flask context """
    with flask.current_app.app_context():
        process_manager: ProcessManager = flask.current_app.process_manager
        return process_manager


def search():
    """ Search handler to list all process action triggered. """
    return get_process_manager().process_actions

# post a new action to a specific process already posted
def post(action: dict):

    # action shall already be a dict containing at least required
    # parameters
    # TODO: Find why action is not a dictionary here
    if not isinstance(action, dict):
        action = json.loads(action)

    action_obj = ProcessAction(
        action['name'], action['process_id'], action['version'])

    try:
        action_id = get_process_manager().do_action(action_obj)
    except ProcessNotFound as e:
        return f'Process {action_obj.process_id} not found', 404
    except ProcessActionTimeout as e:
        return f'Could not stop {action_obj.process_id}', 500
    except Exception as e:
        return f'Process {action_obj.process_id} exception: {str(e)}', 500
    
    response = {
        'status': 'ok',
        'process_action_id': action_id
    }

    return response
