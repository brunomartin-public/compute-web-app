import os
import json
from pathlib import Path

import flask
import connexion

from cwa.base import image_utils
from cwa.base.result_manager import ResultManager

def get_result_manager() -> ResultManager:
    """ Get result manager from current flask context """
    with flask.current_app.app_context():
        result_manager: ResultManager = flask.current_app.result_manager
        return result_manager
    
def search():
    return get_result_manager().load_ids()

def load_result(result_id: str):
    """ Load result data from id. """
    return get(result_id)

# Create a handler for get result
def get(result_id: str):
    """ Load result data from id. """
    result = get_result_manager().load_result(result_id)
    
    # Build dictionary from object
    def json_default(obj):
        # obj may be a numpy.dtype that has no dict
        # Try to return obj as a string in that case
        if not hasattr(obj, '__dict__'):
            return str(obj)        
        return obj.__dict__
    
    result_json = json.dumps(result, default=json_default)
    result_dict = json.loads(result_json)
    
    # This masks root datasets for current frontend
    # TODO: don't mask it
    result_dict['groups'] = result_dict['group_count']
    result_dict['datasets'] = result_dict['group_datasets']
    
    return result_dict

# Create a handler for get data file
def get_file(result_id: str):
    """ Load file to be downloaded. """

    try:
        f = get_result_manager().open_file(data_id=result_id)
        filename = Path(f.name).name
        headers = {
            'Content-Disposition': 'attachment; filename="' + filename + '"',
            'Content-Length': os.fstat(f.fileno()).st_size
        }
        return f.read(), 200, headers
    except Exception as e:
        return f'Exception: {str(e)}', 404

def group_get(result_id: str, group_id):
    """ Load and send group datasets. """

    return get_result_manager().load_group_datasets(result_id=result_id, group_id=group_id)

def dataset_get(result_id, group_id, dataset_id):
    """ Load dataset from group and result. """

    try:
        dataset = get_result_manager().load_group_dataset(result_id, group_id, dataset_id)
    except OSError:
        return f'Result {result_id} does not exists', 404
    except KeyError:
        return f'Group {group_id} of {result_id} does not exists', 404
    
    response = {
        "status": "ok",
        "shape": dataset.shape,
        "dtype": str(dataset.dtype)
    }

    return response

# curl http://localhost:5000/api/result/0/dataset/0/value

# Create a handler for our read (GET) download
def dataset_value_get(result_id, group_id, dataset_id):
    """ Get dataset value as bytes. """

    date_type = None
    if 'type' in connexion.request.query_params:
        date_type = connexion.request.query_params['type']

    try:
        # Need to use None for start and length parameters
        # TODO: find a clearer way to handle that
        array = get_result_manager().load_dataset_value(result_id, group_id, dataset_id, query_data_type=date_type)
    except OSError:
        return f'Result {result_id} does not exists', 404
    except KeyError:
        return f'Group {group_id} of {result_id} does not exists', 404
    except TypeError as e:
        return f'Type not supported: {str(e)}', 400
    except Exception as e:
        return f'Exception: {str(e)}', 500
    
    return array.tobytes()


def get_group_dataset_image(result_id, group_id, dataset_id, start=0, length=1):
    """ Get dataset value create an image from it. """

    try:
        array = get_result_manager().load_dataset_value(
            data_id=result_id, group_id=group_id,
            dataset_id=dataset_id, start=start, length=length)
    except FileNotFoundError as e:
        return f'FileNotFoundError: {str(e)}', 404
    except TypeError as e:
        return f'Type not supported: {str(e)}', 400
    except Exception as e:
        return f'Exception: {str(e)}', 500

    http_accept = str()
    if 'ACCEPT' in connexion.request.headers:
        http_accept = connexion.request.headers['ACCEPT']

    http_accept = http_accept.split(',')

    image_type = "PNG"

    if 'image/png' in http_accept:
        pass
    elif 'image/jpeg' in http_accept:
        image_type = "JPEG"
    else:
        image_type = "PNG"
        # return 'Format not supported', 404

    try:
        result_path = flask.current_app.result_manager.data_path
        body = image_utils.array_to_image(array, image_type, result_path)
    except Exception as e:
        return f'Exception: {str(e)}', 500

    return body