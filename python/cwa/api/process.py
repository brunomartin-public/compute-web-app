import json
import copy

import numpy as np
import flask

from cwa.base.process_manager import ProcessManager, MissingParameters, \
    Process


def get_process_manager() -> ProcessManager:
    """ Get process manager from current flask context """
    with flask.current_app.app_context():
        process_manager: ProcessManager = flask.current_app.process_manager
        return process_manager


def filter_process(process, with_log_content=False):

    # expose all except private
    result = {}
    for key in process:
        if key == 'private':
            pass
        elif key == 'log' and with_log_content:
            # read log file
            log_filepath = process['private']['log_filepath']
            log_file = open(log_filepath, 'r')
            result['status']['log'] = log_file.read()
            log_file.close()
        else:
            result[key] = copy.deepcopy(process[key])

    # add log if asked
    if with_log_content:
        # read log file
        log_filepath = process['private']['log_filepath']
        try:
            log_file = open(log_filepath, 'r')
            result['status']['log'] = log_file.read()
            log_file.close()
        except OSError:
            print('Error while reading ', log_filepath)
            result['status']['log'] = ''
    else:
        result['status']['log'] = ''

    # remove file names from parameters
    if 'data_file' in result['parameters']:
        result['parameters'].pop('data_file')

    if 'result_file' in result['parameters']:
        result['parameters'].pop('result_file')

    return result

def load_and_prepare(process_manager: ProcessManager, process_id):
    """Load process from manager and prepare a dict."""

    process = process_manager.load(process_id)
    process_dict = json.loads(process.dumps_json())
    process_dict = filter_process(process_dict)

    # Current frontend waits for parameters as an object, not as a
    # structure, and some of parameters are not send, quick fix it
    # TODO: to be fixed when frontend has been upgraded
    parameters_obj = {}
    for parameter in process_dict['parameters']:
        if parameter['name'] in ['data_file', 'result_file']:
            continue
        parameters_obj[parameter['name']] = \
            parameter['value']
    process_dict['parameters'] = parameters_obj
    process_dict['post_time'] = process_dict['creation_time']
    del process_dict['creation_time']

    return process_dict

def search():
    """ Return filtered list of processes. """
    
    process_manager = get_process_manager()
    process_ids = process_manager.load_process_ids()
    results = []
    error = False
    for process_id in process_ids:
        try:
            process_dict = load_and_prepare(process_manager, process_id)
        except Exception as e:
            process_dict = {
                'process_id': process_id,
                'status': 'error',
                'error': str(e)
            }
            error = True
        results.append(process_dict)

    if error:
        return results, 500
    
    return results, 200


def running_search():
    """ Return list of running process ids. """
    return get_process_manager().load_running_process_ids()


def post(process: dict):
    """ Post a new process on existing data."""

    # process shall already be a dict containing at least required
    # parameters
    # TODO: Find why process is not a dictionary here
    if not isinstance(process, dict):
        process = json.loads(process)

    # New process_def_id parameter is still not knwon from web GUI
    # TODO: remove next lines when web GUI will be updated
    if 'process_definition_id' in process:
        process['process_def_id'] = process['process_definition_id']
        del process['process_definition_id']

        old_parameters: dict = process.get('parameters', [])
        parameters = []
        for name, value in old_parameters.items():
            dtype = np.array([value]).dtype
            parameters.append({
                'name': name,
                'type': str(dtype),
                'value': value,
            })
        process['parameters'] = parameters

    try:
        process_obj = Process.create_from_json(process)
        process_id = get_process_manager().add(process_obj)
    except MissingParameters as e:
        return 'Missing parameters: ' + ', '.join(e.parameters), 400
    except Exception as e:
        return f'Exception: {str(e)}', 400
    
    response = {
        'status': 'ok',
        'process_id': process_id
    }

    return response

def get(process_id):
    """ Get filtered process of process_id. """

    process_manager = get_process_manager()
    try:
        process_dict = load_and_prepare(process_manager, process_id)
    except Exception as e:
        return f'Exception: {str(e)}', 500

    return process_dict
