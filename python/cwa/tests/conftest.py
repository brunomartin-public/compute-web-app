"""
conftest.py file to add options to pytest command line
"""

def pytest_addoption(parser):
    # Add application data path to option so that test will create test data
    # in that directory.
    parser.addoption('--storage-path', action='store', default=None, help='Application data path')

option = None

def pytest_configure(config):
    """Make cmdline arguments available to dbtest"""
    global option
    option = config.option
