import pytest

import os
import time
import datetime
from pathlib import Path

from cwa.tests.common import TestData
from cwa.base.data_manager import DataManager


class TestPostData(TestData):
    """Test /api/data/{data_id}/group/{group_id}/value post response."""

    def test_post(self):
        self.api_post_data()
        self.api_post_data_group_value(use_threads=False)
        self.check()

    def test_threaded_post(self):
        self.api_post_data()
        self.api_post_data_group_value(use_threads=True)
        self.check()

    def check(self, refresh=False):
        """Test /api/data/{data_id}/group/{group_id}/value post response."""

        # Get base data manager object to get data path
        data_manager: DataManager = self.app.app.data_manager

        # construct data file path
        filepath = data_manager.data_path / (pytest.data_id + '.h5')

        # open file
        file = self.open_file_for_reading(filepath)

        assert 'cwa_stored' in file.attrs
        assert 'cwa_store_end_time' in file.attrs

        if not refresh:
            # wait until uploaded and stored by closing/opening file
            while not bool(file.attrs['cwa_stored']):
                time.sleep(0.1)
                file.close()
                file = self.open_file_for_reading(filepath)

        assert 'test1' in file.attrs
        assert file.attrs['test1'] == 10

        assert 'test2' in file.attrs
        assert file.attrs['test2'] == 'This is a test string'

        for group_id in range(len(pytest.slice_locations)):

            group_array = pytest.data[:, :, :, group_id]
            group_name = '%03d' % group_id
            assert group_name in file
            
            group = file[group_name]
            assert 'cwa_stored' in group.attrs

            if refresh:
                while not group.attrs['cwa_stored']:
                    time.sleep(0.2)
                    group.refresh()

            assert group.attrs['cwa_stored']

            # check magnitude dataset type and value
            assert 'magnitude' in group
            dataset = group['magnitude']
            assert dataset.dtype == 'uint16'

            if refresh:
                dataset.refresh()

            for i in range(len(pytest.echo_times)):
                image = dataset[:, :, i]
                expected_image = group_array[:, :, i]

                if (image != expected_image).any():
                    print("image mismatch !!!")

                assert (image == expected_image).all()

            # check magnitude dataset type and value
            assert 'phase' in group
            dataset = group['phase']
            assert dataset.dtype == 'uint16'

            if refresh:
                dataset.refresh()

            for i in range(len(pytest.echo_times)):
                image_index = len(pytest.echo_times) + i
                assert (dataset[:, :, i] == group_array[:, :, image_index]).all()

        if refresh:
            # wait until uploaded and stored by refreshing root group
            while not bool(file.attrs["cwa_stored"]):
                time.sleep(0.2)
                file["/"].refresh()

        # get upload and store end time
        store_end_time_str = file.attrs['cwa_store_end_time'].decode()
        file.close()

        store_end_time = None
        try:
            store_end_time = datetime.datetime.fromisoformat(store_end_time_str)
        except ValueError as e:
            pass

        # assert store_end_time
        assert store_end_time != None

