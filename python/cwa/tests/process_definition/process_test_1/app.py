try:

    import time
    import sys
    import argparse
    import h5py
    import numpy as np

    # parse arguments
    parser = argparse.ArgumentParser()

    parser.add_argument('mean_factor', help='Mean factor to apply after mean operation', default=1.0, type=float)
    parser.add_argument("step_count", help="Process step count", default=100, type=int)
    parser.add_argument("end_message", help="Message to display at the end", type=str)
    parser.add_argument('data_file', help='data file name', type=str)
    parser.add_argument('result_file', help='result file name', type=str)

    args = parser.parse_args()

    mean_factor = args.mean_factor
    step_count = args.step_count
    end_message = args.end_message
    data_filename = args.data_file
    result_filename = args.result_file

    # open data file for reading
    data_file = h5py.File(
        data_filename, 'r', libver='latest', swmr=True)

    # get group count
    groups = len(data_file) - 2
    processed_groups = 0

    # Check if pause in file attributes and use it if present
    pause = 5/groups
    if 'pause_s' in data_file.attrs:
        pause = float(data_file.attrs['pause_s'])

    # Check if dataset name to process in file attributes and use it if
    # present
    dataset_name = 'magnitude'
    if 'dataset_to_process' in data_file.attrs:
        dataset_name = str(data_file.attrs['dataset_to_process'])

    # Check if there is a errored group and error message in file
    # attributes prepare error to raised
    errored_group = -1
    if 'errored_group' in data_file.attrs:
        errored_group = int(data_file.attrs['errored_group'])

    error_message = ''
    if 'error_message' in data_file.attrs:
        error_message = str(data_file.attrs['error_message'])

    # Check if early process option in file attributes
    early_process = False
    if 'early_process' in data_file.attrs:
        early_process = bool(data_file.attrs['early_process'])

    # Check if early result option in file attributes
    early_result = False
    if 'early_result' in data_file.attrs:
        early_result = bool(data_file.attrs['early_result'])

    # open result file for writing
    if early_result:
        result_file = h5py.File(result_filename, 'w', libver='latest')
    else:
        result_file = h5py.File(result_filename, 'w')

    # copy attributes
    for attr in data_file.attrs:
        result_file.attrs[attr] = data_file.attrs[attr]

    if early_result:
        # first, create all groups and datasets to enable swmr mode
        for group_name in data_file:
            # only create groups for data file groups
            group = data_file[group_name]

            if not isinstance(group, h5py.Group):
                continue

            group = data_file[group_name]
            dtype = group[dataset_name].dtype
            shape = group[dataset_name].shape

            shape = (shape[0], shape[1])

            result_group = result_file.create_group(group_name)
            result_group.create_dataset('mean', shape=shape, dtype=dtype)
            result_group.create_dataset('std', shape=shape, dtype=dtype)
            result_group.create_dataset('min', shape=shape, dtype=dtype)
            result_group.create_dataset('max', shape=shape, dtype=dtype)

        result_file.swmr_mode = True

    # create average, std, min and max dataset for each group
    for group_name in data_file:
        group = data_file[group_name]

        # If not a group, don't compute anything
        if not isinstance(group, h5py.Group):
            continue

        # If early process, wait until group content is stored
        while early_process and not group.attrs['cwa_stored']:
            data_file.close()
            time.sleep(0.5)
            data_file = h5py.File(
                data_filename, 'r', libver='latest', swmr=True)
            group = data_file[group_name]

        # Check if this group shall issue error and do it
        if errored_group == int(group_name):
            half_error_message = error_message[:int(len(error_message)/2)]
            print(f'[ERROR {half_error_message}]')
            raise Exception(error_message)

        data = group[dataset_name]

        mean_value = (np.mean(data, axis=2)*mean_factor).astype(data.dtype)
        std_value = np.std(data, axis=2).astype(data.dtype)
        min_value = np.min(data, axis=2)
        max_value = np.max(data, axis=2)

        time.sleep(pause)

        if early_result:
            result_group = result_file[group_name]
            result_group['mean'][:] = mean_value
            result_group['std'][:] = std_value
            result_group['min'][:] = min_value
            result_group['max'][:] = max_value
        else:
            result_group = result_file.create_group(group_name)
            result_group.create_dataset('mean', data=mean_value)
            result_group.create_dataset('std', data=std_value)
            result_group.create_dataset('min', data=min_value)
            result_group.create_dataset('max', data=max_value)

        processed_groups += 1

        # print progress message that will be catch by the scheduler
        progress = int(processed_groups/groups*100)
        print('[PROGRESS {}%]'.format(progress))

    # close result file
    result_file.close()

    # close data file
    data_file.close()

    # exit with OK status code
    sys.exit(0)

except Exception as e:
    # print error message that will be catch by the scheduler
    print('[ERROR {}]'.format(str(e)))

    # exit with an error status code
    sys.exit(-1)
