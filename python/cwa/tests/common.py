
import pytest

import unittest
import os
import platform
import logging
from pathlib import Path
import time
import json
import numpy as np
import hashlib
import h5py
import random
from datetime import datetime

from threading import Thread

from cwa.app import CwaApp

from cwa.tests.conftest import option
from cwa.base.data_manager import DataManager

local_test_dir = False
# local_test_dir = True


class TestBase(unittest.TestCase):
    """
        Base class for flask unit tests.
    """

    # Instance of CwaApp
    app: CwaApp

    @classmethod
    def setUpClass(cls):

        # define application paths
        temp_path = Path(__file__).parent / 'tmp'
        data_path = temp_path / 'data'
        process_def_path = Path(__file__).parent / 'process_definition'
        result_path = temp_path / 'result'

        # create directory of they not exist
        data_path.mkdir(exist_ok=True, parents=True)
        result_path.mkdir(exist_ok=True, parents=True)
        
        logger = logging.getLogger('cwa.base.test')
        logger.setLevel(logging.INFO)
        logger.info(f'Data path is {data_path}')
        logger.info(f'Process definition path is {process_def_path}')
        logger.info(f'Result path is {result_path}')

        cls.app = CwaApp(data_path, process_def_path, result_path)

        with cls.app.test_client() as client:
            cls.client = client

    @classmethod
    def tearDownClass(cls):
        del cls.client

class TestData(TestBase):

    """
        Base class for process unit tests.
    """

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def api_post_data(self):
        """Test /api/data/ post response."""

        client = self.client

        pytest.data_dims = {
            'height': 20,
            'width': 30,
            'depth': 10,
            'groups': 8
        }

        height = pytest.data_dims['height']
        width = pytest.data_dims['width']
        echo_times = np.arange(float(pytest.data_dims['depth']))
        slice_locations = np.arange(float(pytest.data_dims['groups']))

        # Generate a random name so that multple data will be sent
        random.seed(datetime.now().timestamp())
        name = 'test_data_' + str(random.randint(0,99))

        # Declare datasets to add to root group
        datasets = [
            {
                'name': 'slice_locations',
                'dtype': 'd',
                'shape': [len(slice_locations)],
            },
            {
                'name': 'echo_times',
                'dtype': 'd',
                'shape': [len(echo_times)],
            },
        ]

        # Declare groups and datasets they will contain
        groups = {
            'count': len(slice_locations),
            'datasets': [
                {
                    'name': 'magnitude',
                    'dtype': 'u2',
                    'shape': [height, width, len(echo_times)]
                },
                {
                    'name': 'phase',
                    'dtype': 'u2',
                    'shape': [height, width, len(echo_times)]
                },
            ],
        }

        # Declare attributes to add to root
        attributes = [
            {
                'name': 'test1',
                'dtype': 'u2',
                'value': 10
            },
            {
                'name': 'test2',
                'dtype': 'str',
                'value': 'This is a test string'
            },
        ]

        pytest.data = {
            'name': name,
            'version': '1.0',
            'datasets': datasets,
            'groups': groups,
            'attributes': attributes,
        }

        headers = [{'Content-Type', 'application/json'}]
        response = client.post('/api/data', data=json.dumps(pytest.data), headers=headers)

        assert response.status_code == 200, \
            f'Error: {str(response.content)}'

        result = json.loads(response.content)

        assert 'data_id' in result
        pytest.data_id = result['data_id']
        
        assert 'status' in result
        assert result['status'] == 'ok'

        response = client.get('/api/data')
        assert response.status_code == 200, \
            f'Error: {str(response.content)}'

        result = json.loads(response.content)
        assert pytest.data_id in result

        response = client.get('/api/data?details')
        assert response.status_code == 200, \
            f'Error: {str(response.content)}'

        response = client.get('/api/data_uploading')
        assert response.status_code == 200, \
            f'Error: {str(response.content)}'

        response = client.get(f'/api/data/{pytest.data_id}')
        assert response.status_code == 200, \
            f'Error: {str(response.content)}'

        response = client.get(f'/api/data/{pytest.data_id}/file')
        assert response.status_code == 200, \
            f'Error: {str(response.content)}'

        response = client.get(f'/api/data/{pytest.data_id}/group/1/dataset/phase/image')
        assert response.status_code == 200, \
            f'Error: {str(response.content)}'

    def post_group_dataset_value(self, url, body):
        client = self.client
        headers = [{'Content-Type', 'application/octet-stream'}]
        response = client.put(url, data=body, headers=headers)
        assert response.status_code == 200, \
            f'Error: {str(response.content)}'

        expected = {
            "status": "ok"
        }

        result = json.loads(response.content)
        assert result == expected

    def api_post_data_group_value(self, use_threads=False, join_threads=True, sleep_after_post_s=0.):
        """Test /api/data/{data_id}/group/{group_id}/value post response."""

        data_id = pytest.data_id

        image_height = pytest.data_dims['height']
        image_width = pytest.data_dims['width']
        pytest.echo_times = np.arange(float(pytest.data_dims['depth']))
        pytest.slice_locations = np.arange(float(pytest.data_dims['groups']))

        array_size = (
            image_height,
            image_width,
            len(pytest.echo_times)*2,
            len(pytest.slice_locations)
        )

        # Put root datasets
        client = self.client
        url = '/api/data/' + data_id + '/dataset'
        headers = [{'Content-Type', 'application/octet-stream'}]
        response = client.put(
            url + '/echo_times/value',
            data=pytest.echo_times.tobytes(),
            headers=headers)
        assert response.status_code == 200, \
            f'Error: {str(response.content)}'

        response = client.put(
            url + '/slice_locations/value',
            data=pytest.slice_locations.tobytes(),
            headers=headers)
        assert response.status_code == 200, \
            f'Error: {str(response.content)}'

        pytest.data = np.random.randint(0, high=1000, size=array_size, dtype='uint16')

        post_threads = []

        t = time.time()
        # post data to each dataset of each group
        for group in range(len(pytest.slice_locations)):
            magnitude_data = pytest.data[:, :, :len(pytest.echo_times), group].copy()
            phase_data = pytest.data[:, :, len(pytest.echo_times):, group].copy()
            magnitude_body = magnitude_data.tobytes()
            phase_body = phase_data.tobytes()

            url = '/api/data/' + data_id
            url += '/group/' + str(group)
            magnitude_url = url + '/dataset/magnitude/value'
            phase_url = url + '/dataset/phase/value'

            if use_threads:
                post_thread = Thread(
                    target=self.post_group_dataset_value,args=(magnitude_url, magnitude_body))
                post_thread.start()
                post_threads.append(post_thread)

                post_thread = Thread(
                    target=self.post_group_dataset_value,args=(phase_url, phase_body))
                post_thread.start()
                post_threads.append(post_thread)
            else:
                self.post_group_dataset_value(magnitude_url, magnitude_body)
                self.post_group_dataset_value(phase_url, phase_body)
            
            time.sleep(sleep_after_post_s)

        if use_threads and join_threads:
            for post_thread in post_threads:
                post_thread.join()

        elapsed = time.time() - t

        # print('Elapsed time : {} seconds'.format(elapsed))

    def wait_until_uploaded(self):
        data_stored = False
        while not data_stored:
            response = self.client.get('/api/data/{}'.format(pytest.data_id))
            result = json.loads(response.content)
            assert 'status' in result
            assert 'stored' in result['status']
            data_stored = result['status']['stored']

    def open_file_for_reading(self, filepath: Path):

        file = None
        libver = 'latest'

        file_open = False
        while not file_open:
            try:
                file = h5py.File(filepath, 'r', libver=libver, swmr=True)
                file_open = True
            except ValueError as e:
                pass
            except OSError as e:
                pass

        return file


class TestProcessBase(TestData):

    """
        Base class for process unit tests.
    """

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        logger = logging.getLogger('cwa.base.test')
        logger.setLevel(logging.INFO)

        process_def_path = Path(__file__).parent / 'process_definition'

        cwd = os.getcwd()

        # create virtual environment for test processes
        # and install requirement for processes
        os.chdir(process_def_path)

        if platform.system() in ['Darwin', 'Linux']:
            assert os.system('python3 -m venv .venv') == 0
            assert os.system('.venv/bin/python -m pip install --upgrade pip') == 0
            assert os.system('.venv/bin/python -m pip install -r requirements.txt') == 0
        elif platform.system() == 'Windows':
            assert os.system('python.exe -m venv .venv') == 0
            assert os.system('.venv\\Scripts\\python.exe -m pip install --upgrade pip') == 0
            assert os.system('.venv\\Scripts\\python.exe -m pip install -r requirements.txt') == 0

        os.chdir(cwd)
