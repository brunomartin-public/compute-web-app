import unittest
import time
from multiprocessing import shared_memory, Process
from typing import List, Union
import struct

from cwa.base.data import Data, Dataset, DatasetValue, \
    GroupDatasetValue, Attribute, AttributeValues, GroupAttributeValues
from cwa.base.data_store import DataStore, DataCollectorItem
from cwa.base.data_supplier import DataSupplier

class DataSupplierMock(DataSupplier):
    """Class that mocks DataSupplier for tests."""
    
    def __init__(self, shm: shared_memory.SharedMemory, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.shm = shm
        
        # Created data group count 
        self._data: Data = None
        
    def create(self, data: Data):
        """This method mock data creation."""
        print(f'Handling file creation...')
        self._data = data
        
        expected_len = 1
        expected_len += len(data.datasets)
        expected_len += data.group_count * len(data.group_datasets)
        
        self.shm.buf[0:2] = struct.pack('H', expected_len)
        self.shm.buf[2] = 1
        
    def fill_common_dataset_value(self, value: DatasetValue):
        """ This method must be overriden """        
        print(f'Handling dataset {value.dataset_id}...')
        
        dataset_index = [x.name for x in self._data.datasets] \
            .index(value.dataset_id)
        shm_index = 3 + dataset_index
        self.shm.buf[shm_index] = 1
        
    def fill_group_dataset_value(self, value: GroupDatasetValue):
        """ This method must be overriden """    
        print(f'Handling dataset {value.dataset_id}'
            f' of group {value.group_id}...')
        
        dataset_index = [x.name for x in self._data.group_datasets] \
            .index(value.dataset_id)
        shm_index = 3 + len(self._data.datasets)
        shm_index += value.group_id * len(self._data.group_datasets)
        shm_index += dataset_index
        self.shm.buf[shm_index] = 1
        
    def fill_common_attribute_values(self, value: AttributeValues):
        """ This method must be overriden """
        pass
        
    def fill_group_attribute_values(self, value: GroupAttributeValues):
        """ This method must be overriden """
        pass

class TestDataStore(unittest.TestCase):
    """
        Test class for TestDataStore.
    """
    
    _data_path: str = 'data_path'
    _data_id: str = 'data_test'
    shm: shared_memory.SharedMemory = None
    
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        
        # Clear shared memory blocks in case they still exist
        shm_names = [
            cls._data_id[:16], cls._data_id[:16] + '-transfer',
            cls._data_id[:16] + '-rw', cls._data_id[:16] + '-status',
            cls._data_id[:16] + '-mock']
        for shm_name in shm_names:
            try:
                cls.shm = shared_memory.SharedMemory(
                    name=shm_name,
                    create=False)
                cls.shm.close()
                cls.shm.unlink()
            except FileNotFoundError:
                pass
        
        cls.shm = shared_memory.SharedMemory(
            name=cls._data_id[:16] + '-mock',
            create=True,
            size=1024)
        cls.shm.buf[:] = bytes(cls.shm.size)
        
    @classmethod
    def tearDownClass(cls):
        cls.shm.close()
        cls.shm.unlink()
        
    def test_timeout_errors(self):
        """Test data store errors
        """
        
        data_supplier = DataSupplierMock(self.shm, self._data_id)
        data_store = DataStore(data_supplier, lock_timeout=1.0)
        
        groups = 2
        
        data = Data(
            'data_id_1','name', 'version', 'post_time',
            group_count=groups,
            group_datasets=[Dataset('dataset_id_3', int, (4, )),
                            Dataset('dataset_id_4', int, (5, ))],)
        
        # Test transfer lock timeout
        data_store._transfer_lock.lock()
        
        try:
            data_store.create(data)
            assert False, "Shall have thrown TimeoutError"
        except TimeoutError:
            data_store._transfer_lock.unlock()
        
        # Test read/write lock timeout
        data_store._read_write_lock.lock()
        
        try:
            data_store.create(data)
            assert False, "Shall have thrown TimeoutError"
        except TimeoutError:        
            data_store._transfer_lock.unlock()
            data_store._read_write_lock.unlock()
        
        # Test no timeout error
        data_store._lock_timeout = 10
        data_store.create(data)
        assert data_supplier.shm.buf[2] == 1, 'Data shall be created'
        
        print(f'Waiting for collector process to finish...')
        data_store.wait_for_collector(True)
        print(f'Collector process has finish...')
    
    def test_create_and_fill(self):
        """Test data store in mono process context
        """
        
        # Filling shm with zeros seems to be required
        self.shm.buf[:] = bytes(self.shm.size)
        
        groups = 16
        
        data_supplier = DataSupplierMock(self.shm, self._data_id)
        data_store = DataStore(data_supplier, lock_timeout=1000)
        
        assert list(self.shm.buf) == [0]*self.shm.size, \
            'SHM buffer shall be null'
        
        # Queue data creation item
        data = Data(
            'data_id_1', 'name', 'version', 'post_time',
            datasets=[Dataset('dataset_id_1', int, (2, )),
                      Dataset('dataset_id_2', int, (3, ))],
            attributes=[Attribute('attr_1', int),
                        Attribute('attr_2', str)],
            group_count=groups,
            group_datasets=[Dataset('dataset_id_3', int, (4, )),
                            Dataset('dataset_id_4', int, (5, ))],
            group_attributes=[Attribute('attr_3', int),
                        Attribute('attr_4', str)])
        data_store.create(data)
        
        assert self.shm.buf[2], 'Creation flag shall be set'
        expected_len, = struct.unpack('H', self.shm.buf[0:2])
        
        data_value = DatasetValue('dataset_id_1', [0, 1])
        data_store.put(data_value)
        
        data_value = DatasetValue('dataset_id_2', [0, 1, 2])
        data_store.put(data_value)
        
        attribute_values = AttributeValues(
            ['attr_1', 'attr_2'], [42, 'test'])
        data_store.put(attribute_values)
        
        # Check priorization works for target item contents
        dataset_value = GroupDatasetValue(
            0, 'dataset_id_1', [0, 1, 2, 3])        
        assert DataCollectorItem(-1, data) < \
            DataCollectorItem(0, dataset_value), 'Wrong item priority'
        
        # Queue dataset value item
        for group_id in range(groups):
            print(f'Queuing attributes of group {group_id}...')
            attribute_values = GroupAttributeValues(
                group_id,
                AttributeValues(['attr_3', 'attr_4'], [42, 'test']))
            data_store.put(attribute_values)
            
            print(f'Queuing dataset dataset_id_3 of group {group_id}...')
            dataset_value = GroupDatasetValue(
                group_id, 'dataset_id_3', [0, 1, 2, 3])
            data_store.put(dataset_value)
            
            print(f'Queuing dataset dataset_id_4 of group {group_id}...')
            dataset_value = GroupDatasetValue(
                group_id, 'dataset_id_4', [0, 1, 2, 3, 4])
            data_store.put(dataset_value)
            
        # Wait until all data have been filled        
        start_time = time.time()
        while not data_store.is_data_filled() and \
            time.time() - start_time < 10:
            time.sleep(0.1)
            continue
        
        # Check mock supplier data is filled
        flags = data_supplier.shm.buf[2:2+expected_len]
        assert list(flags) == [1]*expected_len, 'Data shall be filled'
        print(f'Data is filled.')
        
        print(f'Waiting for collector process to finish...')
        data_store.wait_for_collector()
        print(f'Collector process has finish...')
            
    @staticmethod
    def _data_store_process(data_id: str,
                            shm: shared_memory.SharedMemory,
                            groups: int, start: int, stride: int):
        
        print(f'Starting data store {start}...')
        data_supplier = DataSupplierMock(shm, data_id)
        data_store = DataStore(data_supplier)
        
        for group_id in range(start, groups, stride):
            print(f'Queuing attributes of group {group_id}...')
            attribute_values = GroupAttributeValues(
                group_id,
                AttributeValues(['attr_3', 'attr_4'], [42, 'test']))
            data_store.put(attribute_values)
            
            print(f'Queuing dataset dataset_id_3 of group {group_id}...')
            dataset_value = GroupDatasetValue(
                group_id, 'dataset_id_3', [0, 1, 2, 3])
            data_store.put(dataset_value)
            
            print(f'Queuing dataset dataset_id_4 of group {group_id}...')
            dataset_value = GroupDatasetValue(
                group_id, 'dataset_id_4', [0, 1, 2, 3, 4])
            data_store.put(dataset_value)

    def test_create_and_fill_mp(self):
        """Test data store in multi process context
        """
        
        # Filling shm with zeros seems to be required
        self.shm.buf[:] = bytes(self.shm.size)
        
        groups = 30
        workers = 6
        
        data_supplier = DataSupplierMock(self.shm, self._data_id)
        data_store = DataStore(data_supplier, lock_timeout=10)
        
        processes: List[Process] = []
        for i in range(workers):
            args=(self._data_id, self.shm, groups, i, workers)
            process = Process(target=self._data_store_process, args=args)
            processes.append(process)
        
        assert list(self.shm.buf) == [0]*self.shm.size, \
            'SHM buffer shall be null'
        
        # Queue data creation item
        data = Data(
            'data_id_1', 'name', 'version', 'post_time',
            datasets=[Dataset('dataset_id_1', int, (2, )),
                      Dataset('dataset_id_2', int, (3, ))],
            attributes=[Attribute('attr_1', int, 42),
                        Attribute('attr_2', str, 'test')],
            group_count=groups,
            group_datasets=[Dataset('dataset_id_3', int, (4, )),
                            Dataset('dataset_id_4', int, (5, ))],
            group_attributes=[Attribute('attr_3', int),
                        Attribute('attr_4', str)])
        data_store.create(data)
        
        assert self.shm.buf[2], 'Creation flag shall be set'
        expected_len, = struct.unpack('H', self.shm.buf[0:2])
        
        data_value = DatasetValue('dataset_id_1', [0, 1])
        data_store.put(data_value)
        
        data_value = DatasetValue('dataset_id_2', [0, 1, 2])
        data_store.put(data_value)        
        
        attribute_values = AttributeValues(
            ['attr_1', 'attr_2'], [42, 'test'])
        data_store.put(attribute_values)
        
        for process in processes:
            process.start()
            
        # Wait until all data have been filled        
        start_time = time.time()
        while not data_store.is_data_filled() and \
            time.time() - start_time < 10:
            time.sleep(0.1)
            continue
        
        # Check mock supplier data is filled
        flags = data_supplier.shm.buf[2:2+expected_len]
        assert list(flags) == [1]*expected_len, 'Data shall be filled'
        print(f'Data is filled.')
        
        print(f'Waiting for group processes...')
        for process in processes:
            process.join()
        print(f'Group processes stopped.')
                
        print(f'Waiting for collector process to finish...')
        data_store.wait_for_collector()
        print(f'Collector process has finish...')
        
        assert list(data_supplier.shm.buf[2:2+expected_len]) == \
            [1]*expected_len, 'SHM buffer shall be ones'
