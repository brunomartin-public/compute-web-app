import unittest
from pathlib import Path
import uuid
import time

import numpy as np

from cwa.base.data import Data, Dataset, Attribute, GroupDatasetValue
from cwa.base.data_manager import DataManager

class TestDataManager(unittest.TestCase):
    """
        Test class for ProcessManager.
    """
    
    data_path: Path
    data: Data
    
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        
        # define application paths
        temp_path = Path(__file__).parent / 'tmp'
        cls.data_path = temp_path / 'data'

        # create directory of they not exist
        cls.data_path.mkdir(exist_ok=True, parents=True)
        
        cls.data = Data(
            name='name_1',
            datasets=[
                Dataset('root_dataset_1', 'f', (5, 10, 15)),
                Dataset('root_dataset_2', 'u2', (15, 20, 30))
            ],
            attributes=[
                Attribute('attr_1', 'str', 'test'),
                Attribute('attr_2', 'i2', 42)
            ],
            group_count=10,
            group_datasets=[
                Dataset('dataset_1', 'd', (2, 4, 8)),
                Dataset('dataset_2', 'u4', (3, 6, 12))
            ]
        )
        
    def test_add_data(self):
        """ Test add data. """
        
        # Create a data manager        
        data_manager = DataManager(self.data_path)
        
        # Add and load data
        self.data.id = data_manager.add(self.data)
        loaded_data = data_manager.load(self.data.id)

        # Convert dtype from string to dtype for comparison
        for attribute1, attribute2 in \
            zip(self.data.attributes, loaded_data.attributes):
            attribute1.dtype = np.dtype(attribute1.dtype)
            attribute2.dtype = np.dtype(attribute2.dtype)
            
        for dataset1, dataset2 in \
            zip(self.data.datasets, loaded_data.datasets):
            dataset1.dtype = np.dtype(dataset1.dtype)
            dataset2.dtype = np.dtype(dataset2.dtype)
            
        for dataset1, dataset2 in \
            zip(self.data.group_datasets, loaded_data.group_datasets):
            dataset1.dtype = np.dtype(dataset1.dtype)
            dataset2.dtype = np.dtype(dataset2.dtype)
        
        # Compare two data
        assert self.data == loaded_data, \
            'Loaded data does not match added data'

    def test_fill_dataset(self):
        """ Test add data. """
        
        # Create a data manager        
        data_manager = DataManager(self.data_path)
        
        # Add and load data
        self.data.id = data_manager.add(self.data)
        
        group_dataset_values = {}
        for group_id in range(self.data.group_count):
            group_dataset_values[group_id] = {}
            for dataset in self.data.group_datasets:
                # Multiple by 1024 to avoid discriminating between float
                # and integers for this test
                value = (1024 * np.random.random(size=dataset.shape)) \
                    .astype(dataset.dtype)
                value_bytes = value.tobytes()
                dataset_value = GroupDatasetValue(
                    group_id, dataset.name, value_bytes)
                data_manager.fill_group_dataset(self.data.id, dataset_value)
                group_dataset_values[group_id][dataset.name] = value
                
        # Wait until all data is stored
        start_time = time.time()
        while time.time() - start_time < 5:
            status = data_manager.load(self.data.id).status
            if status.stored:
                break
            
        # If data is still not filled, throw an error
        status = data_manager.load(self.data.id).status
        assert status.stored, 'Data as not been entirely stored'
        
        # Check constency of stored values
        for group_id in range(self.data.group_count):
            for dataset in self.data.group_datasets:
                value = data_manager.load_dataset_value(
                    self.data.id, group_id, dataset.name)
                expected_value = \
                    group_dataset_values[group_id][dataset.name]
                status = data_manager.load(self.data.id).status
                assert (value == expected_value).all(), \
                    'Value mismatched'
