import unittest
from datetime import datetime, timezone
from pathlib import Path

from cwa.base.data import Data, Dataset, Attribute
from cwa.base.hdf5_file import H5File, WrongDataSizeException

import h5py
import numpy as np

class TestHdf5File(unittest.TestCase):
    """
        Test class for HDF5 file.
    """

    # Test data
    filepath = 'test_hdf5_file.h5'
    version = '1.0'
    data: Data = None
    expected_attributes: dict = None
    expected_groups_attributes: dict = None

    @classmethod
    def setUpClass(cls):
        
        cls.filepath = 'test_hdf5_file.h5'
        cls.version = '1.0'
        
        cls.data = Data(
            name='name',
            version=cls.version,
            datasets=[
                Dataset('dataset_1', 'f', (10, 15, 20)),
                Dataset('dataset_2', 'f', (15, 20, 30))
            ],
            attributes=[
                Attribute('attr_1', 'f', 41.),
                Attribute('attr_2', 'f', 42.),
                Attribute('attr_3', str, 'test'),
            ],
            group_count=7,
            group_datasets=[
                Dataset('dataset_3', 'f', (5, 10, 15)),
                Dataset('dataset_4', 'f', (15, 20, 25))
            ]
        )

        cls.expected_attributes = [
            {'name': 'cwa_name', 'value': cls.data.name},
            {'name': 'cwa_version', 'value': cls.version},
            {'name': 'cwa_post_time', 'value': ''},

            {'name': 'cwa_stored', 'value': False},
            {'name': 'cwa_store_progress', 'value': 0},
            {'name': 'cwa_store_start_time', 'value': ''},
            {'name': 'cwa_store_end_time', 'value': ''},
        ]

        cls.expected_groups_attributes = [
            {'name': 'cwa_stored', 'value': False},
        ]
    
    def check_file(self, data: Data, filepath: Path):
        
        # Open file for reading (default mode)
        file = h5py.File(filepath)
        
        # Check consistency of dataset and group count
        assert len(file) == data.group_count + len(data.datasets)
        
        # Check consistency of root datasets
        for dataset in data.datasets:
            assert dataset.name in file
            assert file[dataset.name].dtype == dataset.dtype
            assert file[dataset.name].shape == dataset.shape
            
        # Check consistency of root attributes
        for attribute in data.attributes:
            assert attribute.name in file.attrs
            if not isinstance(file.attrs[attribute.name], str):
                assert file.attrs[attribute.name].dtype == \
                    attribute.dtype
            assert file.attrs[attribute.name] == attribute.value
            
        # Check CWA dedicated attributes
        for attribute in self.expected_attributes:
            assert attribute['name'] in file.attrs
            if isinstance(attribute['value'], str):
                assert file.attrs[attribute['name']].decode() == attribute['value']
            else:
                assert file.attrs[attribute['name']] == attribute['value']
        
        # Check consistency of groups and their datasets
        for j in range(data.group_count):
            group_name = '%03d' % j
            assert group_name in file
            group = file[group_name]
            for dataset in data.group_datasets:
                assert dataset.name in group
                assert group[dataset.name].shape == dataset.shape
                assert group[dataset.name].dtype == dataset.dtype
                
            for attribute in self.expected_groups_attributes:
                assert attribute['name'] in file.attrs
                assert attribute['value'] == file.attrs[attribute['name']]
        
        file.close()

    def test_create(self):
        """ Test file creation. """
        # Ensure that file does not already exists
        Path(self.filepath).unlink(missing_ok=True)

        # Create a file
        self.data.creation_time = datetime.now(timezone.utc).isoformat()
        file = H5File.create_file(
            data=self.data, filepath=self.filepath)
        assert file.swmr_mode == True
        file.close()

        # Update expected post time
        add_time_attr = next(x for x in self.expected_attributes \
            if x['name'] == 'cwa_post_time')
        add_time_attr['value'] = self.data.creation_time

        # Check created file and delete it
        self.check_file(self.data, self.filepath)
        Path(self.filepath).unlink()
        
    def test_create_but_exist(self):
        """ Test file creation raises an error if file exists. """
        # Ensure that file does not already exists
        Path(self.filepath).unlink(missing_ok=True)

        # Create a file
        file = H5File.create_file(
            data=self.data, filepath=self.filepath)
        assert file.swmr_mode == True
        file.close()
        
        # Update expected post time
        add_time_attr = next(x for x in self.expected_attributes \
            if x['name'] == 'cwa_post_time')
        add_time_attr['value'] = self.data.creation_time
        
        # Check created file
        self.check_file(self.data, self.filepath)
        
        # Check trying to create another file with same name throws an error
        try:
            file = H5File.create_file(
                data=self.data, filepath=self.filepath)
            assert False, \
                'FileExistsError exception shall have been thrown'
        except FileExistsError:
            pass
        
        # Delete file
        Path(self.filepath).unlink()
    
    def test_fill_dataset(self):
        # Ensure that file does not already exists
        Path(self.filepath).unlink(missing_ok=True)

        # Create a file
        file = H5File.create_file(
            data=self.data, filepath=self.filepath)
        assert file.swmr_mode == True
        file.close()

        # Update expected post time
        add_time_attr = next(x for x in self.expected_attributes \
            if x['name'] == 'cwa_post_time')
        add_time_attr['value'] = self.data.creation_time

        # Check created file
        self.check_file(self.data, self.filepath)

        # Check filled file
        file = h5py.File(self.filepath)
        for dataset in self.data.datasets:
            assert file[dataset.name].attrs['cwa_stored'] == False
        file.close()

        for dataset in self.data.datasets:
            file = H5File.open_file_for_writing(self.filepath)
            data = np.random.random(size=dataset.shape) \
                .astype(dataset.dtype)
            file.update_group_dataset_value(
                '/', dataset.name, data.tobytes())
            file.close()

            file = h5py.File(self.filepath)
            assert (file[dataset.name][:] == data).all(), \
                'Data does not match'
            assert file[dataset.name].attrs['cwa_stored'] == True, \
                'Stored flag has not been set'
            file.close()

        # Check wrong size generate error
        file = H5File.open_file_for_writing(self.filepath)
        dataset = self.data.group_datasets[0]
        data = np.random.random(size=dataset.shape) \
            .astype(dataset.dtype)
        try:
            file.update_group_dataset_value(
                0, dataset.name, data[:-2].tobytes())
            assert False, 'FileExistsError exception shall have been thrown'
        except WrongDataSizeException:
            pass
        file.close()

        # Check file is not stamped as stored
        file = h5py.File(self.filepath)
        assert file.attrs['cwa_stored'] == False, \
            'Stored flag shall not been set'
        file.close()

        groups_count = self.data.group_count
        for j in range(groups_count):
            group_name = '%03d' % j

            # Check group is not stamped as stored
            file = h5py.File(self.filepath)
            assert file.attrs['cwa_stored'] == False
            group = file[group_name]
            assert group.attrs['cwa_stored'] == False
            file.close()

            for dataset in self.data.group_datasets:
                # Check dataset is not stamped as stored
                file = h5py.File(self.filepath)
                group = file[group_name]
                assert not group[dataset.name].attrs['cwa_stored'], \
                    'Stored flag shall not been set'
                file.close()

                file = H5File.open_file_for_writing(self.filepath)
                data = np.random.random(size=dataset.shape) \
                    .astype(dataset.dtype)
                file.update_group_dataset_value(
                    j, dataset.name, data.tobytes())
                file.close()

                # Check dataset is now stamped as stored
                file = h5py.File(self.filepath)
                group = file[group_name]
                assert (group[dataset.name][:] == data).all()
                assert group[dataset.name].attrs['cwa_stored'], \
                    'Stored flag shall have been set'
                file.close()
            
            # Check group is now stamped as stored
            file = h5py.File(self.filepath)
            group = file[group_name]
            assert group.attrs['cwa_stored'], \
                'Stored flag shall have been set'
            file.close()

        # Check file is now stamped as stored
        file = h5py.File(self.filepath)
        assert file.attrs['cwa_stored'] == True, \
            'Stored flag shall have been set'
        file.close()
