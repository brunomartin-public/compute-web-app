import unittest
from pathlib import Path
import json
import time
import random
from multiprocessing import Process, Pool
from typing import List

from cwa.base.lockable_file import LockableFile

class TestLockableFile(unittest.TestCase):
    """Test class for LockableFile."""
    
    filepath = Path('test.json')

    @classmethod
    def setUpClass(cls):
        Path('test.lock').unlink(missing_ok=True)

    @staticmethod
    def run_create(filepath: Path, delay: float, data: dict):
        time.sleep(delay)
        file = LockableFile(filepath)
        file.lock_and_write_json(data)

    @staticmethod
    def run_increment(filepath: Path):
        def update(data: dict):
            data['count'] += 1
        file = LockableFile(filepath)
        file.lock_and_update_json(update)

    @staticmethod
    def run_increment_internal(filepath: Path):
        file = LockableFile(filepath)
        lock_file_id = file._lock_file()
        with open(filepath) as f:
            data = json.load(f)
        data['count'] += 1
        with open(filepath, 'w') as f:
            json.dump(data, f)
        file._unlock_file(lock_file_id)

    @staticmethod
    def run_unlink(filepath: Path):
        file = LockableFile(filepath)
        file.unlink()

    def test_create(self):
        filepath = self.filepath
        file = LockableFile(filepath)

        data = {'count': 0}
        file.lock_and_write_json(data)
        assert Path(filepath).exists()

        with open(filepath) as f:
            content = f.read()
            assert json.loads(content) == data

        read_data = file.lock_and_read_json({})
        assert read_data == data

    def test_create_mp(self):
        filepath = self.filepath
        file = LockableFile(filepath)

        filepath.unlink(missing_ok=True)

        data = {
            'count': random.randint(0, 100),
            'list': list(range(30)),
        }

        process = Process(
            target=self.run_create,
            args=(filepath, 2, data),
            daemon=True)
        process.start()

        assert not Path(filepath).exists(), 'File shall not exist yet'
        read_data = file.lock_and_read_json({})
        assert read_data == {}, 'Read data does not match default'

        while not Path(filepath).exists():
            time.sleep(0.1)

        read_data = file.lock_and_read_json({})
        assert read_data == data, 'Read data does not match data'

        process.join()

    def test_update_mp(self):
        filepath = self.filepath
        file = LockableFile(filepath)

        # Generate data large enough to check if json is parsed as
        # expected
        data = {
            'count': 0,
            'data': [random.choice(range(100)) for _ in range(50)]
        }
        file.lock_and_write_json(data)

        processes = 5

        with Pool(processes) as p:
            p.map(self.run_increment_internal, [filepath]*processes)
            p.map(self.run_increment, [filepath]*processes)

        read_data = file.lock_and_read_json({})
        excepted_data = data
        excepted_data['count'] = 2*processes
        assert read_data == excepted_data, 'Read data does not match data'


    def test_unlink(self):
        filepath = self.filepath
        file = LockableFile(filepath)

        data = {'count': 42}
        file.lock_and_write_json(data)

        assert Path(filepath).exists()

        lock_file_id = file._lock_file()

        process = Process(
            target=self.run_unlink,
            args=(filepath, ),
            daemon=True)
        process.start()

        assert Path(filepath).exists()

        file._unlock_file(lock_file_id)
        process.join()

        assert not Path(filepath).exists()
