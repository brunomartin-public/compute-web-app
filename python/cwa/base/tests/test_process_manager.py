import unittest
from pathlib import Path
import os
import platform
from datetime import datetime, timezone
import time
import random
import string
import copy

import numpy as np
import h5py

from cwa.base.data import Data, Dataset, Attribute
from cwa.base.hdf5_file import H5File
from cwa.base.process_manager import ProcessManager, Process, \
    ProcessStatus, ProcessParameter, ProcessAction
from cwa.base.process_def_manager import ProcessDefManager


class TestProcessManager(unittest.TestCase):
    """
        Test class for ProcessManager.
    """

    data_path: Path
    process_def_path: Path
    process_path: Path
    process_def_manager: ProcessDefManager
    process_manager: ProcessManager

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.process_def_path = Path(__file__).parent.parent.parent \
            / 'tests' / 'process_definition'

        cwd = os.getcwd()

        # create virtual environment for test processes
        # and install requirement for processes
        os.chdir(cls.process_def_path)

        if platform.system() in ['Darwin', 'Linux']:
            assert os.system('python3 -m venv .venv') == 0
            assert os.system('.venv/bin/python -m pip install --upgrade pip') == 0
            assert os.system('.venv/bin/python -m pip install -r requirements.txt') == 0
        elif platform.system() == 'Windows':
            assert os.system('python.exe -m venv .venv') == 0
            assert os.system('.venv\\Scripts\\python.exe -m pip install --upgrade pip') == 0
            assert os.system('.venv\\Scripts\\python.exe -m pip install -r requirements.txt') == 0

        os.chdir(cwd)
        
        cls.data_id = 'test_hdf5_file'
        cls.version = '1.0'
        
        cls.process_def_id_1 = 'process_test_1'
        cls.process_def_id_2 = 'process_test_2'
        cls.dataset_to_process = 'dataset_1'
        
        cls.data = Data(
            name='name',
            version=cls.version,
            datasets=[
                Dataset('dataset_1', 'f', (5, 10, 15)),
                Dataset('dataset_2', 'u2', (15, 20, 30)),
            ],
            attributes=[
                Attribute('pause_s', 'f', 0.1),
                Attribute('dataset_to_process', str, cls.dataset_to_process),
                Attribute('errored_group', int, -1),
                Attribute('error_message', str, ''),
                Attribute('early_process', bool, False),
                Attribute('early_result', bool, False),
            ],
            group_count=10,
            group_datasets=[
                Dataset('dataset_1', 'f', (5, 10, 15)),
                Dataset('dataset_2', 'u2', (15, 20, 30)),
            ]
        )

        cls.groups_datasets_value = []
        for _ in range(cls.data.group_count):
            group_datasets_value = {}
            for dataset in cls.data.group_datasets:
                if np.issubdtype(dataset.dtype, np.integer):
                    group_datasets_value[dataset.name] = np.random.randint(
                        0, high=1000, size=dataset.shape,
                        dtype=dataset.dtype)
                elif np.issubdtype(dataset.dtype, np.floating):
                    group_datasets_value[dataset.name] = \
                        np.random.rand(*dataset.shape) \
                        .astype(dataset.dtype)
            
            cls.groups_datasets_value.append(group_datasets_value)
            
        # define application paths
        temp_path = Path(__file__).parent / 'tmp'
        cls.data_path = temp_path / 'data'
        cls.result_path = temp_path / 'result'
        
        # create directory of they not exist
        cls.data_path.mkdir(exist_ok=True, parents=True)
        cls.process_def_path.mkdir(exist_ok=True, parents=True)
        cls.result_path.mkdir(exist_ok=True, parents=True)
        
        cls.process_def_manager = ProcessDefManager(
            cls.process_def_path)
        cls.process_manager = ProcessManager(
            cls.data_path, cls.process_def_manager, cls.result_path)
        
    def check_process_result_file(self, process_id: str, mean_factor: float):
        """Check process 1 result files."""
        
        # open result file
        result_filepath = self.result_path / (process_id + '.h5')
        
        file = h5py.File(result_filepath, 'r')
        
        for index, group_datasets_value in enumerate(self.groups_datasets_value):
            # check result file as one group for each group from 000 to
            # 0XX where XX is the total number of groups
            group_name = '%03d' % index
            assert group_name in file
            
            # check dataset are present
            assert 'mean' in file[group_name]
            assert 'std' in file[group_name]
            assert 'min' in file[group_name]
            assert 'max' in file[group_name]
            
            result_mean = file[group_name]['mean'][:]
            result_std = file[group_name]['std'][:]
            result_min = file[group_name]['min'][:]
            result_max = file[group_name]['max'][:]
            
            dataset_value = group_datasets_value[self.dataset_to_process]
            
            expected_mean = \
                (np.mean(dataset_value, axis=2) * mean_factor) \
                .astype(dataset_value.dtype)
            expected_std = np.std(dataset_value, axis=2) \
                .astype(dataset_value.dtype)
            expected_min = np.min(dataset_value, axis=2)
            expected_max = np.max(dataset_value, axis=2)
            
            assert (result_mean == expected_mean).all()
            assert (result_std == expected_std).all()
            assert (result_min == expected_min).all()
            assert (result_max == expected_max).all()
            
        file.close()
        
    def test_list_process_defs(self):
        """This test list process definitions and check them."""
        
        process_def_ids = self.process_def_manager.load_ids()
        
        assert len(process_def_ids) == 3, 'Shall have 3 process defs'
        expected_process_def_ids = [
            'process_test_0', 'process_test_1', 'process_test_2']
        
        assert set(process_def_ids) == set(expected_process_def_ids), \
            'Process ids does not match'
        
    def test_run_process(self):
        """This test run a process."""

        data_filepath = self.data_path / (self.data_id + '.h5')

        # Ensure that file does not already exists
        Path(data_filepath).unlink(missing_ok=True)
        
        data = copy.deepcopy(self.data)

        # Create a file
        data.creation_time = datetime.now(timezone.utc).isoformat()
        file = H5File.create_file(
            data=data, filepath=data_filepath)
        assert file.swmr_mode == True

        for index, group_datasets_value in \
            enumerate(self.groups_datasets_value):
            for name, value in group_datasets_value.items():
                file.update_group_dataset_value(
                    index, name, value.tobytes())

        file.close()
        
        process_def = self.process_def_manager. \
            load(self.process_def_id_1)

        mean_factor = 2.0
        parameters = [
            ProcessParameter('mean_factor', 'd', mean_factor),
            ProcessParameter('step_count', 'u2', 100),
            ProcessParameter('end_message', 'str', 'Yahoo!')]
        process = Process(
            self.data_id, process_def.id, '1.0', parameters)

        process_id = self.process_manager.add(process)
        
        # Load and check expected shared properties and values
        status = self.process_manager.load(process_id).status
        assert not status.running and not status.done \
            and not status.suspended and status.progress == 0 \
            and status.log == '' and len(status.errors) == 0, \
            'Unexpected process status'

        # Start process and check status
        action = ProcessAction('start', process_id, '1.0')
        action_id = self.process_manager.do_action(action)
        status = self.process_manager.load(process_id).status
        assert status.running and not status.done \
            and not status.suspended and len(status.errors) == 0, \
            'Unexpected process status'
        
        time.sleep(0.5)
        
        # Suspended and check
        action = ProcessAction('suspend', process_id, '1.0')
        action_id = self.process_manager.do_action(action)
        status = self.process_manager.load(process_id).status
        assert status.running and not status.done \
            and status.suspended and len(status.errors) == 0, \
            'Unexpected process status'
        
        time.sleep(0.5)

        # Resume and check
        action = ProcessAction('resume', process_id, '1.0')
        action_id = self.process_manager.do_action(action)
        status = self.process_manager.load(process_id).status
        assert status.running and not status.done \
            and not status.suspended and len(status.errors) == 0, \
            'Unexpected process status'

        start_time = time.time()
        while not status.done and time.time() - start_time < 20:
            status = self.process_manager.load(process_id).status
            time.sleep(0.5)
            print(f'progress: {status.progress}%')

        status = self.process_manager.load(process_id).status
        assert not status.running and status.done \
            and not status.suspended and len(status.errors) == 0, \
            'Unexpected process status'

        self.check_process_result_file(process_id, mean_factor)

        # Reset and check
        action = ProcessAction('reset', process_id, '1.0')
        action_id = self.process_manager.do_action(action)
        status = self.process_manager.load(process_id).status
        assert not status.running and not status.done \
            and not status.suspended and status.progress == 0 \
            and status.log == '' and len(status.errors) == 0, \
            'Unexpected process status'
        
        action = ProcessAction('start', process_id, '1.0')
        action_id = self.process_manager.do_action(action)
        status = self.process_manager.load(process_id).status
        assert status.running and not status.done \
            and not status.suspended and len(status.errors) == 0, \
            'Unexpected process status'
        
        time.sleep(0.5)
        action = ProcessAction('stop', process_id, '1.0')
        action_id = self.process_manager.do_action(action)
        status = self.process_manager.load(process_id).status
        assert not status.running and not status.done \
            and not status.suspended, \
            'Unexpected process status'

    def test_run_errored_process(self):
        """This test run errored process."""

        data_filepath = self.data_path / (self.data_id + '.h5')

        # Ensure that file does not already exists
        Path(data_filepath).unlink(missing_ok=True)

        # Configure process to issue errors
        data = copy.deepcopy(self.data)
        errored_group = 7
        attribute = next(
            (x for x in data.attributes \
                if x.name == 'errored_group'),
            None)
        attribute.value = errored_group

        error_message = ''.join(
            random.choice(string.ascii_letters) for i in range(32))
        attribute = next(
            (x for x in data.attributes \
                if x.name == 'error_message'),
            None)
        attribute.value = error_message
        half_error_message = error_message[:int(len(error_message)/2)]

        # Create a file with all options
        data.creation_time = datetime.now(timezone.utc).isoformat()
        file = H5File.create_file(
            data=data, filepath=data_filepath)
        assert file.swmr_mode == True

        for index, group_datasets_value in enumerate(self.groups_datasets_value):
            for name, value in group_datasets_value.items():
                file.update_group_dataset_value(
                    index, name, value.tobytes())

        file.close()
        
        process_def = self.process_def_manager. \
            load(self.process_def_id_1)

        mean_factor = 2.0        
        parameters = [
            ProcessParameter('mean_factor', 'd', mean_factor),
            ProcessParameter('step_count', 'u2', 100),
            ProcessParameter('end_message', 'str', 'Yahoo!')]
        process = Process(
            self.data_id, process_def.id, '1.0', parameters)

        process_id = self.process_manager.add(process)
        self.process_manager.do_action(
            ProcessAction('start', process_id, '1.0'))

        status = self.process_manager.load(process_id).status
        while status.running:
            status = self.process_manager.load(process_id).status
            time.sleep(0.5)
            print(f'progress: {status.progress}%')

        # Check process status
        assert not status.running and not status.done \
            and not status.suspended \
            and status.progress == errored_group*self.data.group_count \
            and len(status.errors) == 3, \
            'Unexpected process status'

        assert status.errors == [
            half_error_message, error_message,
            'Bad return code : 255'], \
                'Unexpected error messages'

    def test_run_early_process(self):
        """This test run a process using early process feature."""

        data_filepath = self.data_path / (self.data_id + '.h5')

        # Ensure that file does not already exists
        Path(data_filepath).unlink(missing_ok=True)

        # Configure process for early processing
        data = copy.deepcopy(self.data)
        attribute = next(
            (x for x in data.attributes \
                if x.name == 'pause_s'),
            None)
        attribute.value = 0.2
        
        attribute = next(
            (x for x in data.attributes \
                if x.name == 'early_process'),
            None)
        attribute.value = True
        
        # Create a file with all options
        data.creation_time = datetime.now(timezone.utc).isoformat()
        file = H5File.create_file(
            data=data, filepath=data_filepath)
        assert file.swmr_mode == True

        # Create and start process before sending data to check
        # early process behaves as expected
        process_def = self.process_def_manager. \
            load(self.process_def_id_1)

        mean_factor = 1.5
        parameters = [
            ProcessParameter('mean_factor', 'd', mean_factor),
            ProcessParameter('step_count', 'u2', 100),
            ProcessParameter('end_message', 'str', 'Yahoo!')]
        process = Process(
            self.data_id, process_def.id, '1.0', parameters)

        process_id = self.process_manager.add(process)

        # Load and check expected shared properties and values
        status = self.process_manager.load(process_id).status
        assert not status.running and not status.done \
            and not status.suspended and status.progress == 0 \
            and status.log == '' and len(status.errors) == 0, \
            'Unexpected process status'

        # Start process and check status
        action = ProcessAction('start', process_id, '1.0')
        action_id = self.process_manager.do_action(action)
        status = self.process_manager.load(process_id).status
        assert status.running and not status.done \
            and not status.suspended and len(status.errors) == 0, \
            'Unexpected process status'

        # Now fill data and check progress is getting updated
        for index, group_datasets_value in enumerate(self.groups_datasets_value):

            # Check that progress has not been updated yet
            start_time = time.time()
            progress = -1
            while time.time() < start_time + 10:
                status = self.process_manager.load(process_id).status
                if status.progress == index*10:
                    progress = status.progress
                    break
                time.sleep(0.5)
            assert progress != -1, \
                'Progress shall have not been updated yet'

            for name, value in group_datasets_value.items():
                file.update_group_dataset_value(
                    index, name, value.tobytes())
            
            # Currently, process does not see file updating
            # try to flush the whole file
            # TODO: find why group is not flush and flush the whole file
            #   is required
            file.flush()

            # Check that progress has been updated after group
            # datasets have been stored
            start_time = time.time()
            progress = -1
            while time.time() < start_time + 10:
                status = self.process_manager.load(process_id).status
                if status.progress == (index+1)*10:
                    progress = status.progress
                    break         
                time.sleep(0.5)   
            assert progress != -1, 'Progress shall have been update'

        file.close()

        start_time = time.time()
        process_done = False
        while time.time() < start_time + 5:
            status = self.process_manager.load(process_id).status
            process_done = status.done
            if status.done:
                break
            time.sleep(0.5)
        
        assert process_done, 'Progress shall have been done'

        # process 2 uses process 1 data processing, so check results
        # the same way
        self.check_process_result_file(process_id, mean_factor)
