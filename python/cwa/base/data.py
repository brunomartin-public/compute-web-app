import json
from typing import List, Tuple, Union
from dataclasses import dataclass, field

@dataclass
class Dataset:
    """Class that defines a dataset."""

    # Name of the dataset
    name: str = ''
    
    # Data type of the dataset
    dtype: str = ''
    
    # Shape of the dataset
    shape: Tuple[int, ...] = field(default_factory=lambda: ())
    
@dataclass
class Attribute:
    """Class that defines an attribute."""

    # Name of the dataset
    name: str = ''
        
    # Data type of the dataset
    dtype: str = ''

    # Value
    value: any = None
    
@dataclass
class DataStatus:
    """Class that defines data queued and stored status."""

    # Tell if data has been totally queued
    queued: int = False

    # Queue progress in percent
    queue_progress: int = 0

    # Tell if data has been totally stored
    stored: int = False

    # Queue progress in percent
    store_progress: int = 0

@dataclass
class Data:
    """Class that defines a data.
    datetime are in UTC ISO string format for serializing simplicity
    """

    # Data id
    id: str = ''

    # Data name
    name: str = ''
    
    # Version of CWA that recorded this data
    version: str = ''

    # Data creation time in iso format
    creation_time: str = ''

    # Time when first part of data has been queued
    queue_start_time: str = ''

    # Time when last part of data has been queued
    queue_end_time: str = ''

    # Time when first part of data has been stored
    store_start_time: str = ''

    # Time when last part of data has been stored
    store_end_time: str = ''

    # Status of data
    status: DataStatus = field(default_factory=lambda: DataStatus())

    # Root datasets
    datasets: List[Dataset] = field(default_factory=lambda: [])

    # Root attributes
    attributes: List[Attribute] = field(default_factory=lambda: [])

    # Number of groups
    group_count: int = -1

    # Group dataset
    group_datasets: List[Dataset] = field(default_factory=lambda: [])

    # Group attributes
    group_attributes: List[Attribute] = field(default_factory=lambda: [])
        

@dataclass
class AttributeValues:
    """Class that defines a root dataset value update."""
    
    # Dataset id
    ids: List[str] = field(default_factory=lambda: [])
    
    # New value
    values: List[Union[float, str]] = field(default_factory=lambda: [])

@dataclass
class DatasetValue:
    """Class that defines a root dataset value update."""
    
    # Dataset id
    dataset_id: str = ''
    
    # New value
    value: bytes = bytes()

@dataclass
class GroupDatasetValue:
    """Class that defines a group dataset value update."""
    
    # Group id
    group_id: str = ''
    
    # Dataset id
    dataset_id: str = ''
    
    # New value
    value: bytes = bytes()

@dataclass
class GroupAttributeValues:
    """Class that defines a group dataset attribute update."""
    
    # Group id
    group_id: str = ''
    
    # Attribute values
    values: AttributeValues = field(default_factory=lambda: [])
