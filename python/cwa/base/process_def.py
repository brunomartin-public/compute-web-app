import json
from pathlib import Path
from typing import List, Literal
from dataclasses import dataclass, field


@dataclass
class ProcessDefParameter:
    """Class that defines a process definition parameter."""
    
    # Name of the parameter, shall be unique for a process definition
    name: str
    
    # Type of the parameter, shall be and integer, a number or a string
    dtype: Literal['integer', 'number', 'string']
    
    # Default value for this parameter
    # default: int | float | str
    default: any
    
@dataclass
class ProcessDef:
    """Class that defines a process definition."""

    # Process definition name
    name: str

    # Description giving the purpose of this process
    description: str

    # Process definition id
    id: str = ''
    
    # List of message in case of errors
    errors: List[str] = field(default_factory=lambda: [])
    
    # List of parameter that shall be given to the executable or script
    parameters: List[ProcessDefParameter] = \
        field(default_factory=lambda: [])
        
    # Options of process handling
    options: List[str] = field(default_factory=lambda: [])
    
    # List of strings that the process takes as arguments
    args: List[str] = field(default_factory=lambda: [])
    
    # Same as args but for Windows as it may change (.exe or directory
    # name separator for instance)
    win_args: List[str] = field(default_factory=lambda: [])
    
    # List of dataset name considered as result of process
    result_datasets: List[str] = field(default_factory=lambda: [])

    # Load from json string
    @staticmethod
    def create_from_dict(process_def_json: dict) -> 'ProcessDef':
        """Create new dataclass from json string."""
        
        parameters: List[ProcessDefParameter] = []
        for parameter_json in process_def_json['parameters']:
            parameters.append(ProcessDefParameter(
                parameter_json['name'],
                parameter_json['type'],
                parameter_json['default'],
            ))
            
        return ProcessDef(
            name=process_def_json['name'],
            description=process_def_json['description'],
            parameters=parameters,
            options=process_def_json.get('options', []),
            args=process_def_json['args'],
            win_args=process_def_json.get('win_args', []),
            result_datasets=process_def_json['results'],
        )