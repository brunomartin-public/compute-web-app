import h5py
import numpy as np
from pathlib import Path
from datetime import datetime, timezone
import time

from cwa.base.data import Data, Dataset, Attribute, DataStatus

class WrongDataSizeException(Exception):
    """ Wrong data size exception."""

class H5File(h5py.File):
    """Class for a CWA dedicated h5py file that implement helper methods."""
    
    def _load_root_attrs(self, data: Data):
        data.name = self.attrs['cwa_name'].decode()
        data.version = self.attrs['cwa_version'].decode()
        data.creation_time = self.attrs['cwa_post_time'].decode()
        data.store_start_time = self.attrs['cwa_store_start_time'].decode()
        data.store_end_time = self.attrs['cwa_store_end_time'].decode()
        data.status.stored = bool(self.attrs['cwa_stored'])
        data.status.store_progress = int(self.attrs['cwa_store_progress'])
        
        for name, dataset in self.items():
            if not isinstance(dataset, h5py.Dataset):
                continue
            
            data.datasets.append(
                Dataset(name, str(dataset.dtype), dataset.shape)
            )
        
        name: str
        for name, value in self.attrs.items():
            # Exclude CWA dedicated attributes
            if name.startswith('cwa_'):
                continue
            
            dtype = str if isinstance(value, str) else \
                str(self.attrs[name].dtype)
            data.attributes.append(
                Attribute(name, dtype, value)
            )
    
    def load_group_attrs(self, data: Data):
        """ Load File content to update data structure. """
        
        data.group_count = 0
        for group in self.values():
            if not isinstance(group, h5py.Group):
                continue
            
            data.group_count += 1

            # get info about dataset in the first group
            # all groups need to have the same dataset structures
            if data.group_count > 1:
                continue

            for name, dataset in group.items():
                if not isinstance(dataset, h5py.Dataset):
                    continue
                
                data.group_datasets.append(
                    Dataset(name, str(dataset.dtype), dataset.shape)
                )

    @classmethod
    def open_file_for_reading(cls, filepath: Path) -> 'H5File':
        libver = 'latest'

        file = None
        file_open = False
        start_time = time.time()
        while not file_open and time.time() - start_time < 2:
            try:
                file = H5File(filepath, 'r', libver=libver, swmr=True)
                file_open = True
            except ValueError as e:
                pass
            except OSError as e:
                pass
            
        if not file_open:
            file = H5File(filepath, 'r', libver=libver, swmr=True)

        return file
    
    @classmethod
    def load_data(cls, filepath: Path):

        file = H5File.open_file_for_reading(filepath)
        data = Data()
        file._load_root_attrs(data)
        file.load_group_attrs(data)
        file.close()
        
        return data
    
    @classmethod
    def load_dataset_value(cls, filepath: Path, group_id, dataset_id,
                           start=None, length=None):

        file = H5File.open_file_for_reading(filepath)

        group_name = '%03d' % group_id
        group = file[group_name]

        shape = group[dataset_id].shape

        if len(shape) > 2:
            if start is None:
                start = 0

            if length is None:
                length = shape[2] - start
            end = start + length

            dataset = np.copy(group[dataset_id][:, :, start:end])
        else:
            dataset = np.copy(group[dataset_id][:, :])

        file.close()

        return dataset
    
    @classmethod
    def create_file(cls, data: Data, filepath: Path) \
        -> 'H5File':

        libver = 'latest'

        file = H5File(filepath, 'w-', libver=libver)

        # store compute web app information
        file.attrs.create('cwa_name', np.string_(data.name))
        file.attrs.create('cwa_version', np.string_(data.version))
        file.attrs.create('cwa_post_time', np.string_(data.creation_time))

        file.attrs.create('cwa_stored', False)
        file.attrs.create('cwa_store_progress', 0)
        file.attrs.create('cwa_store_start_time', np.string_(''))
        file.attrs.create('cwa_store_end_time', np.string_(''))

        # Create common (root) datasets
        for dataset in data.datasets:
            h5_dataset = file.create_dataset(
                name=dataset.name, shape=dataset.shape,
                maxshape=dataset.shape, dtype=dataset.dtype)
            h5_dataset.attrs.create('cwa_stored', False)

        # Create common (root) attributes
        for attribute in data.attributes:                
            if isinstance(attribute.value, str):
                file.attrs.create(name=attribute.name,
                                  data=attribute.value)
            else:
                file.attrs.create(name=attribute.name,
                                  dtype=attribute.dtype,
                                  data=attribute.value)

        # create groups and their attributes
        for i in range(data.group_count):
            # TODO: set number of leading zeros according to group count
            group_name = '%03d' % i
            group = file.create_group(group_name)
            group.attrs.create("cwa_stored", False)

            # create datasets in groups
            for dataset in data.group_datasets:
                dataset = group.create_dataset(
                    name=dataset.name, shape=dataset.shape,
                    maxshape=dataset.shape, dtype=dataset.dtype
                )
                dataset.attrs.create('cwa_stored', False)

        # Set SWMR mode here, after all elements are created
        file.swmr_mode = True

        return file

    @classmethod
    def open_file_for_writing(cls, filepath: Path) -> 'H5File':

        file = None

        libver = 'latest'

        file_open = False
        while not file_open:
            try:
                file = H5File(filepath, 'r+', libver=libver)
                file_open = True
                file.swmr_mode = True
            except ValueError as e:
                pass
            except OSError as e:
                pass

        return file
    
    def update_group_dataset_value(self, group_id, dataset_id, data):
        # If group_id is a '/', dataset to be updated is a root dataset
        group_name = '/' if group_id == '/' else '%03d' % group_id
        if group_name not in self:
            raise Exception('group not found')
        group = self[group_name]
        if dataset_id not in group:
            raise Exception('dataset not found')
        dataset = group[dataset_id]
        
        # get type and shape from dataset
        dtype = dataset.dtype
        shape = dataset.shape

        expected_data_size = dtype.itemsize * dataset.size
        data_size = len(data)

        # check if data has right size
        if data_size != expected_data_size:
            message = 'Data has not the right size, should be '\
                      + str(expected_data_size) + ' bytes and is '\
                      + str(data_size) + ' bytes.'
            self.close()
            raise WrongDataSizeException(message)

        # Build linear vector from data received in its own order
        data_vector = np.frombuffer(data, dtype=dtype, count=np.prod(shape))

        # Numpy and, thus, h5py align their matrices in row major order,
        # or lexicographic order
        data_array = np.reshape(data_vector, shape)

        # Trying to update dataset content before attrs generate following error:
        # Unable to create attribute (Parent entry isn't pinned or protected)
        dataset.attrs['cwa_stored'] = True
        dataset[:] = data_array
        dataset.flush()

        # Update group stored state
        group_stored = True
        for dataset in group.values():
            group_stored &= bool(dataset.attrs['cwa_stored'])

        group.attrs['cwa_stored'] = group_stored

        if hasattr(group, 'flush'):
            group.flush()

        self._update_file_cwa_store_attrs()

    def update_group_dataset_attr(self, group_id, dataset_id, name, value):
        # If group_id is a '/', dataset to be updated is a root attribute
        group_name = '/' if group_id == '/' else '%03d' % group_id
        if group_name not in self:
            raise Exception('group not found')
        group = self[group_name]
        if dataset_id not in group:
            raise Exception('dataset not found')
        dataset = group[dataset_id]

        if isinstance(value, str):
            value = np.string_(value)

        dataset.attrs[name] = value

        if hasattr(group, 'flush'):
            group.flush()
    
    def _update_file_cwa_store_attrs(self):
        stored = True
        store_progress = 0
        group_count = 0
        for group in self.values():
            if isinstance(group, h5py.Group):
                group_count += 1

                group_stored = bool(group.attrs['cwa_stored'])
                store_progress += 1 if group_stored else 0

                stored &= group_stored

        store_progress = int(store_progress / group_count * 100)
        self.attrs['cwa_store_progress'] = store_progress

        if group_count == 0:
            self.attrs['cwa_store_start_time'] = \
                np.string_(datetime.now(timezone.utc).isoformat())

        if stored:
            self.attrs['cwa_store_end_time'] = \
                np.string_(datetime.now(timezone.utc).isoformat())
            self.attrs['cwa_stored'] = True

        if hasattr(self['/'], 'flush'):
            self['/'].flush()
