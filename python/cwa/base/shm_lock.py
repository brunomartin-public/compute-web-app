from multiprocessing import shared_memory
import time

class ShmLock:
    """ Lock based on shared memory for multiprocessing locking
    """
    
    def __init__(self, name: str, timeout: int = 10, poll_interval = 0.01):
        """Init lock

        Args:
            name (str): name of shared memory used for locking
            timeout (int, optional): Duration in seconds before sending
                an TimeError exception. Defaults to 10.
            poll_interval (float, optional): Interval in seconds between
                each locking attempt. Defaults to 0.01.
        """
            
        # Name of shared memory block to use for locking by creating it
        self._name: str = name
            
        # Shared memory block to create for locking and unlink for
        # unlocking
        self._shm: shared_memory.SharedMemory = None
        
        # Duration in seconds before sending an TimeError exception
        self._timeout: float = timeout
        
        # Interval in seconds between each locking attempt
        self._poll_interval: float = poll_interval
        
    def lock(self):
        """Try to lock by attempting to create a shared memory block
        during a maximum time and repeatedly with an interval

        Raises:
            TimeoutError: Exception raised if attempt to lock as exceed
                timeout duration
        """
        
        locked = False          
        start_time = time.time()
        while time.time() - start_time < self._timeout:
            try:
                self._shm = shared_memory.SharedMemory(
                    name=self._name, create=True, size=1)
                locked = True
                break
            except FileExistsError:
                time.sleep(self._poll_interval)
                continue
        
        if not locked:
            raise TimeoutError
        
    def unlock(self):
        """Unlock by closing and unlink acquired shared memory block
        
        Raises:
            TypeError: Exception raised if attempting to unlock whereas
                not locked
        """
        
        if not self._shm:
            raise TypeError
                
        self._shm.close()
        self._shm.unlink()
