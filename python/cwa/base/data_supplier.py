from pathlib import Path
import logging
from typing import Union, List
import numpy as np

from cwa.base.data import Data, DatasetValue, GroupDatasetValue, \
    AttributeValues, GroupAttributeValues
from cwa.base.hdf5_file import H5File


class DataSupplier:
    """ This class purpose is to propose an interface to class dedicated
    to supply data to data processes.
    """
    
    def __init__(self, data_id: str):
        """Init a data store for a given id

        Args:
            data_id (str): data id
        """
        
        self.data_id = data_id
    
    def create(self, data: Data):
        """ This method must be overriden """
        
    def fill_common_dataset_value(self, value: DatasetValue):
        """ This method must be overriden """
        
    def fill_group_dataset_value(self, value: GroupDatasetValue):
        """ This method must be overriden """
        
    def fill_common_attribute_values(self, value: AttributeValues):
        """ This method must be overriden """
        
    def fill_group_attribute_values(self, value: GroupAttributeValues):
        """ This method must be overriden """
        
    def load_ids(self) -> List[int]:
        """ This method must be overriden """
        
    def load_data(self) -> Data:
        """ This method must be overriden """
        
    def load_dataset_value(self, group_id, dataset_id,
                           start=None, length=None) -> np.ndarray:
        """ This method must be overriden """

class H5DataSupplier(DataSupplier):
    """Implementation to supply data using h5 files
    """
    
    def __init__(self, data_path: Path, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        # Directory path where to store h5 files
        self._data_path = data_path

        # Dictionary of open files
        self._file: H5File = None    
        
        # Logger for this class
        self._logger = logging.getLogger(__name__)
    
    def create(self, data: Data):
        """ This method create a file according to content of input item """
        
        filepath = Path(self._data_path) / (data.id + '.h5')
        
        self._logger.info(f'Creating file {filepath}...')
        file = H5File.create_file(data=data, filepath=filepath)
        self._logger.info(f'File {filepath} created.')
        
        self._file = file
        
    def fill_common_dataset_value(self, value: DatasetValue):
        """ This method must be overriden """
        self._fill_dataset_value(value)
        
    def fill_group_dataset_value(self, value: GroupDatasetValue):
        """ This method must be overriden """
        self._fill_dataset_value(value)
        
    def _fill_dataset_value(
        self, value: Union[DatasetValue, GroupDatasetValue]):
        """ This method update an existing file according to content of input item """
        
        dataset_value = value
        filepath = Path(self._data_path) / (self.data_id + '.h5')
        
        # open file for the first time and keep it open
        if not self._file:
            self._file = H5File.open_file_for_writing(filepath)
            
        # if it is a data part
        if isinstance(dataset_value, DatasetValue):
            self._file.update_group_dataset_value(
                '/', dataset_value.dataset_id,
                dataset_value.value)            
        elif isinstance(dataset_value, GroupDatasetValue):
            self._file.update_group_dataset_value(
                dataset_value.group_id,
                dataset_value.dataset_id,
                dataset_value.value)
        else:
            raise Exception('Attribute update is not implemented yet')
            
        # if group or attributes cannot be flushed, flush the whole file
        if not hasattr(self._file['/'], 'flush'):
            self._file.flush()
        
        # If file has been stored, close it and remove it from the
        # file list
        if bool(self._file.attrs['cwa_stored']):
            self._file.close()
            self._file = None
            
    def fill_common_attribute_values(self, value: AttributeValues):
        """ Fill common attribute values """
        self._fill_attribute_values(self.data_id, value)
        
    def fill_group_attribute_values(self, value: GroupAttributeValues):
        """ Fill group attribute values """
        self._fill_attribute_values(self.data_id, value)
        
    def _fill_attribute_values(
        self, value: Union[AttributeValues, GroupAttributeValues]):
        """ This method update an existing file according to content of input item """
        
        filepath = Path(self._data_path) / (self.data_id + '.h5')
        
        # open file for the first time and keep it open
        if not self._file:
            self._file = H5File.open_file_for_writing(filepath)
            
        # if it is a data part
        if isinstance(value, AttributeValues):
            for attr_name, attr_value in \
                zip(value.ids, value.values):
                self._file.update_group_dataset_attr(
                    '/', attr_name, attr_value)
        elif isinstance(value, GroupAttributeValues):
            group_id = value.group_id
            for attr_name, attr_value in \
                zip(value.values.ids, value.values.values):
                self._file.update_group_dataset_value(
                    group_id, attr_name, attr_value)
        else:
            raise Exception('Attribute update is not implemented yet')
            
        # if group or attributes cannot be flushed, flush the whole file
        if not hasattr(self._file['/'], 'flush'):
            self._file.flush()
    
    def load_ids(self) -> List[int]:
        """ Load and return all data ids """
        data_ids: List[int] = []
        # Get h5 filenames, remove extension to get data id
        for filename in self._data_path.glob('*.h5'):
            data_id = filename.stem
            data_ids.append(data_id)
        return data_ids

    def load_data(self) -> Data:
        """ Load data content pointed by data_id and return it """

        filename = self.data_id + '.h5'
        filepath = self._data_path / filename
        data = H5File.load_data(filepath)
        data.id = self.data_id
        return data

    def load_dataset_value(self, group_id, dataset_id,
                           start=None, length=None) -> np.ndarray:

        filename = self.data_id + '.h5'
        filepath = self._data_path / filename

        # Following call may throw a FileNotFoundError exception
        return H5File.load_dataset_value(
            filepath, group_id, dataset_id, start, length)
