import dateutil
import platform
from pathlib import Path
import shutil
from datetime import datetime, timezone
import psutil
import uuid
from threading import Event

from cwa.base.lockable_file import LockableFile
from cwa.base.process import Process, ProcessParameter, ProcessStatus, \
    ProcessAction
from cwa.base.process_thread import ProcessThread
from cwa.base.process_def_manager import ProcessDefManager

class ProcessNotFound(Exception):
    """Exception raised when process is not found.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

class ProcessNotStarted(Exception):
    """Exception raised when process has not been started before
    timeout.
    
    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

class ProcessActionTimeout(Exception):
    """Exception raised when timeout has been reach on process action.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

class InvalidAction(Exception):
    """Exception raised when invalid action is requested.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

class MissingParameters(Exception):
    """Exception raised when when missing parametes when adding a process.

    Attributes:
        parameters -- Missing parameter names
    """

    def __init__(self, parameters):
        self.parameters = parameters

class ProcessManager:
    """ This class purpose is to add process and handle them
    """

    # File that store the currentlty running process ids
    running_ids_filename = 'running_process_ids.json'

    def __init__(self, data_path: Path, process_def_manager: ProcessDefManager, result_path: Path):
        # Path to data location
        self.data_path = data_path

        # Path to result location
        self.process_def_manager = process_def_manager

        # Path to result location
        self.result_path = result_path

        # List of process actions requested
        self.process_actions = list()

    def add(self, process: Process) -> str:
        """ Add new process according to input. """

        process_def_id = process.process_def_id
        parameters = process.parameters

        # get parameters from process definition
        process_def = self.process_def_manager.load(process_def_id)
        excepted_parameters = process_def.parameters

        parameter_names = [parameter.name for parameter in parameters]
        excepted_parameter_names = [parameter.name for parameter in excepted_parameters]

        # iterate over process parameters and check if assigned,
        # assign default one of any, return error if required
        missing_parameters = []
        for process_parameter_name in excepted_parameter_names:
            if process_parameter_name not in parameter_names:
                missing_parameters.append(process_parameter_name)

        if len(missing_parameters) > 0:
            raise MissingParameters(missing_parameters)

        # generate a 16 hex random string id for this process
        process.id = str(uuid.uuid4())

        # add creation time
        process.creation_time = datetime.now(timezone.utc).isoformat()

        # add status to process
        process.status = ProcessStatus()
        
        # create a log file into which the process will write
        process.log_filepath = self.result_path / (process.id + '.log')
        process.log_filepath.touch()

        process_json_file = self.result_path / (process.id + '.json')
        with open(process_json_file, 'w') as fp:
            process_json = process.dumps_json()
            fp.write(process_json)

        # Check if running process file exists and create it if not
        running_process_filepath = \
            Path(self.result_path / self.running_ids_filename)
        if not running_process_filepath.exists():
            LockableFile(running_process_filepath).lock_and_write_json(list())
        
        return process.id

    def do_action(self, action: ProcessAction) -> str:

        if action.name not in ['start', 'suspend', 'resume', 'stop', 'reset']:
            return InvalidAction(f'invalid action name')

        process = self.load(action.process_id)

        running_process_filepath = self.result_path / self.running_ids_filename
        process_json_filepath = self.result_path / (process.id + '.json')
        
        # Check pid for specific actions
        if action.name in ['suspend', 'resume', 'stop']:
            if process.pid < 1:
                raise ProcessNotFound(
                    f'No such process with PID {process.pid}.')
            try:
                ps_process = psutil.Process(pid=process.pid)
            except psutil.NoSuchProcess:
                raise ProcessNotFound(
                    f'No such process with PID {process.pid}.')

        if action.name == 'start':

            process_def_id = str(process.process_def_id)
            process_def = self.process_def_manager.load(process_def_id)

            # extract arguments from process definition
            if platform.system() == 'Windows':
                args = process_def.win_args
            else:
                args = process_def.args

            parameters = process.parameters
                
            # add mandatory parameters
            data_file_path = self.data_path / (process.data_id + '.h5')
            parameters.append(ProcessParameter(
                'data_file', 'str', str(data_file_path.absolute())
            ))

            result_file_path = self.result_path / (process.id + '.h5')
            parameters.append(ProcessParameter(
                'result_file', 'str', str(result_file_path.absolute())
            ))

            parameters = {}
            for parameter in process.parameters:
                parameters[parameter.name] = parameter.value

            # look if any arg name of process definition is in parameters
            # list of process, replace it with parameter value
            for i in range(len(args)):
                if args[i] in parameters:
                    args[i] = str(parameters[args[i]])

            # set current working directory to current process definition directory
            cwd = self.process_def_manager.get_execution_path(process_def_id)

            # Create a event to wait for process to start
            started_event = Event()

            # create a thread for running process and start it now
            ProcessThread(
                self.running_ids_filename,process, started_event, cwd,
                self.result_path, args).start()
            
            # Wait for start event to be set telling process as been started
            timeout = 5
            if not started_event.wait(timeout):
                raise ProcessNotStarted(
                    f'Process {process.id} has not been started after'
                    f' {timeout}s')

            # Update process status here
            process.status = ProcessStatus()
            process.status.running = True
            LockableFile(process_json_filepath).lock_and_write(
                process.dumps_json())

        elif action.name == 'suspend':
            ps_process.suspend()

            process.status.suspended = True
            LockableFile(process_json_filepath).lock_and_write(
                process.dumps_json())

            # remove this process id from running processes
            def remove_process(json_data: list):
                if process.id in json_data:
                    json_data.remove(process.id)
            LockableFile(running_process_filepath) \
                .lock_and_update_json(remove_process)

        elif action.name == 'resume':
            ps_process.resume()

            process.status.suspended = False
            LockableFile(process_json_filepath).lock_and_write(
                process.dumps_json())

            # add this process id to running processes
            def add_process(json_data: list):
                if process.id not in json_data:
                    json_data.append(process.id)
            LockableFile(running_process_filepath) \
                .lock_and_update_json(add_process)

        elif action.name == 'stop':    
            ps_process.kill()            
            try:
                ps_process.wait(timeout=5)
            except psutil.TimeoutExpired:
                raise ProcessActionTimeout(
                    'Timeout when trying to stop process'
                    f' {process.id}.')

            process.status.done = False
            process.status.running = False

            LockableFile(process_json_filepath).lock_and_write(
                process.dumps_json())

        elif action.name == 'reset':
            try:
                # make trash directory
                trash_path = self.result_path / 'trash'
                trash_path.mkdir(exist_ok=True)
                
                start_time_str = process.start_time
                start_time = dateutil.parser.parse(start_time_str)
                if not start_time:
                    start_time = datetime.now(timezone.utc).isoformat()
                    
                start_time_suffix = start_time.strftime('-%Y%m%d_%H%M%S')
                
                # copy json file to it
                shutil.copyfile(
                    self.result_path / (process.id + '.json'),
                    trash_path / (process.id + start_time_suffix + '.json')
                )
                
                # move log file to it
                (self.result_path / (process.id + '.log')).rename(
                    trash_path / (process.id + start_time_suffix + '.log')
                )
                
                # move h5 file to it
                (self.result_path / (process.id + '.h5')).rename(
                    trash_path / (process.id + start_time_suffix + '.h5')
                )
                
            except OSError:
                print('Error while resetting process : ', process.id)
                
            process.status.running = False
            process.status.done = False
            process.status.suspended = False
            process.status.progress = 0
            process.status.errors = []
            LockableFile(process_json_filepath).lock_and_write(
                process.dumps_json())
            
        # generate a uuid for this process action
        action.id = str(uuid.uuid4())
        
        self.process_actions.append(action)
        
        return action.id
    
    def load_process_ids(self):
        """Load stored process ids."""
        
        # List all .json files in result path supposing only the one
        # dedicated to running process is to be excluded
        process_ids = [x.stem for x in self.result_path.glob('*.json') \
                       if x.name != self.running_ids_filename]
        
        return process_ids
    
    def load(self, process_id: str):
        """Load process information according to its id."""

        process_json_file: Path = self.result_path / (process_id + '.json')
        if not process_json_file.exists():
            raise ProcessNotFound(f'Process {process_id} not found.')

        process_dict = LockableFile(process_json_file).lock_and_read_json(default=dict())
        process_dict['process_id'] = process_id

        return Process.create_from_json(process_dict)
    
    def load_running_process_ids(self):

        running_process_filepath = self.result_path / self.running_ids_filename
        running_process_ids = LockableFile(running_process_filepath).lock_and_read_json(default=dict())
        return running_process_ids
