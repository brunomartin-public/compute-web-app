from dataclasses import dataclass

from cwa.base.data import Data

@dataclass
class Result(Data):
    """Class that defines a result."""

    # Id of the data from which result has been computed
    data_id: str = ''
