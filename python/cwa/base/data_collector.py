import logging
import time
import pickle
import struct
import os

from multiprocessing import Process, shared_memory, Event
from queue import PriorityQueue, Empty
from threading import Thread

from cwa.base.data import Data, DatasetValue, GroupDatasetValue, \
    AttributeValues, GroupAttributeValues
from cwa.base.data_collector_status import DataCollectorStatus, ItemState
from cwa.base.shm_lock import ShmLock
from cwa.base.data_supplier import DataSupplier

from dataclasses import dataclass, field
from typing import Union

@dataclass(order=True)
class DataCollectorItem:
    """ Item for prioritized queue. """
    # Priority of item in the store process
    priority: int
    
    # Content to carry
    content: Union[
        Data, DatasetValue, GroupDatasetValue,AttributeValues,
        GroupAttributeValues] = field(compare=False)
    
class DataCollector(Process):
    """ This class purpose is to collect data to supply them to
    processes. One DataStore shall be created for each data and
    shared memory is used to collect parts of data.
    """
    
    # Shm status part length 
    _shm_status_len: int = 1024
    
    def __init__(self, shm: shared_memory.SharedMemory,
                 read_write_lock: ShmLock, status_lock: ShmLock,
                 data_supplier: DataSupplier,
                 lock_timeout: float = 10, poll_interval = 0.01,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        # Data collector status
        self._status: DataCollectorStatus = None
        
        # Shared memory block shared between collector process and
        # calling processes
        self._shm: shared_memory.SharedMemory = shm
        
        # Shared memory lock for read/write
        self._read_write_lock: ShmLock = read_write_lock
        
        # Shared memory lock for shm read/write
        self._status_lock: ShmLock = status_lock
        
        # Timeout in seconds for locking access to shm content
        self._lock_timeout: float = lock_timeout
        
        # Poll interval in seconds for reading shm content
        self._poll_interval: float = poll_interval
        
        # Tell if stop queue loop
        self._stop_queue = False
        
        # Data supplier
        self._data_supplier: DataSupplier = data_supplier
        
        # Logger for this class
        self._logger = logging.getLogger(__name__)
        
    def run(self):
        """Main function started in a new process waiting messages from
        shared memory.
        """
        
        # Launcher process shall have initialized status part of shared
        # memory, so, read it and update it
        self._status_lock.lock()
        self._status = pickle.loads(self._shm.buf)
        self._status.pid = os.getpid()
        data = pickle.dumps(self._status)
        self._shm.buf[:len(data)] = data
        self._status_lock.unlock()
        
        shm_buf_data = \
            self._shm.buf[self._shm_status_len:self._shm.size]
        
        # Start record thread in this process
        queue = PriorityQueue()
        queue_thread = Thread(
            target=self._queue_loop, args=(queue, ), daemon=True)
        queue_thread.start()
        
        is_data_queued = False
        while not is_data_queued and not self._asked_to_stop():
            
            # Lock read write to check if there is data to retrieved
            try:
                self._read_write_lock.lock()
                # Check if there is data to retrieved
                data_len, = struct.unpack('I', shm_buf_data[:4])
                if data_len == 0:
                    self._read_write_lock.unlock()
                    time.sleep(self._poll_interval)
                    continue
            except TimeoutError:
                continue
            
            item: DataCollectorItem = pickle.loads(shm_buf_data[4:4+data_len])
            queue.put(item)
            
            if isinstance(item.content, Data) or \
                isinstance(item.content, DatasetValue) or \
                isinstance(item.content, AttributeValues):
                queue.join()
            elif isinstance(item.content, GroupDatasetValue) or \
                isinstance(item.content, GroupAttributeValues):
                pass
            
            # Update shared status
            self._status_lock.lock()
            self._status = pickle.loads(self._shm.buf)
            
            if isinstance(item.content, Data):
                self._status.populate(item.content)
            elif isinstance(item.content, DatasetValue):
                dataset_id = item.content.dataset_id
                self._status.common_dataset_states[dataset_id] \
                    = ItemState.QUEUED
            elif isinstance(item.content, AttributeValues):
                self._status.common_attributes_states = ItemState.QUEUED
            elif isinstance(item.content, GroupDatasetValue):
                dataset_id = item.content.dataset_id
                group_id = item.content.group_id
                self._status.group_dataset_states[dataset_id][group_id] \
                    = ItemState.QUEUED
            elif isinstance(item.content, GroupAttributeValues):
                group_id = item.content.group_id
                self._status.group_attributes_states[group_id] \
                    = ItemState.QUEUED
            
            data = pickle.dumps(self._status)
            self._shm.buf[:len(data)] = data
            self._status_lock.unlock()
            
            # Mark shared memory segment as read and unlock it
            shm_buf_data[:4] = struct.pack('I', 0)
            self._read_write_lock.unlock()
            
            # Check status and break if data is entirely queued
            is_data_queued = self._status.is_data_queued()
        
        if not is_data_queued:
            self._logger.warning(
                'Data has not been completely queued in collector')
        
        self._stop_queue = True
        queue_thread.join()
        
        # delete unless get a BufferError: cannot close exported
        # pointers exist
        del shm_buf_data
        self._shm.close()
        self._shm.unlink()
        
        self._logger.info('DataCollector has been gracefully stopped')
        
    def _asked_to_stop(self) -> bool:
        """Check if collector has been asked to stop

        Returns:
            bool: True of asked to stop, else False
        """
        
        self._status_lock.lock()
        self._status = pickle.loads(self._shm.buf)
        asked_to_stop = self._status.asked_to_stop
        data = pickle.dumps(self._status)
        self._shm.buf[:len(data)] = data
        self._status_lock.unlock()
        
        return asked_to_stop
        
    def _queue_loop(self, queue: PriorityQueue):
        """Item queue loop
        
        Args:
            queue (PriorityQueue): queue from which waiting for
              items
        """
        
        while not self._stop_queue or not queue.empty():
            try:
                item: DataCollectorItem = queue.get(timeout=0.5)
                
                # Check type of item data and call the correct method 
                # between creation and update
                if isinstance(item.content, Data):
                    self._data_supplier.create(item.content)
                elif isinstance(item.content, DatasetValue):
                    self._data_supplier.fill_common_dataset_value(
                        item.content)
                elif isinstance(item.content, GroupDatasetValue):
                    self._data_supplier.fill_group_dataset_value(
                        item.content)
                elif isinstance(item.content, AttributeValues):
                    self._data_supplier.fill_common_attribute_values(
                        item.content)
                elif isinstance(item.content, GroupAttributeValues):
                    self._data_supplier.fill_group_attribute_values(
                        item.content)
                    
                # Update shared status
                self._status_lock.lock()
                self._status = pickle.loads(self._shm.buf)
                
                if isinstance(item.content, Data):
                    # Nothing to do here
                    pass
                elif isinstance(item.content, DatasetValue):
                    dataset_id = item.content.dataset_id
                    self._status.common_dataset_states[dataset_id] \
                        = ItemState.FILLED
                elif isinstance(item.content, AttributeValues):
                    self._status.common_attributes_states = \
                        ItemState.FILLED
                elif isinstance(item.content, GroupDatasetValue):
                    dataset_id = item.content.dataset_id
                    group_id = item.content.group_id
                    self._status.group_dataset_states \
                        [dataset_id][group_id] = ItemState.FILLED
                elif isinstance(item.content, GroupAttributeValues):
                    group_id = item.content.group_id
                    self._status.group_attributes_states[group_id] \
                        = ItemState.FILLED
                
                data = pickle.dumps(self._status)
                self._shm.buf[:len(data)] = data
                self._status_lock.unlock()
                
                # Tell record queue that task is done
                # If an exception is thrown here, thread that has put
                # the item will be stuck in subsequent .join() method
                # TODO: Find a better behavior to handle error in 
                # this collector thread.
                queue.task_done()
                
            except Empty as e:
                # Catch Empty exception telling no item has been get before
                # timeout. https://stackoverflow.com/questions/11247439/python-queue-empty-exception-handling
                continue
            except Exception as e:
                self._logger.error(
                    f'DataCollectorProcess exception: {str(e)}')
                continue
        
        self._logger.info('_queue_loop has been gracefully stopped')
