import platform
import json
from pathlib import Path
from typing import List

from cwa.base.process_def import ProcessDef

class ProcessDefError(Exception):
    """Exception raised for errors in the process definition.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message):
        self.message = message

class ProcessDefManager:
    """ This class purpose is to handle process definitions
    """

    def __init__(self, process_def_path: Path):
        # Path to process definitions location
        self.process_def_path = process_def_path

    def get_execution_path(self, process_def_id):
        """ Return execution path for target process. """
        return self.process_def_path / process_def_id
    
    def load_ids(self):
        """ Load and return all process definition ids """
        
        # Look for process.json in subdirectory and return subdirectory
        # names
        process_def_ids = list()
        for filepath in self.process_def_path.glob('*/process.json'):
            process_def_ids.append(filepath.parent.name)
            
        return process_def_ids
    
    def load(self, process_def_id):
        """ Local function to get a process definition from filename. """
        filepath = self.process_def_path / process_def_id / 'process.json'
    
        file = open(filepath, 'r')            
        try:
            process_def_json = json.load(file)
        except json.JSONDecodeError as e:
            file.close()
            process_def = ProcessDef()
            process_def.errors.append(
                'json.JSONDecodeError : ' + e.msg)
            return process_def
            
        file.close()
        process_def = ProcessDef.create_from_dict(process_def_json)
        process_def.id = filepath.parent.name
        
        return process_def

