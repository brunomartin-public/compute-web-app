from pathlib import Path
import uuid
from datetime import datetime, timezone

import numpy as np

from cwa.base.data import Data, DatasetValue, GroupDatasetValue
from cwa.base.data_store import DataStore
from cwa.base.data_supplier import H5DataSupplier
from cwa.base.lockable_file import LockableFile

class DataManager:
    """ This class purpose is to handle input data recording and reading
    """
    
    def __init__(self, data_path: Path):
        # Path to data location
        self.data_path = data_path
        
        # List of data store handled
        self._stores: dict[str, DataStore] = {}
        
    def load(self, data_id: str):
        """ Load data content pointed by data_id and return it """
        
        if data_id not in self._stores:
            data_supplier = H5DataSupplier(self.data_path, data_id)
            self._stores[data_id] = DataStore(data_supplier)
        store = self._stores[data_id]
        
        data = store.load_data()
        
        # Load current queue state
        json_filepath = self.data_path / (data_id + '.json')
        json_data = LockableFile(json_filepath).lock_and_read_json({})

        data.queue_start_time = json_data['queue_start_time']
        data.queue_end_time = json_data['queue_end_time']
        
        status = store.get_collector_status()
        data.status.queued = status.is_data_queued()
        data.status.queue_progress = status.get_queue_progress()
        
        return data
        
    def load_ids(self):
        """ Load and return all data ids """        
        # Data supplier object has the responsability to list all
        # available data ids
        data_supplier = H5DataSupplier(self.data_path, '')
        return data_supplier.load_ids()
    
    def add(self, data: Data):
        """ Record new data according to input. """

        data.id = str(uuid.uuid4())

        # add post time
        data.creation_time = datetime.now(timezone.utc).isoformat()

        # Create a new data store for data to add and add it to stores
        # for being resued in further calls
        data_supplier = H5DataSupplier(self.data_path, data.id)
        store = DataStore(data_supplier)
        self._stores[data.id] = store
        
        # Put file creation item in queue and wait for its completion
        store.create(data)

        # Create file to store state of data filling
        json_filepath = self.data_path / (data.id + '.json')
        LockableFile(json_filepath).lock_and_write_json({
            'creation_time': data.creation_time,
            'queue_start_time': '',
            'queue_end_time': ''
        })

        return data.id
    
    def fill_root_dataset(self, data_id: str, data_value: DatasetValue):
        """Fill root dataset value

        Args:
            data_id (str): id of data to fill
            data_value (DataValue): dataset value
        """
        
        if data_id not in self.load_ids():
            raise Exception(f'No data id {data_id} found.')
        
        # This process may not have been used to create the data store.
        # Try to get it from the dictionnary and create it if not
        # present.
        if data_id not in self._stores:
            data_supplier = H5DataSupplier(self.data_path, data_id)
            self._stores[data_id] = DataStore(data_supplier)
        store = self._stores[data_id]
        
        # Put data value to queue
        store.put(data_value)
        
        # Get collector status to update json file
        status = store.get_collector_status()
        
        # Update json information on queued root datasets
        def update(json_data: dict):
            # Check if first dataset queued and record time
            if len(json_data['queue_start_time']) == 0:
                queue_start_time = datetime.now(timezone.utc).isoformat()
                json_data['queue_start_time'] = queue_start_time
                            
            if status.is_data_queued():
                queue_end_time = datetime.now(timezone.utc).isoformat()
                json_data['queue_end_time'] = queue_end_time
                
        json_filepath = self.data_path / (data_id + '.json')
        LockableFile(json_filepath).lock_and_update_json(update)
                
    def fill_group_dataset(self, data_id: str, dataset_value: GroupDatasetValue):
        
        if data_id not in self.load_ids():
            raise Exception(f'No data id {data_id} found.')
        
        # This process may not have been used to create the data store.
        # Try to get it from the dictionnary and create it if not
        # present.
        if data_id not in self._stores:
            data_supplier = H5DataSupplier(self.data_path, data_id)
            self._stores[data_id] = DataStore(data_supplier)
        store = self._stores[data_id]
        
        # Put dataset value to queue
        store.put(dataset_value)
        
        # Get collector status to update json file
        status = store.get_collector_status()
        
        # Update json information on queued group datasets
        def update(json_data: dict):            
            # Check if first dataset queued and record time
            if len(json_data['queue_start_time']) == 0:
                queue_start_time = datetime.now(timezone.utc).isoformat()
                json_data['queue_start_time'] = queue_start_time
                
            if status.is_data_queued():
                queue_end_time = datetime.now(timezone.utc).isoformat()
                json_data['queue_end_time'] = queue_end_time
                
        json_filepath = self.data_path / (data_id + '.json')
        LockableFile(json_filepath).lock_and_update_json(update)

    def get_uploading_ids(self):
        uploading_data_ids = list()
        # Get json filenames that mean that uploading is occurring
        for filename in self.data_path.glob('*.json'):
            data_id = filename.stem
            uploading_data_ids.append(data_id)
        return uploading_data_ids
    
    def load_dataset_value(self, data_id, group_id, dataset_id,
                           start=None, length=None,
                           query_data_type: str=None) -> np.ndarray:

        # This process may not have been used to create the data store.
        # Try to get it from the dictionnary and create it if not
        # present.
        if data_id not in self._stores:
            data_supplier = H5DataSupplier(self.data_path, data_id)
            self._stores[data_id] = DataStore(data_supplier)
        store = self._stores[data_id]
    
        value = store.load_dataset_value(
            group_id, dataset_id, start, length,
            query_data_type)
        
        return value

    def open_file(self, data_id):
        """@
        This function responds to a request for /api/data/{data_id}
        by recording data uploaded
        """

        # check if file exists
        filename = data_id + '.h5'
        filepath = self.data_path / filename

        if not filepath.is_file():
            raise Exception(f'file with data_id {data_id} not found')

        # if file exists, read it and return file descriptor
        return open(filepath, 'rb')
