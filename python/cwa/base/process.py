import json
from pathlib import Path
from typing import List
from dataclasses import dataclass, field


@dataclass
class ProcessParameter:
    """Class that defines a process parameter."""

    # Parameter name
    name: str

    # Parameter type compatible with numpy
    type: str

    # Value which type depends on type
    # value: str | int | float
    value: any

@dataclass
class ProcessStatus:
    """Class that defines the process status."""

    # Tell if process is running
    running: bool = False

    # Tell if process is done
    done: bool = False

    # Tell if process is suspended
    suspended: bool = False

    # Process progress in percent
    progress: int = 0

    # Process errors if any
    errors: List[str] = field(default_factory=lambda: [])

    # Log of process
    log: str = ''

@dataclass
class Process:
    """Class that defines a process."""

    # Data id to be processed
    data_id: str

    # Process definition id to use to run process
    process_def_id: str

    # Version of process
    version: str

    # Parameters of this process
    parameters: List[ProcessParameter]

    # Process id
    id: str = ''

    # Process creation time in iso format
    creation_time: str = ''

    # Process start time in iso format
    start_time: str = ''

    # Process end time in iso format
    end_time: str = ''

    # Process status
    status: ProcessStatus = None

    # Process log output filepath, shall be converted to string
    # before dumping it to json
    log_filepath: Path = None

    # System id of the launched process
    pid: int = -1

    # Dump to json string
    def dumps_json(self) -> str:
        """ Dump dataclass content to json string."""

        self.log_filepath = str(self.log_filepath.absolute())
        process_json = json.dumps(self, default=lambda obj: obj.__dict__)
        self.log_filepath = Path(self.log_filepath)

        # Keep compatibility with other methods
        # TODO: remove following lines once completely moved to
        #   object
        process_dict = json.loads(process_json)
        process_dict['private'] = {}
        process_dict['private']['log_filepath'] = \
            str(self.log_filepath.absolute())
        del process_dict['log_filepath']
        process_json = json.dumps(process_dict)

        return process_json
    
    # Load from json string
    @staticmethod
    def create_from_json(process_json: str) -> 'Process':
        """Create new dataclass from json string."""

        parameters: List[ProcessParameter] = []
        for parameter_json in process_json['parameters']:
            parameters.append(ProcessParameter(
                parameter_json['name'],
                parameter_json['type'],
                parameter_json['value'],
            ))
        
        if 'status' in process_json:
            status_json = process_json['status']
            status = ProcessStatus(
                status_json['running'],
                status_json['done'],
                status_json['suspended'],
                status_json['progress'],
                status_json['errors'],
                ''
            )
        else:
            status = ProcessStatus()
        
        # TODO: remove when not necessary anymore
        log_filepath = process_json.get('log_filepath', '')
        if 'private' in process_json:
            log_filepath = process_json['private'].get(
                'log_filepath', '')

        return Process(
            process_json['data_id'],
            process_json['process_def_id'],
            process_json['version'],
            parameters,
            process_json.get('process_id', -1),
            process_json.get('creation_time', ''),
            process_json.get('start_time', ''),
            process_json.get('end_time', ''),
            status,
            Path(log_filepath),
            process_json.get('pid'),
        )

@dataclass
class ProcessAction(object):
    """Class that define an action to do on a process."""

    # Name of the action
    name: str

    # id of the process to do tha action
    process_id: str

    # Version of process
    version: str

    # ID of the action
    id: str = ''
