import os
import time
from pathlib import Path
import json
from collections.abc import Callable

class LockableFile:
    """Class for a file that can be locked for reading and writing access.
    For a crossplatform and multi file system purpose, a file is created to
    prevent reader and writer to access the file simultaneously."""

    # TODO: To prevent deadlocks on access on these files, a timeout may be
    # appreciated.

    def __init__(self, path: Path):
        # Path of the file to lock
        self.path = path

        # Path of the lock file to create/unlink for locking
        self.lock_filepath = path.with_suffix('.lock')

    def _lock_file(self) -> int:
        """ Try to lock file until success and return id to lock file created """

        # wait until lock file is deleted
        lock_file_id = None
        file_locked = False
        while not file_locked:
            try:
                # try create a lock file
                lock_file_id = os.open(self.lock_filepath,
                    os.O_CREAT | os.O_EXCL | os.O_RDWR)
                file_locked = True
            except OSError as e:
                time.sleep(0.2)

        return lock_file_id
    
    def _unlock_file(self, lock_file_id: int):
        """ Unlock file using lock file id created on lock """
        os.close(lock_file_id)
        Path(self.lock_filepath).unlink()

    def _lock_and_read(self, default: str) -> str:
        """ Lock and read content of file """
        
        if not self.path.is_file():
            return default

        lock_file_id = self._lock_file()
        with open(self.path) as f:
            content = f.read()
        self._unlock_file(lock_file_id)
        return content
    
    def lock_and_write(self, content: str) -> str:
        """ Lock and write content of file """
        lock_file_id = self._lock_file()
        with open(self.path, 'w') as f:
            content = f.write(content)
        self._unlock_file(lock_file_id)
        
    def lock_and_read_json(self, default: dict) -> dict:
        """ Lock, read and convert json content of file to dict"""
        
        default_str = json.dumps(default)
        content = self._lock_and_read(default_str)
        json_content = json.loads(content)
        return json_content
    
    def lock_and_write_json(self, json_data: dict):
        """ Lock and write json serialized content to file"""
        content = json.dumps(json_data)
        self.lock_and_write(content)
        
    def lock_and_update_json(self,  update_method: Callable):
        """ Lock and update json content of file """
        
        lock_file_id = self._lock_file()
        try:
            json_file = open(self.path, 'r')
            json_data = json.load(json_file)
            json_file.close()
        except FileNotFoundError:
            self._unlock_file(lock_file_id)
            raise Exception(str(self.path) + ' not found')

        update_method(json_data)

        json_file = open(self.path, 'w')
        json.dump(json_data, json_file, indent=4)
        json_file.close()

        # unlock process file
        self._unlock_file(lock_file_id)

    def unlink(self):
        """ Wait until file is unlocked and delete it """

        lock_file_id = self._lock_file()
        self.path.unlink()
        self._unlock_file(lock_file_id)
