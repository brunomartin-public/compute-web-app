from dataclasses import dataclass, field
from typing import List, Dict
from enum import Enum

from cwa.base.data import Data


class ItemState(Enum):
    EMPTY = 0x00
    QUEUED = 0x01
    FILLED = 0x02
        
    def __le__(self, value: 'ItemState'):
        return self.value <= value.value

@dataclass
class DataCollectorStatus:
    """Collector status"""
    
    # PID of the collector process
    pid: int = -1
    
    # Tell if collector has been asked to stop
    asked_to_stop: bool = False
    
    # Expected group count
    group_count = -1
    
    # Common dataset states
    common_dataset_states: Dict[str, ItemState] = \
        field(default_factory=lambda: {})
    
    # Common attributes states
    common_attributes_states: ItemState = None
    
    # Group dataset states
    group_dataset_states: Dict[str, List[ItemState]] = \
        field(default_factory=lambda: {})
    
    # Group attributes states
    group_attributes_states: List[ItemState] = \
        field(default_factory=lambda: {})
    
    def populate(self, data: Data) -> None:
        """Populate dataset status accroding to data

        Args:
            data (Data): data from which read expected dataset
        """
        
        self.group_count = data.group_count
            
        for dataset in data.datasets:
            self.common_dataset_states[dataset.name] = ItemState.EMPTY
            
        if len(data.attributes):
            self.common_attributes_states = ItemState.EMPTY
        
        for dataset in data.group_datasets:
            self.group_dataset_states[dataset.name] = \
                [ItemState.EMPTY]*data.group_count
        
        if len(data.group_attributes) > 0:
            self.group_attributes_states = \
                [ItemState.EMPTY]*data.group_count
    
    
    def _get_progress(self, target_state: ItemState) -> int:
        """Compute and output progress in percent

        Args:
            state (ItemState): state for progress computing

        Returns:
            int: state progress in percent
        """
        
        # If this status has not been populated, return a full status
        if self.group_count == -1:
            return 100
        
        # Compute group states
        group_states = \
            [x for x in self.group_dataset_states.values()]
        if len(self.group_attributes_states) > 0:
            group_states.append(self.group_attributes_states)
        
        group_result = ([all(y >= target_state for y in x) \
                for x in zip(*group_states)]).count(True)
        
        # Compute common states
        common_states = list(self.common_dataset_states.values())
        if self.common_attributes_states:
            common_states.append(self.common_attributes_states)
        
        common_result = \
            all([y >= target_state for y in common_states])
        
        # Consider group result as 99%, and common result as 1% 
        result = int(group_result/self.group_count * 99 + \
            int(common_result))
        
        return result
        
    def get_queue_progress(self) -> int:
        """Compute and output queue progress in percent

        Returns:
            int: queue progress in percent
        """
        
        return self._get_progress(ItemState.QUEUED)
    
    def is_data_queued(self) -> bool:
        """Tell if data is filled according to group count

        Returns:
            bool: True if filled else False
        """
        
        return self.get_queue_progress() == 100
    
    def get_fill_progress(self) -> int:
        """Compute and output queue progress in percent

        Returns:
            int: queue progress in percent
        """
        
        return self._get_progress(ItemState.FILLED)
    
    def is_data_filled(self) -> bool:
        """Tell if data is filled according to group count

        Returns:
            bool: True if filled else False
        """
        
        return self.get_fill_progress() == 100
