from pathlib import Path
from datetime import datetime, timezone
import re
import sys
from typing import List

from threading import Thread, Event
import subprocess

from cwa.base.lockable_file import LockableFile
from cwa.base.process import Process, ProcessStatus

class ProcessThread(Thread):
    """ Class that start and monitor a process.
    """
    
    def __init__(self, running_ids_filename: Path, process: Process,
                 started_event: Event, working_directory: Path,
                 result_path: Path, process_args, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Path to file storing currently running process
        self._running_ids_filename: Path = running_ids_filename
        
        # Object containg process information
        self._process: Process = process
        
        # Event telling of process has been started
        self._started_event: Event = started_event
        
        # Process working directory
        self._working_directory: Path = working_directory
        
        # Result path
        self._result_path: Path = result_path
        
        # Argument to give to process
        self._process_args: List[str] = process_args
        
    def _parse_log(self, line):

        # try to find a "[PROGRESS XX%]" in log
        # check progress, store result
        matches = re.findall(r'.*\[PROGRESS\s([0-9]+)[%]\].*', line)

        # get the last one of the line,
        if len(matches) > 0:
            progress = int(matches[-1])
            self._process.status.progress = progress

        # check errors, store all errors found in the line
        matches = re.findall(r'.*\[ERROR\s*(.*)\].*', line)

        for match in matches:
            self._process.status.errors.append(match)

    def run(self):
        """ Start process and monitor it."""
        
        process = self._process
        args = self._process_args
        cwd = self._working_directory
        
        # Update process information
        process.start_time = datetime.now(timezone.utc).isoformat()
        
        process_json_filepath = Path(self._result_path) / \
            (process.id + '.json')
        
        try:
            # add this process id to running processes
            running_process_filepath = Path(self._result_path) / \
                self._running_ids_filename
            
            def add_process(json_data: list):
                if process.id not in json_data:
                    json_data.append(process.id)
            LockableFile(running_process_filepath) \
                .lock_and_update_json(add_process)
            
            log_filepath = process.log_filepath
            log_file = open(log_filepath, 'w+')
            
            return_code = -1
            
            print('Starting process with cwd: {}\n and args: {}'
                  .format(cwd, ' '.join(args)))
            
            inner_process = subprocess.Popen(
                args=args, stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT, cwd=cwd,
                text=True, executable=str(Path(cwd) / args[0])
            )
            process.pid = inner_process.pid
            
            LockableFile(process_json_filepath).lock_and_write(
                process.dumps_json())
            
            self._started_event.set()
            
            process_finished = False
            while not process_finished:
                line = inner_process.stdout.readline()
                return_code = inner_process.poll()
                if line == '' and return_code is not None:
                    process_finished = True
                    continue
                if line:
                    self._parse_log(line)
                    
                    # update log file
                    log_file.write(line)
                    log_file.flush()
                    
                    LockableFile(process_json_filepath).lock_and_write(
                        process.dumps_json())
            
            process.end_time = datetime.now(timezone.utc).isoformat()
            process.pid = -1

            # if bad return code, add it to errors and at this end of log file
            if return_code != 0:
                print('return_code : {}'.format(return_code))
                message = 'Bad return code : {}'.format(return_code)
                process.status.errors.append(message)
                
                line = '[ERROR ' + message + ']\n'
                log_file.write(line)
                log_file.flush()
                
            log_file.close()
            
            # Try to clear the file consistency flag removing swmr tags from
            # file so that it can be read by Matlab < 2021b, for instance
            # and where the process has not been flaged as early_process
            try:
                parameters = process.parameters
                result_file = next(
                    (x.value for x in parameters if x.name == 'result_file'),
                    None)
                subprocess.call('h5clear -s ' + result_file, shell=True)
            except OSError as e:
                print('h5clear execution failed:', e, file=sys.stderr)
                
            # update process without changing progress
            process.status.running = False
            process.status.done = len(process.status.errors) == 0
            
            LockableFile(process_json_filepath).lock_and_write(
                process.dumps_json())
            
            # remove this process id from running processes
            def remove_process(json_data: list):
                if process.id in json_data:
                    json_data.remove(process.id)
            LockableFile(running_process_filepath) \
                .lock_and_update_json(remove_process)
                    
        except FileNotFoundError as e:
            message = 'FileNotFoundError : {}'.format(e)
            print(message)
            return_code = -1
            process.status.errors.append(message)
