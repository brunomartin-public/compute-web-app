import logging
import time
import pickle
import struct
import psutil
from typing import Union
import numpy as np

from multiprocessing import shared_memory, Event

from cwa.base.shm_lock import ShmLock
from cwa.base.data import Data, DatasetValue, GroupDatasetValue, \
    AttributeValues, GroupAttributeValues
from cwa.base.data_collector import DataCollector, \
    DataCollectorItem
from cwa.base.data_collector import DataCollectorStatus
from cwa.base.data_supplier import DataSupplier

class DataStore:
    """ This class purpose is to collect data to supply them to
    processes. One DataStore shall be created for each data and
    shared memory is used to collect parts of data.
    """
    
    def __init__(self, data_supplier: DataSupplier,
                 shm_size: int=256*256*20*2, lock_timeout: float = 10,
                 poll_interval = 0.01):
        
        # Data supplier to effectively make data available to data
        # processes
        self._data_supplier: DataSupplier = data_supplier
        
        # Shared memory segment size in bytes
        self._shm_size: int = shm_size
        
        # Shared memory segment name
        self._shm_name: str = data_supplier.data_id[:16]
        
        # Timeout in seconds for locking access to shm content
        self._lock_timeout: float = lock_timeout
        
        # Poll interval in seconds for reading shm content
        self._poll_interval: float = poll_interval
        
        # Shared memory lock for shm data transfer
        self._transfer_lock = ShmLock(
            self._shm_name + '-transfer', self._lock_timeout,
            self._poll_interval)
        
        # Shared memory lock for shm data read/write
        self._read_write_lock = ShmLock(
            self._shm_name + '-rw', self._lock_timeout,
            self._poll_interval)
        
        # Shared memory lock for shm status read/write
        self._status_lock = ShmLock(
            self._shm_name + '-status', self._lock_timeout,
            self._poll_interval)
        
        # Logger for this class
        self._logger = logging.getLogger(__name__)

    def _start_collector(self):
        """ Prepare connection and start process. """
        
        # Try to create shm block. If succeeds, start the data collector
        # process, if fails, that means that collector process has
        # already been started by another process
        # shm block contains data and status to avoid adding complexity
        # on attempt to get unicity of collector process creation.
        try:
            shm = shared_memory.SharedMemory(
                name=self._shm_name, create=True,
                size=self._shm_size)
            # If running on CI docker, it seems that 512*512*50*2 bytes,
            # i.e. 25MB, is too much and causes following error:
            # Fatal Python error: Bus error
            # Setting 256*256*20*2, i.e. 2.5MB solves the issue
            shm.buf[:self._shm_size] = bytes(self._shm_size)
            
            # Initialize here colletor status so that other processes
            # may read it from now
            data = pickle.dumps(DataCollectorStatus())
            shm.buf[:len(data)] = data
            
            process = DataCollector(
                shm, self._read_write_lock,
                self._status_lock, self._data_supplier,
                self._lock_timeout, self._poll_interval,
                daemon=True)
            process.start()
        except FileExistsError:
            pass
        
    def wait_for_collector(self, force_stop: bool = False) -> None:
        """Wait for collector process to finish

        Args:
            force_stop (bool, optional): Force to stop it. Defaults to False.

        Raises:
            TimeoutError: When waiting too long
        """
        
        status = self.get_collector_status()
        # If pid has not been set, suppose collector process is not
        # running
        if status.pid < 0:
            return
        
        if force_stop:
            self._stop_collector()
            
        try:
            collector_process = psutil.Process(pid=status.pid)
            collector_process.wait(self._lock_timeout)
        except psutil.NoSuchProcess:
            pass
        except psutil.TimeoutExpired:
            raise TimeoutError
            
    def create(self, data: Data):
        """Create a new data to be filled, data shall be created when
        return from this method.

        Args:
            data (Data): data to create
        """
        
        # Start collector here
        self._start_collector()
        
        self._queue_item(DataCollectorItem(-1, data))
        
    def put(self, value: Union[DatasetValue, AttributeValues, \
        GroupDatasetValue, GroupAttributeValues]):
        """Put content part to fill created data

        Args:
            value (DatasetValue | DataValue): content representing dataset value
        """
        
        # If item concerns a group, first one shall be handled first
        priority = -1
        if isinstance(value, GroupDatasetValue) or \
            isinstance(value, GroupAttributeValues):
            priority = value.group_id
            
        self._queue_item(DataCollectorItem(priority, value))
            
    def load_data(self):
        """ Load data content of this supplier and return it """
        return self._data_supplier.load_data()
    
    def load_dataset_value(self, group_id, dataset_id,
                           start=None, length=None,
                           query_data_type: str=None) -> np.ndarray:
        
        value = self._data_supplier.load_dataset_value(
            group_id, dataset_id, start, length)
        
        # default data type is the one of the result file
        # if type is in query, then try to return this data type
        if query_data_type:
            
            # Convert query data type to a numpyp one
            # TODO: check if it is required and document in case
            if query_data_type == 'int8':
                query_data_type = 'i1'
            elif query_data_type == 'uint8':
                query_data_type = 'u1'
            elif query_data_type == 'int16':
                query_data_type = 'i2'
            elif query_data_type == 'uint16':
                query_data_type = 'u2'
            else:
                raise TypeError('Type not supported')

            data_type = value.dtype
            if data_type in [np.float32, np.float64]:
                if query_data_type in ['u1', 'u2']:
                    # move value on uint8/uint16 full scale and
                    # convert it to uint8/uint16 type
                    if value.max() != value.min():
                        value -= value.min()

                        max_value = np.iinfo(query_data_type).max
                        value *= float(max_value)/value.max()

                    value = value.astype(query_data_type)
            else:
                raise TypeError('Type not supported')
            
        return value
    
    def get_collector_status(self) -> DataCollectorStatus:
        """Get collector status

        Returns:
            DataCollectorStatus: current collector status or None if
                collector is not running
        """
        
        # At this step, shm shall be created, if not, collector process
        # has already finished
        try:
            shm = shared_memory.SharedMemory(
                name=self._shm_name, create=False,
                size=self._shm_size)
        except FileNotFoundError:
            return DataCollectorStatus()
            
        # Load status
        self._status_lock.lock()
        status: DataCollectorStatus = pickle.loads(shm.buf)
        self._status_lock.unlock()
        
        shm.close()
        
        return status
    
    def _stop_collector(self) -> None:
        """Stop collector process

        """
        
        # At this step, shm shall be created, if not, collector process
        # has already finished
        try:
            shm = shared_memory.SharedMemory(
                name=self._shm_name, create=False,
                size=self._shm_size)
        except FileNotFoundError:
            return
            
        # Load status
        self._status_lock.lock()
        status: DataCollectorStatus = pickle.loads(shm.buf)
        status.asked_to_stop = True
        data = pickle.dumps(status)
        shm.buf[:len(data)] = data
        self._status_lock.unlock()
        
        shm.close()
        
        return status
    
    def is_data_filled(self) -> bool:
        """Tell if data is filled
        
        Returns:
            bool: True if data is filled else False
        """
        
        # Collector status is None if process is not running
        # We suppose that not running means filled
        status = self.get_collector_status()
        return status.is_data_filled() if status else True
    
    def _queue_item(self, item: DataCollectorItem):
        """Queue item contained data to make it available to processes
        
        Args:
            item (DataCollectorItem): item to queue
        """
                
        # Serialize data and update length
        data = pickle.dumps(item)
        data_len = len(data)
        
        # First lock for transfer, so that other process won't interfer
        # with transfer then lock read/write access to data for writing
        self._transfer_lock.lock()
        self._read_write_lock.lock()
        
        # At this step, shm shall be created
        shm = shared_memory.SharedMemory(
            name=self._shm_name, create=False,
            size=self._shm_size)
        
        # Now shm is locked, update it
        shm_buf_data = \
            shm.buf[DataCollector._shm_status_len:self._shm_size]        
        shm_buf_data[:4] = struct.pack('I', data_len)
        shm_buf_data[4:4+data_len] = data
        
        # Now unlock transfer write to let collector process read data
        self._read_write_lock.unlock()
        
        # Wait until data lenght has been updated to 0 testifying that
        # collector process has queued item
        start_time = time.time()
        data_len = -1
        while time.time() - start_time < self._lock_timeout:
            self._read_write_lock.lock()
            data_len, = struct.unpack('I', shm_buf_data[:4])
            self._read_write_lock.unlock()
            if data_len == 0:
                break
            time.sleep(self._poll_interval)
        
        assert data_len == 0, 'Data length has not been updated'
        
        # Transfer is done so unlock it now
        self._transfer_lock.unlock()
        
        # Delete unless get a BufferError: cannot close exported
        # pointers exist
        del shm_buf_data
        shm.close()
