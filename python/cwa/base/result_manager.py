from pathlib import Path
from typing import List

import numpy as np

from cwa.base.hdf5_file import H5File
from cwa.base.result import Result
from cwa.base.data_manager import DataManager

class ResultManager(DataManager):
    """ This class purpose is to handle output result reading
    """

    def load_result(self, result_id: str):
        """ Load content from result matching result id. """

        # construct result file path
        filename = result_id + '.h5'
        filepath = self.data_path / filename

        if not filepath.is_file():
            raise Exception(f'Result {result_id} not found.')

        # load result file information        
        result = Result()

        file = H5File.open_file_for_reading(filepath)
        file.load_group_attrs(result)
        file.close()
        
        result.id = result_id

        return result
        
    def load_group_datasets(self, result_id: str, group_id):
        """ Load groups datasets. """

        # construct result file path
        filename = result_id + '.h5'
        filepath = self.data_path / filename

        group_name = '%03d' % group_id

        response = {}

        f = H5File.open_file_for_reading(filepath)

        group = f[group_name]
        for attr in group.attrs:
            response[attr] = group.attrs[attr]

        datasets = []
        for dataset in group:
            datasets.append(dataset)

        response["datasets"] = datasets
        f.close()

        return response
    
    def load_group_dataset(self, result_id: str, group_id, dataset_id):

        # construct result file path
        filename = result_id + '.h5'
        filepath = self.data_path / filename

        # check if file exists
        try:
            f = H5File.open_file_for_reading(filepath)
        except OSError as e:
            return "File " + str(filepath) + " does not exists", 400

        # check if group exists
        try:
            group_name = '%03d' % group_id
            group = f[group_name]
        except KeyError:
            return "Group does not exists", 400

        # check if dataset exists
        try:
            dataset = group[dataset_id]
        except KeyError:
            return "Dataset does not exists", 400
        
        return dataset
