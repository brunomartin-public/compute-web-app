from connexion import FlaskApp

from flask import render_template
from flask_cors import CORS

import sys
import os
import logging
from pathlib import Path

from cwa.base.lockable_file import LockableFile
from cwa.base.data_manager import DataManager
from cwa.base.process_def_manager import ProcessDefManager
from cwa.base.process_manager import ProcessManager
from cwa.base.result_manager import ResultManager


def create_app():
    data_path = Path(__file__).parent / 'tests' / 'data'
    process_def_path = Path(__file__).parent.parent / 'process_definition'
    result_path = Path(__file__).parent / 'tests' / 'result'

    if os.getenv('CWA_DATA_PATH'):
        data_path = Path(os.environ['CWA_DATA_PATH'])

    if os.getenv('CWA_PROCESS_DEFINITION_PATH'):
        process_def_path = Path(os.environ['CWA_PROCESS_DEFINITION_PATH'])

    if os.getenv('CWA_RESULT_PATH'):
        result_path = Path(os.environ['CWA_RESULT_PATH'])

    app = CwaApp(
        data_path=data_path,
        process_def_path=process_def_path,
        result_path=result_path
    )

    return app


class CwaApp(FlaskApp):
    def __init__(self, data_path: Path, process_def_path: Path,
                result_path: Path):

        if getattr(sys, 'frozen', False):
            # we are running in a bundle
            bundle_dir = sys._MEIPASS
        else:
            # we are running in a normal Python environment
            bundle_dir = Path(__file__).parent

        # set static folder to vue js dist directory
        if getattr(sys, 'frozen', False):
            static_folder = Path(bundle_dir).parent / 'frontend' / 'dist'
            specification_dir = Path(bundle_dir).parent / 'openapi'
        else:
            static_folder = Path(bundle_dir).parent.parent / 'frontend' / 'dist'
            specification_dir = Path(bundle_dir).parent.parent / 'openapi'

        # Call parent init here
        super().__init__(
            __name__,
            specification_dir = specification_dir,
            server_args = {
                'static_folder': static_folder,
                'template_folder': static_folder,
                'static_url_path': ''
            }
        )
        
        # Specify app type
        self.app : CwaApp

        # enable CORS
        CORS(self.app)

        # Create the application instance
        # add swagger_ui to url path /api/ui
        # app = connexion.App(__name__, options={"swagger_ui": True})
        # super().__init__(__name__, options={"swagger_ui": True})

        # pass configured flask app for static_url_path for instance
        # hoping it won't bug connexion app
        # self.app = flask_app

        logging.basicConfig()
        logger = logging.getLogger('connexion.apis.flask_api')
        logger.setLevel(logging.INFO)

        # By default, swagger ui page is available at /api/ui
        self.add_api('openapi3.yml')

        # remove lock files if exists
        file_list = list(Path.rglob(data_path, '*.lock'))
        file_list.extend(list(Path.rglob(result_path, '*.lock')))
        file_list.append(Path('write_process'))

        # Iterate over the list of filepaths & remove each file.
        for file_path in file_list:
            if not Path(file_path).is_file():
                continue
            try:
                file_path.unlink()
            except OSError:
                print("Error while deleting file : ", file_path)

        # create empty dynamic files
        running_process_filepath = result_path / ProcessManager.running_ids_filename
        LockableFile(running_process_filepath).lock_and_write_json(list())

        self.app.bundle_dir = bundle_dir

        data_manager = DataManager(data_path)
        process_def_manager = ProcessDefManager(process_def_path)
        process_manager = ProcessManager(data_path, process_def_manager,
            result_path)
        result_manager = ResultManager(result_path)

        self.app.data_manager = data_manager
        self.app.process_def_manager = process_def_manager
        self.app.process_manager = process_manager
        self.app.result_manager = result_manager

        # Create a URL route in our application for "/"
        @self.route('/')
        def home():
            """
            This function just responds to the browser ULR
            localhost:5000/

            :return:        the rendered template 'home.html'
            """
            return render_template('index.html')
