Compute Web App in python
===

### 1. Introduction

This project has been started some years ago with Python 3.8. It is now working and tested with Python version from 3.8 to 3.11.

Sources are in `cwa` subdirectory and are organized as follow:
- `cwa/base` containes low level classes for retrieving data, listing process definitions, run processes and expose results,
- `cwa/api` contains endpoints pointed in `openapi3` file (upper level) and is built on top of base,
- `cwa/tests` contains tests for `cwa` package and
- `examples` subdirectory contains samples of how to use `cwa`, check it.

This Python project uses HDF5 file via `h5py` module. Data are posted/put via HTTP RESTful API. Parallelization supposes that input data can be sperated in parts that can be processed independently of the other. Thus, this independent data parts are stored in groups in h5 file. each group may contain multiple datasets and dataset at file root are common to all independent data parts.

Example organization for IRMq data (D for Dataset and G for Group):
```
data.h5
| - echo_times (D)
| - slice_locations (D)
| - 000 (G)
    | - magnitude (D)
    | - phase (D)
|- 001 (G)
    |- magnitude (D)
    |- phase (D)
...
|- 007 (G)
    |- magnitude (D)
    |- phase (D)
```

Information from `cwa` are stored in those file to let processes react on file states like group entirely stored or file entirely stored. React on group entirely stored may be used with HDF5 swmr feature. this information shall be stored into groups at root level and in each group so that flush/refresh will be seamless, fast and efficient.

TODO: today, it is not the case. cwa dedicated groups has to be implemented hoping that h5py let flushing/refreshing a group.

### 2. Prepare and run

- Create a virtual environment and install requirements:
```
# python3 -m venv .venv
# source .venv/bin/activate
# pip install -r requirements.txt
```

- Start the web application, by default, a `tmp` subdirectory is create with `data` and `result` and `examples` is the process definition path:
```
# python app.py
```

- Each example has a `remote/example.py` script that can be used to fill data and run processes. Check `examples` subdirectory `README` to know more.

### 3. Test

This project uses `pytest` package for tests, so, run tests with following command line:
```
# pytest -v
```

Tests generates data `cwa/tests/tmp` directory and can be manipulated by the running application from previous section.

### 4. CWA systemd service and ngynx proxy

**This part needs to be updated, CWA gunicorn support seems to be broken**

- create cwa user for cloning sources, build and run service
```
$ adduser --gecos "" cwa
```

- create a systemd service file:
```
$ nano /etc/systemd/system/cwa.base.service
$ cat /etc/systemd/system/cwa.base.service
[Unit]
Description=Compute Web App
After=network.target mnt-wibble.mount

[Service]
Type=simple
SyslogIdentifier=cwa
PermissionsStartOnly=true
User=cwa
WorkingDirectory=/home/cwa/compute-web-app/
ExecStart=/home/cwa/compute-web-app/.venv/bin/gunicorn -c gunicorn.py 'cwa.app:create_app()' -e CWA_DATA_PATH=/home/cwa/data
StandardOutput=journal+console
TimeoutSec=5
KillSignal=SIGINT

[Install]
WantedBy=multi-user.target
```

- enable and start service
```
$ systemctl enable cwa.base.service
$ systemctl start cwa.base.service
```

- if using different application data path, you will need to create a virtual environment .venv in that directory
```
$ cd /home/cwa/data/process_definition
$ python3 -m venv .venv
$ pip install -r requirement.txt
```


- create a nginx configuration for serving CWA
```
$ nano /etc/nginx/site-available/10.7.0.3.conf
$ cat /etc/nginx/site-available/10.7.0.3.conf
server {
    listen       443 ssl http2 default_server;
    listen       [::]:443 ssl http2 default_server;
    server_name  10.7.0.3;
    
    location / {
        proxy_pass http://127.0.0.1:8042;
    }

    ssl_certificate "/etc/letsencrypt/live/vps01.brunocsmartin.fr/cert.pem";
    ssl_certificate_key "/etc/letsencrypt/live/vps01.brunocsmartin.fr/privkey.pem";
    ssl_session_cache shared:SSL:1m;
    ssl_session_timeout  10m;
    ssl_ciphers PROFILE=SYSTEM;
    ssl_prefer_server_ciphers on;
}
```

- enable it and restart nginx
```
$ ln -s /etc/nginx/site-available/10.7.0.3.conf /etc/nginx/site-enabled/
$ systemctl restart nginx
```

- generated h5 file are in general in v110. Some application can only read v180. To convert them, use h5tool in result directory :
```
$ for f in *.h5;  do h5format_convert ${f}; done;
```
