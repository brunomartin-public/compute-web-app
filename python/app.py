import argparse
from distutils import util as distutils

import os
import sys
import platform
import multiprocessing
from pathlib import Path

# for window production server
from waitress import serve


# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    multiprocessing.freeze_support()

    # Define paths for data, process definition and results
    # Point them to test one so that test data can be manipulated
    data_path = Path(__file__).parent / 'tmp' / 'data'
    process_def_path = Path(__file__).parent / 'examples'
    result_path = Path(__file__).parent / 'tmp' / 'result'

    if os.getenv('CWA_DATA_PATH'):
        data_path = Path(os.environ['CWA_DATA_PATH'])

    if os.getenv('CWA_PROCESS_DEFINITION_PATH'):
        process_def_path = Path(os.environ['CWA_PROCESS_DEFINITION_PATH'])

    if os.getenv('CWA_RESULT_PATH'):
        result_path = Path(os.environ['CWA_RESULT_PATH'])

    host_address = '0.0.0.0'
    host_port = 5000

    debug = True
    if getattr(sys, 'frozen', False):
        # we are running in a bundle
        debug = False

    # parse command line arguments, if any, they be erased config value
    parser = argparse.ArgumentParser()

    parser.add_argument("--address", default=host_address,
                        help='Set application host address (default: %(default)s)')
    
    parser.add_argument("--port", default=host_port,
                        help='Set application host port (default: %(default)s)')
    
    parser.add_argument("--data-path", default=data_path,
                        help='Set application data path (default: %(default)s)')
    
    parser.add_argument("--process-definition-path", default=process_def_path,
                        help='Set application process definition path (default: %(default)s)')
    
    parser.add_argument("--result-path", default=result_path,
                        help='Set application result path (default: %(default)s)')

    args = parser.parse_args()

    host_address = args.address
    host_port = int(args.port)
    data_path = Path(args.data_path)
    process_def_path = Path(args.process_definition_path)
    result_path = Path(args.result_path)

    # create directory of they not exist
    data_path.mkdir(exist_ok=True, parents=True)
    result_path.mkdir(exist_ok=True, parents=True)

    from cwa.app import CwaApp
    app = CwaApp(
        data_path=data_path,
        process_def_path=process_def_path,
        result_path=result_path
    )

    try:
        if debug:
            app.run(host=host_address, port=host_port)
        else:
            serve(app, host=host_address, port=host_port, threads=10)
    except KeyboardInterrupt as e:
        # Catch keyboard interrupt as serve may be stopped by a ctrl+c
        pass
