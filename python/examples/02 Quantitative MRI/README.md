Compute Web App example: Quantitative MRI
===

This subdirectory contains an example of how to use `cwa` for processing quantitative MRI data processing.

Ideally, this example will:
- Choose a data of a study from an open MRI database (for instance https://openneuro.org),
- Create one or multiple data on `cwa` with the adapted parameters,
- Start downloading data and feeding `cwa` data created,
- Start a fat water seperation data processing,
- Wait until data processing first result and
- Display results.

> qMRI example would be satisfying if we could extract PDFF information from it. Unfortunately, current data process is not conclusive and need to be enhanced.

> And time is running out, this example will be enhanced soon.

This subdirectory contains the necessary to be identified and run by CWA. Still, requirements has to be installed, so go to that directory and enter the following commands:
```
# python3 -m venv .venv
# source .venv/bin/activate
# pip install -r requirements.txt
```

The `remote` subdirectory contains scripts that shows how to post data, run process and retrieve result. Start a CWA instance, go to the subdiretory and do the usual:
```
# python3 -m venv .venv
# source .venv/bin/activate
# pip install -r requirements.txt
# python example.py
```
