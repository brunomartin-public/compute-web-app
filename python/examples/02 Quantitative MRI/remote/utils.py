from typing import List
from pathlib import Path
import nibabel as nib
import numpy as np


def download_and_extract_data() \
    -> tuple[List[np.ndarray], List[np.ndarray], List[np.ndarray],
             float]:
    """Download MRI magnitude and phase data from Openneuro and store it
        in [mag|phase]_XX.[json|nii.gz] formatted files in dowload
        subdirectory.
    """

    # Use Openneuro database for this example and choose the 
    # "The FreeSurfer Maintenance Dataset" (Thank you!)

    echo_count = 12
    base_url = \
        'https://s3.amazonaws.com/openneuro.org/ds004958/sub-fsm02cd'
    
    download_dir = Path(__file__).parent / 'download'
    download_dir.mkdir(exist_ok=True)

    cached_data_filename = 'cached_data'

    cached_data_filepath = download_dir /  (cached_data_filename + '.npz')
    if cached_data_filepath.exists():
        cached_data = np.load(cached_data_filepath)
        magnitudes = cached_data['magnitudes']
        phases = cached_data['phases']
        echo_times = cached_data['echo_times']
        imaging_frequency = cached_data['imaging_frequency']
        return magnitudes, phases, echo_times, imaging_frequency

    for echo in range(1, echo_count + 1):
        for dtype in ['mag', 'phase']:
            url = base_url + \
                '/anat/sub-fsm02cd_acq-gre3d_echo-' \
                f'{echo}_part-{dtype}_MEGRE'

            # Download data nii and json files if not downloaded yet
            filepath = download_dir /  f'{dtype}_{echo:02}.json'
            if not filepath.exists():
                print(f'Downloading {filepath.name}...')
                response = requests.get(url + '.json')
                open(filepath, 'wb').write(response.content)

            filepath = download_dir /  f'{dtype}_{echo:02}.nii.gz'
            if not filepath.exists():
                print(f'Downloading {filepath.name}...')
                response = requests.get(url + '.nii.gz')
                open(filepath, 'wb').write(response.content)

    mag_filepaths = sorted(Path('download').glob('mag_*.nii.gz'))
    phase_filepaths = sorted(Path('download').glob('phase_*.nii.gz'))
    json_filepaths = sorted(Path('download').glob('mag_*.json'))

    assert len(mag_filepaths) == len(phase_filepaths), \
        'There shall be equal number of mag and phase echos'
    
    # Load first mag to get sizes
    filepath = mag_filepaths[0]
    img = nib.load(filepath)
    dtype = img.get_data_dtype()
    height, width, depth = img.shape

    # Build destination data
    echo_indices = range(echo_count)
    slice_indices = range(depth)
    magnitudes: List[np.ndarray] = []
    phases: List[np.ndarray] = []

    imaging_frequency = -1
    echo_times = np.ndarray((echo_count, ), np.double)
    for _ in slice_indices:
        shape = [height, width, echo_count]
        magnitudes.append(np.ndarray(shape, dtype))
        phases.append(np.ndarray(shape, dtype))

    # Iterate over file and extract content to destination structures
    for mag_filepath, phase_filepath, json_filepath, echo_index in \
        zip(mag_filepaths, phase_filepaths, json_filepaths,
            echo_indices):
        print(f'Extracting {mag_filepath.name} and'
              f' {phase_filepath.name} content...')
        mag = nib.load(mag_filepath)
        phase = nib.load(phase_filepath)
        for slice_index in slice_indices:
            magnitudes[slice_index][:, :, echo_index] = \
                mag.get_fdata()[:, :, slice_index]
            phases[slice_index][:, :, echo_index] = \
                phase.get_fdata()[:, :, slice_index]
        
        json_file = open(json_filepath)
        json_data = json.load(json_file)
        echo_times[echo_index] = json_data['EchoTime']
        imaging_frequency  = json_data['ImagingFrequency']
        json_file.close()
    
    # Store data to avoid extracting them next time
    np.savez(download_dir / cached_data_filename,
             magnitudes=magnitudes,
             phases=phases,
             echo_times=echo_times,
             imaging_frequency=imaging_frequency)

    return magnitudes, phases
