"""This script describes how to interact programmatically with CWA.

This script do the following steps:
- dowload data from a database,
- interact with CWA via its RESTful API to:
  - create an input data,
  - fill datasets,
  - create and start processes with differents parameters,
  - retrieve results and
- display results in a matplolib animation.
"""

import json
import time
import sys

import requests
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

from utils import download_and_extract_data

if __name__ == '__main__':

    # Define address and port to communicate with CWA
    ip_address = 'localhost'
    ip_port = 5000

    # Define API base url
    api_url = 'http://' + ip_address + ':' + str(ip_port) + '/api'

    # Download and parse MRI data
    try:
        magnitudes, phases, echo_times, imaging_frequency = \
            download_and_extract_data()
    except Exception as e:
        print(f'Error: {str(e)}', file=sys.stderr)
        sys.exit(-1)

    # Declare groups and datasets they will contain
    groups = {
        'count': len(magnitudes),
        'datasets': [
            {
                'name': 'magnitude',
                'dtype': str(magnitudes[0].dtype),
                'shape': magnitudes[0].shape
            },
            {
                'name': 'phase',
                'dtype': str(phases[0].dtype),
                'shape': phases[0].shape
            },
        ],
    }

    # Declare data object to send to CWA
    data = {
        'name': 'MRI data samples',
        'version': '1.0',
        'groups': groups,
        'datasets': [
            {
            'name': 'echo_times',
            'dtype': str(echo_times.dtype),
            'shape': echo_times.shape
            }
        ],
        'attributes': [
            {
            'name': 'imaging_frequency',
            'dtype': str(imaging_frequency.dtype),
            'value': float(imaging_frequency)
            }
        ]
    }

    # Create a data on CWA and retrieve its data_id
    print('Posting data...')
    headers = {'Content-Type': 'application/json'}
    response = requests.post(api_url + '/data', data=json.dumps(data), headers=headers)
    result = json.loads(response.content)
    data_id = result['data_id']

    # Set header for sending bytes
    headers = {'Content-Type': 'application/octet-stream'}

    # Fill root dataset
    response = requests.put(
        api_url + '/data/' + data_id + '/dataset/echo_times/value',
        data=echo_times.tobytes(), headers=headers)
    assert response.ok, 'Error: ' + str(response.content)

    # Fill group datasets
    print('Filling datasets...')
    for group_id, (magnitude, phase) in enumerate(zip(magnitudes, phases)):
        base_url = api_url + '/data/' + data_id + '/group/' + '%03d' % group_id

        dataset_url = base_url + '/dataset/magnitude/value'
        response = requests.put(dataset_url,
                                data=magnitude.tobytes(),
                                headers=headers)
        assert response.ok, 'Error: ' + str(response.content)
        
        dataset_url = base_url + '/dataset/phase/value'
        response = requests.put(dataset_url,
                                data=phase.tobytes(),
                                headers=headers)
        assert response.ok, 'Error: ' + str(response.content)

    # Get process definition and get id of heat equation one
    headers = {'Content-Type': 'application/json'}
    response = requests.get(api_url + '/process_definition', headers=headers)
    process_defs = json.loads(response.content)
    process_def_id = \
        next((x['id'] for x in process_defs \
              if 'Quantitative MRI' in x['id']), None)
    assert process_def_id, 'Quantitative MRI process def not found.'

    # Build a new process from heat equaqtion process definition
    process = {
        'data_id': data_id,
        'process_def_id': process_def_id,
        'version': '1.0',
        'parameters': [
            {'name': 'threshold', 'type': 'd', 'value' : 10},
        ]
    }

    # Post it and get its process id
    print('Posting new process...')
    response = requests.post(api_url + '/process', data=json.dumps(process), headers=headers)
    result = json.loads(response.content)
    process_id = result['process_id']

    # Start this process
    process_action = {
        'process_id': process_id,
        'version': '1.0',
        'name': 'start',
    }

    print('Starting process...')
    response = requests.post(api_url + '/process_action', data=json.dumps(process_action))
    result = json.loads(response.content)

    print('Waiting for process to finish...')
    process_done = False
    last_progress = 0
    while not process_done:
        response = requests.get(api_url + '/process/' + process_id)
        result = json.loads(response.content)
        progress = result['status']['progress']

        if progress >= last_progress + 10:
            print(f'Progress: {progress}%')
            last_progress = progress
        
        process_done = result['status']['done']
        if process_done:
            break
        time.sleep(1)

    print('Retrieving results...')
    b0s = []
    b0_min = 0
    b0_max = 0
    for group_id in range(len(magnitudes)):
        dataset_url = api_url + '/result/' + process_id + '/group/' + \
            '%03d' % group_id
        dataset_url += '/dataset/b0/value'
        response = requests.get(dataset_url, headers=headers)

        dtype = 'd'
        shape = magnitudes[0].shape
        shape = (shape[0], shape[1])
        b0 = np.frombuffer(response.content, dtype=dtype,
                           count=np.prod(shape))
        b0 = np.reshape(b0, shape)

        if np.min(b0) < b0_min:
            b0_min = np.min(b0)

        if np.max(b0) > b0_max:
            b0_max = np.max(b0)

        b0s.append(b0)

    print('Plotting results...')
    def animate(k):
        global b0s
        b0 = b0s[k]

        # Clear the current plot figure
        plt.clf()

        plt.title(f'B0 at index {k+1}')
        plt.xlabel('x')
        plt.ylabel('y')

        plt.imshow(b0, cmap=plt.cm.jet, vmin=b0_min, vmax=b0_max)
        plt.colorbar()

        return plt

    anim = FuncAnimation(plt.figure(), animate, interval=1,
                         frames=len(b0s), repeat=False)
    plt.show()
