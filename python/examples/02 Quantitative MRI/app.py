import argparse
import time

import h5py
import numpy as np
from skimage.restoration import unwrap_phase

plot_data = False

if plot_data:
    import matplotlib.pyplot as plt

if __name__ == '__main__':

    # parse arguments
    parser = argparse.ArgumentParser()

    # parse process specific argument as positional arguments
    parser.add_argument('threshold', help='Threshold', default=100, type=float)

    # parse cwa specific argument as positional arguments at the end
    parser.add_argument('data_file', help='data file', default='', type=str)
    parser.add_argument('result_file', help='result file', default='', type=str)

    args = parser.parse_args()

    threshold = args.threshold
    data_filepath = args.data_file
    result_filepath = args.result_file

    # Open input file
    data_file = h5py.File(data_filepath, 'r', libver='latest', swmr=True)

    # Prepare result file
    result_file = h5py.File(result_filepath, 'w', libver='latest')
    group_names = []
    for group_name, group in data_file.items():
        # Process only groups
        if not isinstance(group, h5py.Group):
            continue

        shape = group['magnitude'].shape
        result_shape = (shape[0], shape[1])
        result_group = result_file.create_group(group_name)
        dataset_names = ['b0', 't2', 'water_mag', 'fat_mag', 'pdff']

        for dataset_name in dataset_names:
            result_group.create_dataset(
                dataset_name, shape=result_shape,
                dtype=np.float64, fillvalue=0, track_order=True
            )
        
        group_names.append(group_name)

    # Read root datasets and attributes
    echo_times = data_file['echo_times'][:].copy()
    imaging_frequency = data_file.attrs['imaging_frequency'].copy()

    for group_index, group_name in enumerate(group_names):
        group = data_file[group_name]

        # Check if data is stored before continue
        while not group.attrs['cwa_stored']:
            data_file.close()
            time.sleep(0.5)
            data_file = h5py.File(
                data_filepath, 'r', libver='latest', swmr=True)
            group = data_file[group_name]

        magnitude = group['magnitude'][:].astype(float).copy()
        phase = group['phase'][:].astype(float).copy()

        height = magnitude.shape[0]
        width = magnitude.shape[1]
        echos = magnitude.shape[2]

        result_group = result_file[group_name]

        # Compute a mask according to threshold
        mask = magnitude.min(axis=2).astype(float) > threshold
        mask_tiled = np.tile(mask[:, :, np.newaxis], (1, 1, echos))

        # Filter magnitude and phase according to mask
        magnitude[~mask_tiled] = 1e-12
        phase[~mask_tiled] = 0

        # Normalize phase between [-pi, +pi] and unwrap it
        phase /= np.amax(np.absolute(phase)) / np.pi

        mphase = np.ma.masked_array(phase.copy(), mask=~mask_tiled)
        mphase = unwrap_phase(mphase)
        phase = mphase.filled(0).copy()

        # Extract b0 from phase data using minimum least squares
        # y ~= b0 + b1*echo_times
        # (y - A*b)^2 -> 0, b=[b0, b1] ? with y=phase and A=[1; echo_times]
        # [b0, b1] = inv(A'.A).A'.y

        A = np.vstack((np.ones(echos), echo_times)).T
        y = np.reshape(phase, (-1, echos)).T
        A_pinv = np.linalg.inv(np.matmul(A.T, A))
        b = np.matmul(np.matmul(A_pinv, A.T), y)
        b = np.reshape(b.T,(height, width, -1))

        phase_fit = np.matmul(b, A.T)
        phase_res = phase - phase_fit

        # Compute relavtive Residual Sum of Squares
        phase_sse = np.sum(phase_res * phase_res, axis=2)
        phase_sse /= np.max(np.abs(phase_res))
        phase_sse_max = np.max(phase_sse)

        if plot_data:
            fig, ax = plt.subplots()

            ax1 = plt.subplot(231)
            cs = ax1.imshow(b[:, :, 0])
            ax1.set_title('phase b0')
            fig.colorbar(cs)

            ax2 = plt.subplot(232)
            cs = ax2.imshow(b[:, :, 1])
            ax2.set_title('phase b1')
            fig.colorbar(cs)

            ax3 = plt.subplot(233)
            cs = ax3.imshow(phase[:, :, 0])
            ax3.set_title('phase[:, :, 0]')
            fig.colorbar(cs)

            ax4 = plt.subplot(234)
            cs = ax4.imshow(phase_sse)
            ax4.set_title(f'phase_sse\nmax:{phase_sse_max:.2f}')
            fig.colorbar(cs)

            ax6 = plt.subplot(236)
            cs = ax6.imshow(phase_fit[:, :, 0])
            ax6.set_title('phase_fit[:, :, 0]')
            fig.colorbar(cs)

            plt.show()

        # Update mask removing out of fit pixels
        mask1 = mask.copy()
        mask &= phase_sse < 0.01*phase_sse_max
        mask_tiled = np.tile(mask[:, :, np.newaxis], (1, 1, echos))

        phase_res[~mask_tiled] = 0

        # Estimate magnitude parameters
        # y ~= b0*exp(-b1*echo_times)
        # log(y) ~= log(b0) - b1*echo_times
        # (log(y) - A*b)^2 -> 0, b=[log(b0), -b1] ? with y=magnitude and A=[1; echo_times]
        # [log(b0), -b1] = inv(A'.A).A'.y

        A = np.vstack((np.ones(echos), -echo_times)).T
        y = np.reshape(np.log(magnitude), (-1, echos)).T
        A_pinv = np.linalg.inv(np.matmul(A.T, A))
        b = np.matmul(np.matmul(A_pinv, A.T), y)
        b = np.reshape(b.T,(height, width, -1))

        mag = np.exp(b[:, :, 0]); mag[~mask] = 0
        t2 = 1 / b[:, :, 1]; t2[~mask] = 0

        # t2, relaxation time, shall be positive and not higher than a
        # second
        # Update mask removing out of fit pixels
        mask2 = mask.copy()
        mask &= (t2 > 0) & (t2 < 1)
        mask_tiled = np.tile(mask[:, :, np.newaxis], (1, 1, echos))

        mag[~mask] = 0
        t2[~mask] = 0

        magnitude_fit = np.exp(np.matmul(b, A.T))
        magnitude_fit[~mask_tiled] = 0
        magnitude_res = magnitude - magnitude_fit

        # Compute relavtive Residual Sum of Squares
        magnitude_sse = np.sum(magnitude_res * magnitude_res, axis=2)
        magnitude_sse /= np.max(np.abs(magnitude_res))
        magnitude_sse_max = np.max(magnitude_sse)

        if plot_data:
            fig, ax = plt.subplots()

            ax1 = plt.subplot(231)
            cs = ax1.imshow(mag)
            ax1.set_title('mag')
            fig.colorbar(cs)

            ax2 = plt.subplot(232)
            cs = ax2.imshow(b[:, :, 1])
            ax2.set_title('magnitude b1')
            fig.colorbar(cs)

            ax3 = plt.subplot(233)
            cs = ax3.imshow(magnitude[:, :, 0])
            ax3.set_title('magnitude[:, :, 0]')
            fig.colorbar(cs)

            ax4 = plt.subplot(234)
            cs = ax4.imshow(magnitude_sse)
            ax4.set_title(f'magnitude_sse\nmax:{magnitude_sse_max:.2f}')
            fig.colorbar(cs)

            ax5 = plt.subplot(235)
            cs = ax5.imshow(t2)
            ax5.set_title('t2')
            fig.colorbar(cs)

            ax6 = plt.subplot(236)
            cs = ax6.imshow(magnitude_fit[:, :, 0])
            ax6.set_title('magnitude_fit[:, :, 0]')
            fig.colorbar(cs)

            plt.show()

        # Update mask removing out of fit pixels
        mask3 = mask.copy()
        mask &= magnitude_sse < 0.01*magnitude_sse_max
        mask_tiled = np.tile(mask[:, :, np.newaxis], (1, 1, echos))

        if plot_data:
            fig, ax = plt.subplots()

            ax1 = plt.subplot(221)
            cs = ax1.imshow(mask1)
            ax1.set_title('mask1')
            fig.colorbar(cs)

            ax2 = plt.subplot(222)
            cs = ax2.imshow(mask2)
            ax2.set_title('mask2')
            fig.colorbar(cs)

            ax2 = plt.subplot(223)
            cs = ax2.imshow(mask3)
            ax2.set_title('mask3')
            fig.colorbar(cs)

            ax3 = plt.subplot(224)
            cs = ax3.imshow(mask)
            ax3.set_title('mask')
            fig.colorbar(cs)

            plt.show()

        magnitude[~mask_tiled] = 0
        phase_res[~mask_tiled] = 0
        mag[~mask] = 0
        t2[~mask] = 0

        # 1. From water and fat chemical shifts (DOI: 10.1002/mrm.28471)
        water_ppm = 4.7
        fat_peak_shifts_ppm = np.array([
            0.90, 1.30, 1.59, 2.03, 2.25, 2.77,
            4.10, 4.30, 5.21, 5.31])

        # 2. Compute fat delay vectors
        fat_peak_freqs = (water_ppm - fat_peak_shifts_ppm) * \
            imaging_frequency / 1e3
        fat_peak_freqs = (water_ppm - fat_peak_shifts_ppm) * \
            imaging_frequency
        fat_peak_delays = np.matmul(
            fat_peak_freqs[:, np.newaxis], echo_times[np.newaxis, :])
        fat_peak_oscills = np.cos(2 * np.pi * fat_peak_delays)

        # 3. Apply fat delay peak coefficients (DOI: 10.1002/mrm.28471)        
        ndb = 2.8; nmidb = 0.6; cl = 17.3; 
        fat_peak_coeffs = np.array([
            9, 6*(cl-4)-8*ndb+2*nmidb, 6, 4*(ndb-nmidb), 6, 2*nmidb,
            2, 2, 1, 2*ndb])
        fat_peak_coeffs[fat_peak_coeffs < 30] = 0
        
        # 4. Compute fat delay values
        fat_delay_values = np.matmul(fat_peak_coeffs, fat_peak_oscills)

        # 5. Correct residual phase
        phase_corrected = phase_res.copy()
        
        # phase_corrected -= \
        #     np.tile(phase_res[:, :, 0, np.newaxis], (1, 1, echos))
        
        # phase_corrected += \
        #     2*np.pi*np.tile(np.sum(fat_peak_delays[:, 0]),
        #                       (height, width, echos))
        
        phase_corrected[~mask_tiled] = 0

        # if plot_data:
        #     fig, ax = plt.subplots()

        #     for i in range(6):
        #         ax = plt.subplot(231 + i)
        #         cs = ax.imshow(phase_corrected[:, :, i])
        #         ax.set_title(f'phase_corrected[:, :, {i}]')
        #         fig.colorbar(cs)

        #     plt.show()

        # Build complete set to fit
        data_values = magnitude * np.cos(phase_corrected)

        # Extract water and fat coefficients from residual phase using
        # minimum least squares
        # y ~= b0 + b1*fat_delay_values
        # (y - A*b)^2 -> 0, b=[b0, b1] ? with y=cos(phase_res) and A=[1; fat_delay_values]
        # [b0, b1] = inv(A'.A).A'.y

        A = np.vstack((np.ones(echos), fat_delay_values)).T
        y = np.reshape(np.cos(phase_corrected), (-1, echos)).T
        A_pinv = np.linalg.inv(np.matmul(A.T, A))
        b = np.matmul(np.matmul(A_pinv, A.T), y)
        b = np.reshape(b.T,(height, width, -1))

        data_values_fit = np.matmul(b, A.T)
        data_values_fit[~mask_tiled] = 0

        if plot_data:
            mask_vector = np.reshape(mask, (-1, 1))
            data_values_fit_vector = np.reshape(data_values_fit, (-1, echos))

            # for i in range(y.shape[1]):
            #     if mask_vector[i]:
            #         plt.plot(y.T[i, :])
            #         plt.plot(data_values_fit_vector[i, :])
            #         plt.show()

        water_mag = mag * b[:, :, 0]
        fat_mag = mag * b[:, :, 1]

        water_mag[np.abs(water_mag) > 500] = 0
        fat_mag[np.abs(fat_mag) > 100] = 0

        pdff = fat_mag / (water_mag + fat_mag) * 100; pdff[~mask] = 0

        if plot_data:
            fig, ax = plt.subplots()
            ax1 = plt.subplot(231)
            cs = ax1.imshow(water_mag)
            ax1.set_title('water_mag')
            fig.colorbar(cs)

            ax2 = plt.subplot(232)
            cs = ax2.imshow(fat_mag)
            ax2.set_title('fat_mag')
            fig.colorbar(cs)

            ax2 = plt.subplot(233)
            cs = ax2.imshow(pdff)
            ax2.set_title('pdff')
            fig.colorbar(cs)

            ax3 = plt.subplot(234)
            cs = ax3.imshow(mask)
            ax3.set_title('mask')
            fig.colorbar(cs)

            plt.show()

        result_group['b0'][:] = mag
        result_group['t2'][:] = t2
        result_group['water_mag'][:] = water_mag
        result_group['fat_mag'][:] = fat_mag
        result_group['pdff'][:] = pdff

        progress = int((group_index+1)/len(group_names)*100)
        print("[PROGRESS {}%]".format(progress))

    result_file.close()

    print('Done!')
