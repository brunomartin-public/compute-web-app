"""This script describes how to interact programmatically with CWA.

This script do the following steps:
- generate input data,
- interact with CWA via its RESTful API to:
  - create an input data,
  - fill datasets,
  - create and start processes with differents parameters,
  - retrieve results and
- display results in a matplolib animation.
"""

from typing import List
import json
import time

import requests
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

def generate_input_data() -> List[np.ndarray]:

    base_source = np.empty((50, 50))
    base_source[:] = np.nan
    base_source[0, :] = 100
    base_source[:, -1] = 0
    base_source[-1, :] = 0
    base_source[:, 0] = 0

    sources = []

    source = base_source.copy()
    source[20:30, 20:30] = 50
    sources.append(source)

    source = base_source.copy()
    source[10:20, 10:20] = 50
    source[10:20, 30:40] = 50
    source[30:40, 30:40] = 50
    source[30:40, 10:20] = 50
    sources.append(source)

    return sources

if __name__ == '__main__':

    # Define address and port to communicate with CWA
    ip_address = 'localhost'
    ip_port = 5000

    # Define API base url
    api_url = 'http://' + ip_address + ':' + str(ip_port) + '/api'

    # Generate some heat source maps
    sources = generate_input_data()

    # Declare groups and datasets they will contain
    groups = {
        'count': len(sources),
        'datasets': [
            {
                'name': 'source',
                'dtype': 'd',
                'shape': sources[0].shape
            },
        ],
    }

    # Declare data object to send to CWA
    data = {
        'name': 'source samples',
        'version': '1.0',
        'groups': groups,
    }

    # Create a data on CWA and retrieve its data_id
    print('Posting data...')
    headers = {'Content-Type': 'application/json'}
    response = requests.post(api_url + '/data', data=json.dumps(data), headers=headers)
    result = json.loads(response.content)
    data_id = result['data_id']

    # Fill data
    print('Filling datasets...')
    for group_id, source in enumerate(sources):
        dataset_url = api_url + '/data/' + data_id + '/group/' + '%03d' % group_id
        dataset_url += '/dataset/source/value'
        headers = {'Content-Type': 'application/octet-stream'}
        response = requests.put(dataset_url, data=source.tobytes(), headers=headers)

    # Get process definition and get id of heat equation one
    headers = {'Content-Type': 'application/json'}
    response = requests.get(api_url + '/process_definition', headers=headers)
    process_defs = json.loads(response.content)
    process_def_id = \
        next((x['id'] for x in process_defs \
              if 'Heat equation' in x['id']), None)
    assert process_def_id, 'Heat equation process def not found.'

    # Build a new process from heat equaqtion process definition
    step_count = 200
    process = {
        'data_id': data_id,
        'process_def_id': process_def_id,
        'version': '1.0',
        'parameters': [
            {'name': 'step_count', 'type': 'u2', 'value' : step_count},
            {'name': 'thermal_diffusivity', 'type': 'd', 'value' : 2.},
        ]
    }

    # Post it and get its process id
    print('Posting new process...')
    response = requests.post(api_url + '/process', data=json.dumps(process), headers=headers)
    result = json.loads(response.content)
    process_id = result['process_id']

    # Start this process
    process_action = {
        'process_id': process_id,
        'version': '1.0',
        'name': 'start',
    }

    print('Starting process...')
    response = requests.post(api_url + '/process_action', data=json.dumps(process_action))
    result = json.loads(response.content)

    print('Waiting for process to finish...')
    process_done = False
    last_progress = 0
    while not process_done:
        response = requests.get(api_url + '/process/' + process_id)
        result = json.loads(response.content)
        progress = result['status']['progress']

        if progress >= last_progress + 10:
            print(f'Progress: {progress}%')
            last_progress = progress
        
        process_done = result['status']['done']
        if process_done:
            break
        time.sleep(1)

    print('Retrieving results...')
    heat_maps = []
    for group_id in range(len(sources)):
        dataset_url = api_url + '/result/' + process_id + '/group/' + '%03d' % group_id
        dataset_url += '/dataset/heat_map/value'
        response = requests.get(dataset_url, headers=headers)

        dtype = 'f'
        shape = sources[0].shape
        heat_map = np.frombuffer(
            response.content, dtype=dtype, count=np.prod(shape)*step_count)
        heat_map = np.reshape(heat_map, (*shape, step_count))
        heat_maps.append(heat_map)


    print('Plotting results...')
    heat_map = heat_maps[0]
    heat_map = heat_maps[1]

    def animate(k):
        global heat_map
        heat_map_k = heat_map[:,:,k]

        # Clear the current plot figure
        plt.clf()

        plt.title(f'Temperature at step {k+1}')
        plt.xlabel('x')
        plt.ylabel('y')

        # This is to plot u_k (u at time-step k)
        plt.imshow(heat_map_k, cmap=plt.cm.jet, vmin=0, vmax=100)
        plt.colorbar()

        return plt

    anim = FuncAnimation(plt.figure(), animate, interval=1, frames=step_count, repeat=False)
    plt.show()
