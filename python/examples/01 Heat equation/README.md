Compute Web App example: Quantitative MRI
===

This subdirectory contains an example of how to use `cwa` for processing quantitative MRI.

Heat equation is the following:
$$
\frac{\delta u}{\delta t} - \alpha \nabla u = 0
$$

Where $\alpha$ is the thermal diffusivity, $u$ the temperature and $t$ the time variable.

Using FDM, next temperature at time $k$ and position $(i,j)$ can be computed as follows:

$$
u_{i,j}^{k+1} = u_{i,j}^{k} + \alpha \frac{\Delta t}{\Delta x^2} \left(
  u_{i+1,j}^k + u_{i-1,j}^k + u_{i,j+1}^k + u_{i,j-1}^k - 4 u_{i,j}^k
\right)
$$

To be numerically stable, following difference shall be respected:

$$
\Delta t \leq \frac{\Delta x^2}{4\alpha}
$$

Implementation of this mathematical formulation is deliberately naive to run on single CPU core to show benefit of CWA: not wasting time optimizing data process may be an option. Each instance may be optimized using a gradient linear operator involving a multiplication between a vector and a 5 diagonal matrix that could run on more CPU cores in parallel.

This subdirectory contains the necessary to be identified and run by CWA. Still, requirements has to be installed, so go to that directory and enter the following commands:
```
# python3 -m venv .venv
# source .venv/bin/activate
# pip install -r requirements.txt
```

The `remote` subdirectory contains scripts that shows how to post data, run process and retrieve result. Start a CWA instance, go to the subdiretory and do the usual:
```
# python3 -m venv .venv
# source .venv/bin/activate
# pip install -r requirements.txt
# python example.py
```
