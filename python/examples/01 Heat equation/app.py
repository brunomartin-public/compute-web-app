import argparse
import h5py
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation


if __name__ == '__main__':

    # parse arguments
    parser = argparse.ArgumentParser()

    # parse process specific argument as positional arguments
    parser.add_argument('thermal_diffusivity', help='Thermal diffusivity', default=2, type=float)
    parser.add_argument('step_count', help='process step count', default=100, type=int)

    # parse cwa specific argument as positional arguments at the end
    parser.add_argument('data_file', help='data file', default='', type=str)
    parser.add_argument('result_file', help='result file', default='', type=str)

    # Define a testing mode where all arguments are not given
    testing = False

    # Try to parse arguments, if error, define default ones
    try:
        args = parser.parse_args()

        thermal_diffusivity = args.thermal_diffusivity
        step_count = args.step_count
        data_file = args.data_file
        result_file = args.result_file

    except SystemExit as e:
        # Write following line to console, it will be catch by cwa and tell the error
        print("[ERROR {}]".format(str(e)))
        testing = True

        # Mock what cwa would give as arguments
        thermal_diffusivity = 2
        step_count = 750
        data_file = 'heatmap_example_data.h5'
        result_file = 'heatmap_example_result.h5'

        source = np.empty((50, 50))
        source[:] = np.nan
        source[0, :] = 100
        source[:, -1] = 0
        source[-1, :] = 0
        source[:, 0] = 0

        data = h5py.File(data_file, 'w')
        data_group = data.create_group('000')
        data_group.create_dataset('source', data=source)

        source[20:30, 20:30] = 50
        data_group = data.create_group('001')
        data_group.create_dataset('source', data=source)
        data.close()
        
    # Define here simuluation parameters and function
    alpha = thermal_diffusivity
    delta_x = 1

    delta_t = (delta_x ** 2)/(4 * alpha)
    gamma = (alpha * delta_t) / (delta_x ** 2)

    # Define function to compute next time step
    def calculate(gamma: float, source: np.array, heat: np.array) -> np.array:
        next_heat = np.copy(heat)
        for i in range(heat.shape[0]):
            for j in range(heat.shape[1]):
                if np.isnan(source[i, j]):
                    delta_heat = heat[i+1, j] + heat[i-1, j] + heat[i, j+1] + heat[i, j-1]
                    delta_heat -= 4*heat[i, j]
                    next_heat[i, j] += gamma * delta_heat
        return next_heat

    # Open input file if not empty
    data = h5py.File(data_file, 'r', libver='latest')

    # Prepare result file
    result = h5py.File(result_file, 'w')

    group_count = 0
    for group in data.values():
        if isinstance(group, h5py.Group):
            group_count += 1

    total_steps = 0
    for group_name in data:
        data_group = data[group_name]

        # Only groups are handled 
        if not isinstance(data_group, h5py.Group):
            continue

        source = data_group['source']

        # This simple algorithm supposes that boundaries are sources
        # Check it and raise an error if not
        if (any(np.isnan(source[0, :])) or 
            any(np.isnan(source[:, -1])) or 
            any(np.isnan(source[-1, :])) or 
            any(np.isnan(source[:, 0]))):
            e = Exception('Boundaries shall be sources')
            print("[ERROR {}]".format(str(e)))
            raise e

        result_group = result.create_group(group_name)
        heat_maps = result_group.create_dataset('heat_map', dtype='f', shape=(*source.shape, step_count))

        # Initial condition everywhere inside the grid
        init_heat_map = np.empty_like(source)
        init_heat_map[np.isnan(source)] = 0
        init_heat_map[np.isfinite(source)] = source[np.isfinite(source)]

        # Compute next temperature maps
        heat_map = np.copy(init_heat_map)
        heat_maps[:, :, 0] = heat_map
        heat_maps.flush()
        total_steps += 1

        last_progress = 0
        for k in range(1, step_count):
            heat_map = calculate(gamma, source, heat_map)
            heat_maps[:, :, k] = heat_map
            heat_maps.flush()

            total_steps += 1

            # print progress message that will be catch by the scheduler
            progress = int(total_steps/(group_count*step_count-1)*100)
            if progress >= last_progress + 10:
                print('[PROGRESS {}%]'.format(progress))
                last_progress = progress

        # Plot only if testing algorithm
        if testing:
            # Plot heat evolution over time
            heat_map = np.copy(init_heat_map)
            def animate(k):
                global heat_map
                heat_map = calculate(gamma, source, heat_map)

                # Clear the current plot figure
                plt.clf()

                plt.title(f'Temperature at t = {k*delta_t:.3f} unit time')
                plt.xlabel('x')
                plt.ylabel('y')

                # This is to plot u_k (u at time-step k)
                plt.imshow(heat_map, cmap=plt.cm.jet, vmin=0, vmax=100)
                plt.colorbar()

                return plt

            anim = FuncAnimation(plt.figure(), animate, interval=1, frames=step_count-1, repeat=False)
            plt.show()

    result.close()

    print('Done!')