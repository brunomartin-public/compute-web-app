Compute Web App examples
===

This directory contains examples of how to use `cwa`:
- `01 Heat equation` subdirectory show how to compute 2D heat equation from initial states and
- `02 Quantitative MRI` subdirectory show how to compute fat/water seperation from multi echo MRI data.
