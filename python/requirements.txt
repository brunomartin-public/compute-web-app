# website and REST api packages
connexion[flask, swagger-ui, uvicorn]==3.1.0
werkzeug==3.0.3
Flask==3.0.3
Flask-Cors==4.0.1

# write data packages
h5py==3.11.0

#pkgconfig==1.5.1
#Cython==0.29.19
#-e git://github.com/brunomartin/h5py.git#egg=h5py

psutil==6.0.0

# render images packages
Pillow==10.4.0
matplotlib==3.7.5

# test packages
pytest==8.3.2

# production packages
PyInstaller==6.10.0
gunicorn==23.0.0
waitress==3.0.0
