#ifndef CWA_PROCESS_PARAMETER_H
#define CWA_PROCESS_PARAMETER_H

#include <string>
#include <cstdint>
#include <vector>
#include <map>

#include "process/variant.h"

namespace cwa {
namespace process {

/**
 * @brief Parameter class to handle process arguments
 */
class Parameter {
public:
  /** Name of the parameter */
  const std::string name;

  /** Description of the parameter */
  const std::string description;

  /** Default value of the parameter */
  const Variant default_value;
  
  /** Assignement operator for the parameter */
  Parameter & operator=(const Parameter & rvalue);

private:
  
};

typedef std::map<std::string, Parameter> ParameterMap;

}
}

#endif // CWA_PROCESS_PARAMETER_H
