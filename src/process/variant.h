#ifndef CWA_PROCESS_VARIANT_H
#define CWA_PROCESS_VARIANT_H

#include <string>
#include <cstdint>
#include <memory>
#include <vector>
#include <map>
#include <iostream>

namespace cwa {
namespace process {

class Variant;
typedef std::vector<Variant> Variants;
typedef std::map<std::string, Variant> VariantMap;

/**
 * @brief Variant class to handle multiple type of parameters
 */
class Variant {
public:
    
  /** @enum Type
   *  @brief Variant possible types
   */
  enum class Type {
    None, /**< Not a type */
    Integer, /**< Integer type */
    Decimal, /**< Decimal type */
    String, /**< String type */
    Variants  /**< Variant list type */
  };
  
  /** Construct a new Variant object */
  Variant();

  /**
   * @brief Copy constructor
   * @param value from wich copy variant
   */
  Variant(const Variant & value);

  /**
   * @brief Assignement operator
   * @param value from wich assign variant
   */
  Variant & operator=(const Variant & value);
  
  /**
   * @brief Construct a new Variant from integer
   * @param value variant value as integer
   */
  Variant(int value);

  /**
   * @brief Construct a new Variant from double
   * @param value variant value as double
   */
  Variant(double value);

  /**
   * @brief Construct a new Variant from std::string
   * @param value variant value as std::string
   */
  Variant(const std::string & value);

  /**
   * @brief Construct a new Variant from char string
   * @param value variant value as char string
   */
  Variant(const char* value);

  /**
   * @brief Construct a new Variant from variant list
   * @param values variant value as variant list
   */
  Variant(const Variants & values);
    
  /**
   * @brief Get variant Type
   * @return variant Type 
   */
  Type GetType() const;
  
  /**
   * @brief Return integer value
   * @return integer value 
   */
  int ToInteger() const;

  /**
   * @brief Return decimal value
   * @return decimal value as a double
   */
  double ToDecimal() const;

  /**
   * @brief Return string value
   * @return decimal value as a std::string
   */
  const std::string & ToString() const;

  /**
   * @brief Return variant list value
   * @return decimal value as a variant list
   */
  const Variants & ToVariants() const;
  
  /**
   * @brief equality comporator
   * @param value variant to compare
   * @return true if equal else false
   */
  bool operator==(const Variant & value) const;
  
  /**
   * @brief Serialize values to std::string
   * @param values variant list to serialize
   * @param prefix prefix to add
   * @return serialized value as a std::string 
   */
  static std::string SerializeValues(const VariantMap & values, const std::string & prefix = "");
  
private:
  /** Private class for private implementation */
  class Private;

  /** Private class object */
  std::shared_ptr<Private> data_;
};

std::ostream& operator<<(std::ostream& os, const Variant& value);
bool operator==(const Variants & lvalue, const Variants & rvalue);
bool operator!=(const Variants & lvalue, const Variants & rvalue);

}
}

#endif // CWA_PROCESS_VARIANT_H
