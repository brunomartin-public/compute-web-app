#ifndef CWA_PROCESS_DEFINITION_H
#define CWA_PROCESS_DEFINITION_H

#include <string>
#include <vector>

#include "process/parameter.h"

namespace cwa {
namespace process {

/**
 * @brief Class for process definition
 */
class Definition {
public:
  /**
   * @brief Construct a new Definition object
   * 
   * @param command command to run
   * @param args arguments for the command to run
   */
  Definition(const std::string & command, const std::vector<std::string> args = {});
  
  /**
   * @brief Add a parameter to the definition
   * 
   * @param parameter parameter to add
   */
  void AddParameter(const Parameter & parameter);
  
  /**
   * @brief Get default parameter values
   * 
   * @return Default parameters as a variant map 
   */
  VariantMap GetDefaultParameterValues() const;
  
  /**
   * @brief Get the command to run
   * @return command to run as a string 
   */
  const std::string & GetCommand() const;

  /**
   * @brief Get command arguments as a vector
   * @return command arguments as a vector 
   */
  const std::vector<std::string> & GetArgs() const;
  
private:
  /** Command to run as a string */
  std::string command_;

  /** Command arguments as a string vector */
  std::vector<std::string> args_;
  
  /** parameter map */
  ParameterMap parameters_;
  
};

}

}

#endif // CWA_PROCESS_DEFINITION_H
