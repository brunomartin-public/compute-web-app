#ifndef CWA_PROCESS_WORKER_H
#define CWA_PROCESS_WORKER_H

#include <memory>
#include <string>

#include <data/dataset_supplier_server.h>
#include <data/dataset_aggregator_server.h>

#include "process/definition.h"

using cwa::data::DatasetSupplierServerPtr;
using cwa::data::DatasetAggregatorServerPtr;

namespace cwa {
namespace process {

/**
 * @brief Base class for worker processes
 * 
 */
class Worker {
public:
  /**
   * @brief Construct a new Worker object
   * @param definition definition of the process
   * @param parameter_values map of parameters for the process
   * @param supplier pointer to dataset supplier server
   * @param aggregator pointer to dataset aggregator server
   */
  Worker(const Definition & definition, const VariantMap & parameter_values,
    DatasetSupplierServerPtr supplier, DatasetAggregatorServerPtr aggregator);

  /** Destroy the Worker object */
  virtual ~Worker() {};
  
  /** Method called to launch the process, to be implemented */
  virtual void Launch() = 0;
  
protected:
  /**
   * @brief Get worker definition
   * @return definition
   */
  const Definition & GetDefinition();

  /**
   * @brief Get worker parameter values
   * @return map of parameter values
   */
  const VariantMap & GetParameterValues();

  /**
   * @brief Get supplier server
   * @return pointer to supplier server
   */
  DatasetSupplierServerPtr GetSupplier();

  /**
   * @brief Get aggregator server
   * @return pointer to aggregator server
   */
  DatasetAggregatorServerPtr GetAggregator();
  
private:
  /** Private class for private implementation */
  class Private;

  /** Private class object */
  std::shared_ptr<Private> data_;
};

typedef std::shared_ptr<Worker> WorkerPtr;


}
}

#endif // CWA_PROCESS_WORKER_H
