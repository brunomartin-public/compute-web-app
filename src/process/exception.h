#ifndef CWA_PROCESS_EXCEPTION_H
#define CWA_PROCESS_EXCEPTION_H

#include <exception>

namespace cwa {
namespace process {

/**
 * @brief Base exception for cwa process exceptions
 */
class Exception : public std::exception {
public:
private:
};

/**
 * @brief Exception thrown when a bad conversion occurs
 */
class BadConversionException : public Exception {
public:
private:
};

}
}

#endif // CWA_DATA_EXCEPTION_H
