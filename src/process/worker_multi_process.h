#ifndef CWA_PROCESS_WORKER_MULTI_PROCESS_H
#define CWA_PROCESS_WORKER_MULTI_PROCESS_H

#include <memory>
#include <string>
#include <functional>

#include <data/dataset.h>

#include "process/worker.h"

using cwa::data::DatasetPtr;
using cwa::data::ConstDatasetPtr;

namespace cwa {
namespace process {

/**
 * @brief Multiprocess implementation of Worker base class
 * 
 */
class WorkerMultiProcess : public Worker {
public:

  /**
   * @brief Construct a new Worker object
   * @param definition definition of the process
   * @param parameter_values map of parameters for the process
   * @param supplier pointer to dataset supplier server
   * @param aggregator pointer to dataset aggregator server
   */
  WorkerMultiProcess(const Definition & definition, const VariantMap & parameter_values,
                    DatasetSupplierServerPtr supplier, DatasetAggregatorServerPtr aggregator);
  
  /** Destroy the WorkerMultiProcess object */
  ~WorkerMultiProcess();
  
  virtual void Launch() override;
  
private:
  /** Private class for private implementation */
  class Private;

  /** Private class object */
  std::shared_ptr<Private> data_;
};

}

}

#endif // CWA_PROCESS_WORKER_MULTI_THREAD_H
