#ifndef CWA_PROCESS_WORKER_MULTI_THREAD_H
#define CWA_PROCESS_WORKER_MULTI_THREAD_H

#include <memory>
#include <string>
#include <functional>

#include <data/dataset.h>

#include "process/worker.h"

using cwa::data::DatasetPtr;
using cwa::data::ConstDatasetPtr;

namespace cwa {
namespace process {

/**
 * @brief Multithread implementation of Worker base class
 * 
 */
class WorkerMultiThread : public Worker {
public:
  
  /** Defines function to run for the worker in multithread context */
  typedef std::function<void(const std::string&, const std::string&, const std::string&, const std::string&)> RunFunction;
  
  /**
   * @brief Construct a new Worker object
   * @param definition definition of the process
   * @param parameter_values map of parameters for the process
   * @param supplier pointer to dataset supplier server
   * @param aggregator pointer to dataset aggregator server
   * @param run_function function to run in worker
   */
  WorkerMultiThread(const Definition & definition, const VariantMap & parameter_values,
                    DatasetSupplierServerPtr supplier, DatasetAggregatorServerPtr aggregator,
                    RunFunction run_function);
  
  /** Destroy the WorkerMultiThread object */
  ~WorkerMultiThread();
  
  virtual void Launch() override;
  
private:
  /** Private class for private implementation */
  class Private;
  
  /** Private class object */
  std::shared_ptr<Private> data_;
};

}

}

#endif // CWA_PROCESS_WORKER_MULTI_THREAD_H
