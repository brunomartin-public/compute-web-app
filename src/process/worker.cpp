#include "process/worker.h"

namespace cwa {
namespace process {

/**
 * @brief Private class for private implementation
 */
class Worker::Private {
public:
  /** Worker definition */
  Definition definition = Definition("");

  /** Parameter values */
  VariantMap parameter_values;

  /** Dataset aggregator server for that worker */
  DatasetAggregatorServerPtr aggregator;

  /** Dataset supplier server for that worker */
  DatasetSupplierServerPtr supplier;
};

Worker::Worker(const Definition & definition, const VariantMap & parameter_values,
  DatasetSupplierServerPtr supplier, DatasetAggregatorServerPtr aggregator) :
  data_(new Private) {
    data_->definition = definition;
    data_->parameter_values = parameter_values;
    data_->aggregator = aggregator;
    data_->supplier = supplier;
}

const Definition & Worker::GetDefinition() {
  return data_->definition;
}

const VariantMap & Worker::GetParameterValues() {
  return data_->parameter_values;
}

DatasetSupplierServerPtr Worker::GetSupplier() {
  return data_->supplier;
}

DatasetAggregatorServerPtr Worker::GetAggregator() {
  return data_->aggregator;  
}


}
}
