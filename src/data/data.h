#ifndef CWA_DATA_DATA_H
#define CWA_DATA_DATA_H

#include <memory>
#include <vector>

#include "data/dataset.h"

namespace cwa {
namespace data {

/**
 * @brief Data class that represent a data composed of datasets
 */
class Data {
public:
  /**
   * @brief Construct a new Data object
   * @param id data id as a string
   */
  Data(const std::string & id);
  
  /**
   * @brief Get id of the data
   * @return data id as a string
   */
  const std::string & GetId() const;
  
  /**
   * @brief Add dataset to data
   * @param dataset pointer to a dataset to add
   */
  void AddDataset(DatasetPtr dataset);
  
  /**
   * @brief Get pointers to datasets of this data object
   * @return pointers to datasets 
   */
  const ConstDatasets & GetDatasets() const;

private:
  /** Private class for private implementation */
  class Private;

  /** Private class object */
  std::shared_ptr<Private> data_;
};

typedef std::shared_ptr<Data> DataPtr;
typedef std::shared_ptr<const Data> ConstDataPtr;

}
}

#endif // CWA_DATA_DATA_H
