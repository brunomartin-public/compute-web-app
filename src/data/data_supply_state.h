#ifndef CWA_DATA_PROVIDE_STATE_H
#define CWA_DATA_PROVIDE_STATE_H

#include <vector>

# include <log/log.h>

#include "data/data.h"
#include "data/dataset.h"

namespace cwa {
namespace data {

/**
 * @brief Data supply state that give handle data sent information
 */
class DataSupplyState {
public:
  /**
   * @brief Construct a new Data Supply State object
   * @param data pointer to data to supply
   */
  DataSupplyState(ConstDataPtr data);
  
  /**
   * @brief Grab a dataset from the data to supply it
   * @param dataset grabbed dataset to supply
   * @param index grabbed dataset index to supply
   */
  void GrabDataset(ConstDatasetPtr & dataset, uint32_t & index);

private:
  /** Private class for private implementation */
  class Private;

  /** Private class object */
  std::shared_ptr<Private> data_;
};

typedef std::shared_ptr<DataSupplyState> DataSupplyStatePtr;

}
}

#endif // CWA_DATA_PROVIDE_STATE_H
