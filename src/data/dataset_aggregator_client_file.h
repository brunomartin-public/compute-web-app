#ifndef CWA_DATA_DATASET_AGGREGATOR_CLIENT_FILE_H
#define CWA_DATA_DATASET_AGGREGATOR_CLIENT_FILE_H

#include "data/dataset_aggregator_client.h"

namespace cwa {
namespace data {

/**
 * @brief Implementation class for dataset aggregator clients using file
 */
class DatasetAggregatorClientFile : public DatasetAggregatorClient {
public:
  /** Construct a new Dataset Aggregator Client File object */
  DatasetAggregatorClientFile();

  /** Destroy the Dataset Aggregator Client File object */
  ~DatasetAggregatorClientFile();
  
  virtual void Connect(const std::string & info) override;
  virtual void Close() override;
  
  virtual void PullDataset(DatasetPtr& dataset, uint32_t & index) override;
  virtual void HandleEndSignal() override;

private:
  /** Private class for private implementation */
  class Private;

  /** Private class object */
  std::shared_ptr<Private> data_;
};

}
}

#endif // CWA_DATA_DATASET_AGGREGATOR_CLIENT_FILE_H
