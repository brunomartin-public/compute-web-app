#ifndef CWA_DATA_DATA_SUPPLIER_H
#define CWA_DATA_DATA_SUPPLIER_H

#include "data/dataset_supplier_server.h"
#include "data/data_supply_state.h"

namespace cwa {
namespace data {

/**
 * @brief Data supplier class to supply data composed of datasets
 */
class DataSupplier {
public:

  /**
   * @brief Construct a new Data supplier object
   * @param dataset_supplier pointer to server dataset supplier
   */
  DataSupplier(DatasetSupplierServerPtr dataset_supplier);
    
  /**
   * @brief Get the Id object of this data supplier
   * @return id as an integer
   */
  int GetId() const;
  
  /**
   * @brief Start pushing dataset from data
   * @param supply_state data supply state
   */
  void Start(DataSupplyStatePtr supply_state);
  
  /** Wait until all dataset have been pushed */
  void Wait();
  
private:
  /** Private class for private implementation */
  class Private;

  /** Private class object */
  std::shared_ptr<Private> data_;
};

}
}

#endif // CWA_DATA_SUPPLIER_H
