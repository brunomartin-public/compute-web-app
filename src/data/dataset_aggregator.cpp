#include "data/dataset_aggregator.h"

namespace cwa {
namespace data {

/**
 * @brief Private class for private implementation
 */
class DatasetAggregator::Private {
public:

  /** Unique id of this dataset aggregator */
  int id;

  /** Id to be given to next instanciated dataset aggregator */
  static int next_id;

  /** Total number of buffers aggregated by this dataset aggregator */
  int total_buffers = 0;

  /** Timeout in ms */
  int timeout_ms = 5000;
};

int DatasetAggregator::Private::next_id = 1;

DatasetAggregator::DatasetAggregator() : data_(new Private) {
  data_->id = data_->next_id;
  data_->next_id++;
}

int DatasetAggregator::GetId() const {
  return data_->id;
}

void DatasetAggregator::SetTimeout(int timeout_ms) {
  data_->timeout_ms = timeout_ms;
}

int DatasetAggregator::GetTimeout() const {
  return data_->timeout_ms;
}

}
}
