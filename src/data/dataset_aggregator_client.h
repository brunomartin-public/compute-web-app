#ifndef CWA_DATA_DATASET_AGGREGATOR_CLIENT_H
#define CWA_DATA_DATASET_AGGREGATOR_CLIENT_H

#include "data/dataset_aggregator.h"

namespace cwa {
namespace data {

/**
 * @brief Base class for dataset aggregator clients
 */
class DatasetAggregatorClient : public DatasetAggregator {
public:

  /**
   * @brief Method to connect to dataset supplier, to be implemented
   * @param info string containing information for connection
   */
  virtual void Connect(const std::string & info) = 0;
  
private:
};

typedef std::shared_ptr<DatasetAggregatorClient> DatasetAggregatorClientPtr;


}
}

#endif // CWA_DATA_DATASET_AGGREGATOR_CLIENT_H
