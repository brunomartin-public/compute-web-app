#ifndef CWA_DATA_DATASET_H
#define CWA_DATA_DATASET_H

#include <memory>
#include <string>
#include <vector>

namespace cwa {
namespace data {

class Data;

/**
 * @brief Dataset class to store parts of data
 */
class Dataset {
public:
  /** Construct a new Dataset object */
  Dataset();
  
  /**
   * @brief Set id of the dataset as a string
   *  For a file, it will be its name, for a database record
   *  it will be its unique id.
   * @param id id of the datset
   */
  void SetId(const std::string & id);
  
  /**
   * @brief Set data whose this dataset is part, for a file it will be a directory
   *  For a database, it will be a table.
   * 
   * @param data Data pointer as a shared pointer
   */
  void SetData(const Data* data);
  
  /**
   * @brief Get id of the dataset
   * @return id as a string 
   */
  const std::string & GetId() const;

  /**
   * @brief Get dataset size in bytes
   * @return size of the dataset in bytes as integer
   */
  size_t GetSize() const;
  
  /**
   * @brief Get dataset filename
   * @return dataset filename as a string 
   */
  std::string GetFilename() const;
  
  /**
   * @brief Retrieve and store buffer from a file
   * @param filename filename from which to reitrieve buffer
   */
  void RetrieveBufferFromFile(const std::string & filename) const;
  
  /**
   * @brief Retrieve stored buffer and copied it to buffer parameter
   * @param buffer in which stored buffer is sopied
   */
  void RetrieveBuffer(std::string & buffer) const;
  
  /**
   * @brief Copy buffer in stored buffer in object
   * @param buffer to copy
   */
  void RetainBuffer(std::string & buffer);

  /**
   * @brief Get stored buffer
   * @return stored buffer as reference 
   */
  std::string & GetBuffer() const;

  /** Release stored buffer */
  void ReleaseBuffer();
  
  /** Persist stored buffer to file */
  void Persist();

private:
  /** Private class for private implementation */
  class Private;

  /** Private class object */
  std::shared_ptr<Private> data_;
};

typedef std::shared_ptr<Dataset> DatasetPtr;
typedef std::shared_ptr<const Dataset> ConstDatasetPtr;
typedef std::vector<DatasetPtr> Datasets;
typedef std::vector<ConstDatasetPtr> ConstDatasets;

}
}

#endif // CWA_DATA_DATASET_H
