#ifndef CWA_DATA_DATASET_SUPPLIER_SERVER_FILE_H
#define CWA_DATA_DATASET_SUPPLIER_SERVER_FILE_H

#include "data/dataset_supplier_server.h"

namespace cwa {
namespace data {

/**
 * @brief Implementation class for dataset supplier servers using file
 */
class DatasetSupplierServerFile : public DatasetSupplierServer {
public:

  /** Construct a new Dataset Supplier Server File object */
  DatasetSupplierServerFile();

  /** Destroy the Dataset Supplier Server File object */
  ~DatasetSupplierServerFile();
  
  virtual void Bind(const std::string & info) override;
  virtual void Close() override;
  
  virtual std::string GetInfo() const override;
  virtual void PushDataset(ConstDatasetPtr dataset, uint32_t index) override;
  virtual void PushEndSignal() override;

private:
  /** Private class for private implementation */
  class Private;

  /** Private class object */
  std::shared_ptr<Private> data_;
};

}
}

#endif // CWA_DATA_DATASET_SUPPLIER_SERVER_FILE_H
