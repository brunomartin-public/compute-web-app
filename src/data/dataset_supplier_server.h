#ifndef CWA_DATA_DATASET_SUPPLIER_SERVER_H
#define CWA_DATA_DATASET_SUPPLIER_SERVER_H

#include "data/dataset_supplier.h"

namespace cwa {
namespace data {

/**
 * @brief Base class for dataset supplier servers
 */
class DatasetSupplierServer : public DatasetSupplier {
public:

  /**
   * @brief Method to bind for aggregator clients to connect, to be implemented
   * @param info string containing information for connection
   */
  virtual void Bind(const std::string & info) = 0;

  /**
   * @brief Method to get connection information, to be implemented
   * @return connection information as a string 
   */
  virtual std::string GetInfo() const = 0;
  
private:
};

typedef std::shared_ptr<DatasetSupplierServer> DatasetSupplierServerPtr;

}
}

#endif // CWA_DATA_DATASET_SUPPLIER_SERVER_H
