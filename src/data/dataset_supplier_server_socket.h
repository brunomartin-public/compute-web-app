#ifndef CWA_DATA_DATASET_SUPPLIER_SERVER_SOCKET_H
#define CWA_DATA_DATASET_SUPPLIER_SERVER_SOCKET_H

#include "data/dataset_supplier_server.h"

namespace cwa {
namespace data {

/**
 * @brief Implementation class for dataset supplier servers using socket
 */
class DatasetSupplierServerSocket : public DatasetSupplierServer {
public:

  /** Construct a new Dataset Supplier Server Socket object */
  DatasetSupplierServerSocket();

  /** Destroy the Dataset Supplier Server Socket object */
  ~DatasetSupplierServerSocket();
  
  virtual void Bind(const std::string & info) override;
  virtual void Close() override;
  
  virtual std::string GetInfo() const override;
  virtual void PushDataset(ConstDatasetPtr dataset, uint32_t index) override;
  virtual void PushEndSignal() override;

private:
  /** Private class for private implementation */
  class Private;

  /** Private class object */
  std::shared_ptr<Private> data_;
};

}
}

#endif // CWA_DATA_DATASET_SUPPLIER_SERVER_SOCKET_H
