#ifndef CWA_DATA_UTILS_H
#define CWA_DATA_UTILS_H

#include <string>
#include <sstream>
#include <vector>
#include <iterator>
#include <chrono>

using std::chrono::high_resolution_clock;
using std::chrono::microseconds;
using std::chrono::duration_cast;

/** Template class for Split method */
template<char delimiter>
class WordDelimitedBy : public std::string {};

/** input stream operator specification for WordDelimitedBy input */
template<char delimiter>
std::istream& operator>>(std::istream& is, WordDelimitedBy<delimiter>& output) {
   std::getline(is, output, delimiter);
   return is;
}
  
/**
 * @brief Split a string with a delimiter
 * 
 * @tparam delimiter delimiter as a string
 * @param string string to split
 * @return vector of string from the split operation
 */
template<char delimiter>
std::vector<std::string> Split(const std::string & string) {
  std::istringstream iss(string);
  std::istream_iterator<WordDelimitedBy<delimiter>> begin(iss);
  std::istream_iterator<WordDelimitedBy<delimiter>> end;
  std::vector<std::string> results(begin, end);
  return results;
}

/**
 * @brief Duration class to easily measure elapsed time between to lines in
 *  the code
 */
class Duration {
public:
  /** Construct a new Duration object */
  Duration() {
    start_ = high_resolution_clock::now();
  }
  
  /** Store the start time point */
  void Start() {
    start_ = high_resolution_clock::now();    
  }
  
  /**
   * @brief Get elapsed time between now and the stored start time
   * @return elapsed time in micro seconds 
   */
  int GetElapsedUs() {
    auto duration = high_resolution_clock::now() - start_;
    return duration_cast<microseconds>(duration).count();
  }
  
private:
  /** Stored start time point */
  high_resolution_clock::time_point start_;
};

#endif // CWA_DATA_UTILS_H
