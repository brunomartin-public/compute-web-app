#ifndef CWA_DATA_DATASET_AGGREGATOR_CLIENT_SOCKET_H
#define CWA_DATA_DATASET_AGGREGATOR_CLIENT_SOCKET_H

#include "data/dataset_aggregator_client.h"

namespace cwa {
namespace data {

/**
 * @brief Implementation class for dataset aggregator clients using socket
 */
class DatasetAggregatorClientSocket : public DatasetAggregatorClient {
public:
  /** Construct a new Dataset Aggregator Client Socket object */
  DatasetAggregatorClientSocket();

  /** Destroy the Dataset Aggregator Client Socket object */
  ~DatasetAggregatorClientSocket();
  
  virtual void Connect(const std::string & info) override;
  virtual void Close() override;
  
  virtual void PullDataset(DatasetPtr& dataset, uint32_t & index) override;
  virtual void HandleEndSignal() override;

private:
  /** Private class for private implementation */
  class Private;

  /** Private class object */
  std::shared_ptr<Private> data_;
};

}
}

#endif // CWA_DATA_DATASET_AGGREGATOR_CLIENT_SOCKET_H
