#include "data/data.h"

namespace cwa {
namespace data {

/**
 * @brief Private class for private implementation
 */
class Data::Private {
public:
  /** data id */
  std::string id;

  /** datasets of this data */
  ConstDatasets datasets;
};

Data::Data(const std::string & id) : data_(new Private) {
  data_->id = id;
}

const std::string & Data::GetId() const {
  return data_->id;
}

void Data::AddDataset(DatasetPtr dataset) {
  dataset->SetData(this);
  data_->datasets.push_back(dataset);
}

const ConstDatasets & Data::GetDatasets() const {
  return data_->datasets;
}

}
}
