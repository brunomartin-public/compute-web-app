#ifndef CWA_DATA_DATASET_SOCKET_SERVER_H
#define CWA_DATA_DATASET_SOCKET_SERVER_H

#include <thread>
#include <mutex>
#include <condition_variable>
#include <map>
#include <asio.hpp>

using asio::ip::tcp;

namespace cwa {
namespace data {

/** Define a socket associative map */
typedef std::map<uint32_t, std::shared_ptr<tcp::socket>> SocketMap;

/**
 * @brief Dataset socket server for TCP data exchange
 */
class DatasetSocketServer {
public:

  /** Construct a new Dataset Socket Server object */
  DatasetSocketServer();

  /** Destroy the Dataset Socket Server object */  
  ~DatasetSocketServer();
  
  /** Define method signature for registration callback */
  typedef std::function<void(std::shared_ptr<tcp::socket>,uint32_t)> Callback;

  /**
   * @brief Set a registration callback
   * @param on_client_registered callback that will be called when a client
   *  registers
   */
  void SetRegistrationCallback(Callback on_client_registered);
  
  /** Init socket server, open sockets */
  void Init();

  /** Close socket server, close sockets */
  void Close();

  /**
   * @brief Start registration process to specific host and port
   * @param ip_adress ip address and port with ":" sperator
   */
  void StartRegistration(const std::string & ip_adress);

  /** Stop registration process */
  void StopRegistration();
  
  /**
   * @brief Get information on server
   * @return information as a string
   */
  std::string GetInfo() const;

  /**
   * @brief Get client socket map
   * @return client socket map
   */
  SocketMap & GetClientSockets();
  
private:
  /** Context for asio */
  asio::io_context io_context_;
  
  /** Service for asio */
  asio::io_service io_service_;

  /** Service work for asio */
  asio::io_service::work work_ = asio::io_service::work(io_service_);

  /** Service thread for asio */
  std::thread io_service_thread_;
  
  /** Registration thread for asio */
  std::thread register_thread_;

  /** Tell to stop registration process */
  bool stop_registering_ = false;

  /** Mutex for concurrent registration operations */
  std::mutex register_socket_mutex_;

  /** Wait condition for concurrent registration operations */
  std::condition_variable register_socket_cv_;

  /** Registration endpoint */
  std::string register_end_point_;
  
  /** TCP server socket */
  tcp::socket server_socket_ = tcp::socket(io_context_);

  /** Next client id */
  static uint32_t next_client_id;

  /** Associsative map to store client sockets */
  std::map<uint32_t, std::shared_ptr<tcp::socket>> client_sockets_;
  
  /**
   * @brief Run registration
   * @param port port number to listen to for registration
   */
  void RunRegistration_(uint16_t port);
  
  /** Method call on accepting client connection */
  void DoAccept_();

  /** asio object for accepting client connections */
  std::unique_ptr<tcp::acceptor> acceptor_;
  
  /** Registered on client connection callback */
  Callback on_client_registered_ = nullptr;
};


}
}

#endif // CWA_DATA_DATASET_SOCKET_SERVER_H
