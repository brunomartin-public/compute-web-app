#ifndef CWA_DATA_DATASET_SUPPLIER_CLIENT_H
#define CWA_DATA_DATASET_SUPPLIER_CLIENT_H

#include "data/dataset_supplier.h"

namespace cwa {
namespace data {

/**
 * @brief Base class for dataset supplier clients
 */
class DatasetSupplierClient : public DatasetSupplier {
public:

  /**
   * @brief Method to connect to dataset aggregtor, to be implemented
   * @param info string containing information for connection
   */
  virtual void Connect(const std::string & info) = 0;
  
private:
};

typedef std::shared_ptr<DatasetSupplierClient> DatasetSupplierClientPtr;

}
}

#endif // CWA_DATA_DATASET_SUPPLIER_CLIENT_H
