#ifndef CWA_DATA_DATASET_SUPPLIER_CLIENT_SOCKET_H
#define CWA_DATA_DATASET_SUPPLIER_CLIENT_SOCKET_H

#include "data/dataset_supplier_client.h"

namespace cwa {
namespace data {

/**
 * @brief Implementation class for dataset supplier client using socket
 */
class DatasetSupplierClientSocket : public DatasetSupplierClient {
public:

  /** Construct a new Dataset Supplier Client Socket object */
  DatasetSupplierClientSocket();

  /** Destroy the Dataset Supplier Client Socket object */
  ~DatasetSupplierClientSocket();
  
  virtual void Connect(const std::string & info) override;
  virtual void Close() override;
  
  virtual void PushDataset(ConstDatasetPtr dataset, uint32_t index) override;
  virtual void PushEndSignal() override;

private:
  /** Private class for private implementation */
  class Private;

  /** Private class object */
  std::shared_ptr<Private> data_;
};

}
}

#endif // CWA_DATA_DATASET_SUPPLIER_CLIENT_SOCKET_H
