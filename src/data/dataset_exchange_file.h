#ifndef CWA_DATA_DATASET_EXCHANGE_FILE_H
#define CWA_DATA_DATASET_EXCHANGE_FILE_H

#include <string>
#include <fstream>

namespace cwa {
namespace data {

/**
 * @brief Dataset exchange file that handle dataset pull and push atomic
 *  operations
 */
class DatasetExchangeFile {
public:

  /**
   * @brief Construct a new Dataset Exchange File object
   * @param filename of the exhange file
   */
  DatasetExchangeFile(const std::string & filename);

  /** Destroy the Dataset Exchange File object */
  ~DatasetExchangeFile();
  
  /** Dataset structure for exhanging data */
  struct Dataset {
    /** id of the dataset */
    uint32_t id = -1;

    /** filename that contains dataset */
    std::string filename = "";

    /** client id for that dataset */
    uint32_t client_id = -1;
  };
  
  /**
   * @brief Get filename of the exhange file
   * @return filename as a string
   */
  const std::string & GetFilename() const;
  
  /**
   * @brief Wait for file to be available and filled
   * @param timeout_ms time to wait before issuing a timeout
   */
  void WaitForFile(int timeout_ms = 5000) const;
  
  /** Lock the file for read/write operation */
  void LockFile();

  /** Unlock the file */
  void UnlockFile();
  
  /**
   * @brief Initialize a file telling if it is for an aggregator or a supplier
   * @param is_aggregator tell if it is an aggregator, false if it is a supplier
   */
  void Init(bool is_aggregator);

  /** Register the client */
  uint32_t RegisterClient();
  
  /**
   * @brief Push a dataset in the exchange file
   * @param dataset dataset to push
   */
  void PushDataset(const Dataset & dataset);
  
  /**
   * @brief Tell if there at least one dataset
   * @return true if there is a dataset else false
   */
  bool HasDataset() const;

  /**
   * @brief Get the first available dataset
   * @return first available dataset 
   */
  Dataset GetFirstDataset();

  /** Pop the first available dataset */
  void PopFirstDataset();
  
  /**
   * @brief Unregister a client by its id
   * @param client_id id of the client to unregister
   */
  void UnregisterClient(uint32_t client_id);
  
  /**
   * @brief Tell if all client are unregistered
   * @return true if all client are unregistered else false
   */
  bool AreAllClientsUnregistered() const;
  
  /** Removes files */
  void RemoveFiles();
  
private:

  /** Filename as a string (.*)*/
  std::string filename_;
};

}
}

#endif // CWA_DATA_DATASET_EXCHANGE_FILE_H
