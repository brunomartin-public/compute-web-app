#ifndef CWA_DATA_DATASET_AGGREGATOR_H
#define CWA_DATA_DATASET_AGGREGATOR_H

#include "data/dataset.h"

namespace cwa {
namespace data {

/**
 * @brief Dataset aggregator class to aggregate dataset
 * 
 */
class DatasetAggregator {
public:

  /** Construct a new Dataset aggregator object */
  DatasetAggregator();

  /** Destroy the Dataset aggregator object */
  virtual ~DatasetAggregator() {};
  
  /**
   * @brief Get id of the dataset aggregator
   * @return id as an integer 
   */
  int GetId() const;
  
  /**
   * @brief Set timeout in ms for aggregation operations
   * @param timeout_ms timeout in ms
   */
  void SetTimeout(int timeout_ms);

  /**
   * @brief Get timeout in ms
   * @return timeout in ms 
   */
  int GetTimeout() const;
  
  /** Method to close aggregator communication, to be implemented */
  virtual void Close() = 0;
  
  /**
   * @brief Method that defines how to pull dataset, to be implemented
   * @param dataset dataset that has been pulled
   * @param index index of the pulled dataset
   */
  virtual void PullDataset(DatasetPtr& dataset, uint32_t & index) = 0;

  /** Handle end signal received from supplier, to be implemented */
  virtual void HandleEndSignal() = 0;
  
private:
  /** Private class for private implementation */
  class Private;

  /** Private class object */
  std::shared_ptr<Private> data_;
};


}
}

#endif // CWA_DATA_DATASET_AGGREGATOR_H
