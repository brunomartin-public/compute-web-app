#ifndef CWA_DATA_DATASET_SUPPLIER_H
#define CWA_DATA_DATASET_SUPPLIER_H

#include "data/dataset.h"

namespace cwa {
namespace data {

/**
 * @brief Dataset supplier class to aggregate dataset
 * 
 */
class DatasetSupplier {
public:

  /** Construct a new Dataset supplier object */
  DatasetSupplier();

  /** Destroy the Dataset supplier object */
  virtual ~DatasetSupplier() {};
  
  /**
   * @brief Get id of the dataset supplier
   * @return id as an integer 
   */
  int GetId() const;
  
  /**
   * @brief Set timeout in ms for supply operations
   * @param timeout_ms timeout in ms
   */
  void SetTimeout(int timeout_ms);

  /**
   * @brief Get timeout in ms
   * @return timeout in ms 
   */
  int GetTimeout() const;
  
  /** Method to close supplier communication, to be implemented */
  virtual void Close() = 0;
  
  /**
   * @brief Method that defines how to push dataset, to be implemented
   * @param dataset dataset to push
   * @param index index of the pushed dataset
   */
  virtual void PushDataset(ConstDatasetPtr dataset, uint32_t index) = 0;

  /** Push end signal to aggregator, to be implemented */
  virtual void PushEndSignal() = 0;
  
private:
  /** Private class for private implementation */
  class Private;

  /** Private class object */
  std::shared_ptr<Private> data_;
};


}
}

#endif // CWA_DATA_DATASET_SUPPLIER_H
