#ifndef CWA_DATA_EXCEPTION_H
#define CWA_DATA_EXCEPTION_H

#include <exception>

namespace cwa {
namespace data {

/**
 * @brief Base exception for cwa data exceptions
 */
class Exception : public std::exception {
public:
  /** Construct a new Exception object */
  Exception() {}

  /**
   * @brief Construct a new Exception object with message
   * @param message exception message
   */
  Exception(const char * message) : message_(message) {}
  
private:
  /** exception message */
  std::string message_;
};

/**
 * @brief Exception thrown when a dataset is detected as orhpan with no data
 */
class OrphanDatasetException : public Exception {
public:
private:
};

/**
 * @brief Exception thrown when there is no more dataset
 */
class NoMoreDatasetException : public Exception {
public:
private:
};

/**
 * @brief Exception thrown when there is no more supplier
 */
class NoMoreSupplierException : public Exception {
public:
private:
};

/**
 * @brief Exception thrown when cannot connect to target server
 */
class CannotConnectToServerException : public Exception {
public:
private:
};

/**
 * @brief Exception thrown when a timeout has occured
 */
class TimeoutException : public Exception {
public:
private:
};

}
}

#endif // CWA_DATA_EXCEPTION_H
