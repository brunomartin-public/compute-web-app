#ifndef CWA_DATA_DATA_AGGREGATOR_H
#define CWA_DATA_DATA_AGGREGATOR_H

#include <functional>

#include "data/data.h"
#include "data/dataset.h"
#include "data/dataset_aggregator_server.h"

namespace cwa {
namespace data {

/**
 * @brief Data aggregator class to aggregate data composed of datasets
 */
class DataAggregator {
public:

  /**
   * @brief Construct a new Data Aggregator object
   * @param dataset_aggregator pointer to server dataset aggregator
   */
  DataAggregator(DatasetAggregatorServerPtr dataset_aggregator);
  
  /**
   * @brief Get the Id object of this data aggregator
   * @return id as an integer
   */
  int GetId() const;

  /**
   @brief Start pulling datasets to data
   @param data data where to aggregate pulled datasets
   */
  void Start(DataPtr data);
  
  /** Type definition of for function to be called when dataset is pulled */
  typedef std::function<void(DatasetPtr)> DatasetPulledCallback;

  /**
   * @brief Set callback to be run when dataset has been pulld
   * @param callback callback function
   */
  void SetDatasetPulledCallback(DatasetPulledCallback callback);
  
  /**
   Wait until all dataset have been pulled
   */
  void Wait();
  
private:
  /** Private class for private implementation */
  class Private;

  /** Private class object */
  std::shared_ptr<Private> data_;
};

}
}

#endif // CWA_DATA_DATA_AGGREGATOR_H
