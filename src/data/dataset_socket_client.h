#ifndef CWA_DATA_DATASET_SOCKET_CLIENT_H
#define CWA_DATA_DATASET_SOCKET_CLIENT_H

#include <asio.hpp>

using asio::ip::tcp;

namespace cwa {
namespace data {

/**
 * @brief Dataset socket client for TCP data exchange
 */
class DatasetSocketClient {
public:
  /** Construct a new Dataset Socket Client object */
  DatasetSocketClient();

  /** Destroy the Dataset Socket Client object */
  ~DatasetSocketClient();
  
  /**
   * @brief Register client to a server with a timeout
   * @param url url of the server to register to
   * @param timeout_ms timeout for server answer
   */
  void Register(const std::string & url, int timeout_ms);
  
  /**
   * @brief Get id of the client
   * @return id of the client as an uint32_t 
   */
  uint32_t GetId() const;

  /**
   * @brief Get push/pull asio socket
   * @return soeckt as an asio tcp::socket
   */
  tcp::socket & GetPushPullSocket();
  
private:
  /** Context for asio */
  asio::io_context io_context_;

  /** TCP Push/Pull socket */
  tcp::socket push_pull_socket_ = tcp::socket(io_context_);
  
  /** Client id */
  uint32_t client_id_ = -1;
  
};


}
}

#endif // CWA_DATA_DATASET_SOCKET_CLIENT_H
