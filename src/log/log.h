#ifndef CWA_LOG_LOG_H
#define CWA_LOG_LOG_H

#include <string>
#include <memory>

namespace cwa {

/** @enum LogLevel
 *  @brief Log level
 */
enum class LogLevel {
  Trace, /**< output all log */
  Debug, /**< log for debug */
  Info, /**< default log level */
  Error, /**< log only errors */
  Fatal /**< log only fatal errors */
};

/**
 * @brief Log class for logging message with different levels
 * 
 */
class Log {
public:

  /**
   * @brief Initialize log from options given to main application
   * 
   * @param argc number arguments
   * @param argv argument string vector
   */
  static void Init(int argc, char* argv[]);

  /**
   * @brief Initialize log and optinally record a file instead of std out
   * 
   * @param level log level
   * @param filename filename to record in, if given, log is recroded into
   *  that file
   */
  static void Init(LogLevel level, const std::string & filename = "");

  /**
   * @brief %Log formatted message
   * 
   * @param level log level
   * @param format format 
   * @param ... format parameter values
   */
  static void Print(LogLevel level, const char* format, ...);
  
private:
  /** Private class for private implementation */
  class Private;

  /** Private class object */
  static std::unique_ptr<Private> data_;

};

}

#endif // CWA_LOG_LOG_H
